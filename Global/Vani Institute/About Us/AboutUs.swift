//
//  AboutUs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import KRProgressHUD

class AboutUs: UIViewController {
    
    @IBOutlet weak var aboutCEO: UITextView!
    @IBOutlet weak var des: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var institute: UILabel!
    @IBOutlet weak var CEOImage: UIImageView!
    
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var name: UILabel!
    
    var getCeoVani:[getAboutUs] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        
        getCeoInfo()
        // Do any additional setup after loading the view.
    }
    
    
    func getCeoInfo(){
        
        Service.shared.GETService(extraParam: API.aboutUs) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            guard let about = response.dictionary else{return}
            if let aboutVani = about["about_us"]!.array{
                for get in aboutVani{
                
                self.name.text = get["name"].string!
                self.position.text = get["position"].string!
                self.institute.text = get["institute"].string!
                self.aboutCEO.text = get["description"].string!
                
                let imageRes = ImageResource(downloadURL: URL(string: get["image"].string!)!)
                self.CEOImage.kf.setImage(with: imageRes)
                self.CEOImage.layer.cornerRadius = 8
                self.CEOImage.layer.borderWidth = 1.0
                self.CEOImage.layer.borderColor = UIColor.black.cgColor
                
            }
            
            }
            
        }
        
        
    }
    
    
    @IBAction func aboutUsAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
    present(movetoMain, animated: true, completion: nil)
       // dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
