//
//  Publications.swift
//  VaniInstitute
//
//  Created by Raju Matta on 08/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import WebKit
class Publications: UIViewController,WKUIDelegate {

    @IBOutlet weak var publicationsWebView: WKWebView!
    var urlDemo : String?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let urlString = urlDemo
        let request = URLRequest(url: URL(string: urlString!)!)
        self.publicationsWebView.load(request)
  
    }
    
    @IBAction func publicationAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
      //  dismiss(animated: false, completion: nil)
    }
}
