//
//  Register.swift
//  VaniInstitute
//
//  Created by Raju Matta on 05/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KRProgressHUD
import Kingfisher

class UpdateProfile: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var toastMessage: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var locTableview: UITableView!
    var profileIMageData:String = ""
    
    @IBOutlet weak var specTableView: UITableView!
    @IBOutlet weak var upDateProfileImage: UIButton!
    
    @IBOutlet weak var branchTableview: UITableView!
    @IBOutlet weak var branchnameTF: TextFieldValidator!
    @IBOutlet weak var courseTAbleView: UITableView!
    @IBOutlet weak var userProfileImageView: UIImageView!
    
    @IBOutlet weak var collegeNameTF: TextFieldValidator!
    @IBOutlet weak var emailTextField: TextFieldValidator!
    @IBOutlet weak var mobileTextFiled: TextFieldValidator!
    @IBOutlet weak var nameTextFiled: TextFieldValidator!
    
    @IBOutlet weak var specialization: TextFieldValidator!
    
    @IBOutlet weak var yrTF: TextFieldValidator!
    @IBOutlet weak var locationTF: TextFieldValidator!
    @IBOutlet weak var courseTF: TextFieldValidator!
    
    var selectedTF:UITextField?
    var courses:[Courses] = []
    var specializationData:[Specialization] = []
    var branchesArray:[BranchesGet] = []
    let picker = UIImagePickerController()
    
    var  REGEX_USER_NAME_LIMIT = "^.{3,20}$"
    var REGEX_USER_NAME = "[A-Za-z0-9]{3,20}"
    var REGEX_EMAIL  = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    var REGEX_PHONE_DEFAULT = "[0-9]{10}"
    var REGEX_course = "[A-Za-z]{3,30}"
    var TFNames:[TextFieldValidator] = []
    var TFImagesArray = [#imageLiteral(resourceName: "name_unselected"),#imageLiteral(resourceName: "call_us_unselected"),#imageLiteral(resourceName: "mail_unselected"),#imageLiteral(resourceName: "clg_unselected"),#imageLiteral(resourceName: "crs_unselected"),#imageLiteral(resourceName: "crs_unselected"),#imageLiteral(resourceName: "loc_unselected"),#imageLiteral(resourceName: "branch_unselected"),#imageLiteral(resourceName: "crs_unselected")]
    
    
    var locations:[Location] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        TFNames  = [nameTextFiled,mobileTextFiled,emailTextField,collegeNameTF,courseTF,specialization,locationTF,branchnameTF,yrTF]
        assignImagesToTF()
        
        
        TFvalidations()
        getProfileData()
        courseTAbleView.isHidden = true
        specTableView.isHidden = true
        locTableview.isHidden = true
        branchTableview.isHidden = true
        picker.delegate = self
        collegeNameTF.delegate = self
        nameTextFiled.delegate = self
        emailTextField.delegate = self
        courseTF.delegate = self
        specialization.delegate = self
        locationTF.delegate = self
        yrTF.delegate = self
        mobileTextFiled.delegate = self
        branchnameTF.delegate = self
        DispatchQueue.main.async {
            var frame = self.courseTAbleView.frame;
            frame.size.height = self.courseTAbleView.contentSize.height;
            self.courseTAbleView.frame = frame;
            
        }
        //This code will run in the main thread:
        
        
        
        //courseview.isHidden = true
    }
    
    
    @IBAction func SelectCourse(_ sender: Any) {
        
    }
    func assignImagesToTF(){
        
        
        for j in 0..<TFNames.count{
            
            
            
            print(TFNames[j])
            let imageView1 = UIImageView(frame: CGRect(x: -5, y: 0, width: 30, height: 30))
            imageView1.image = TFImagesArray[j]
            
            //imageView1.tintColor = tintColor
            let view1 = UIView(frame : CGRect(x: -5, y: 5, width: 30, height: 30))
            TFNames[j].leftViewMode = .always
            
            
            view1.addSubview(imageView1)
            
            if(j == 4 || j == 5 || j == 6 )
            {
                let imageView2 = UIImageView(frame: CGRect(x: -5, y: 0, width: 30, height: 30))
                imageView2.image = UIImage(named: "arw_unselected")
                
                //imageView1.tintColor = tintColor
                let view2 = UIView(frame : CGRect(x: -5, y: 5, width: 30, height: 30))
                view2.addSubview(imageView2)
                TFNames[j].rightViewMode = .always
                
                TFNames[j].rightView = view2
            }
            
            
            TFNames[j].leftView = view1
            
        }
        
    }
    
    func TFvalidations(){
        
        nameTextFiled.addRegx(REGEX_USER_NAME_LIMIT, withMsg: "User name charaters limit should be come between 3-10")
        // nameTextFiled.validateOnResign = false
        mobileTextFiled.addRegx(REGEX_PHONE_DEFAULT, withMsg: "Phone number must be in proper format (eg. ##########)")
        // mobileTextFiled.isMandatory = false
        
        nameTextFiled.addRegx(REGEX_USER_NAME, withMsg: "Only alpha numeric characters are allowed.")
        emailTextField.addRegx(REGEX_EMAIL, withMsg: "Enter valid email.")
        
        
        
        
    }
    
    func getProfileData(){
        
        KRProgressHUD.show()
        
        Service.shared.GETService(extraParam: API.profile + Credentials.userId.string(forKey: "id")!) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            
            guard  let getData = response.dictionary else {return}
            
            if let studentInfo = getData["profile_details"]!.array{
                
                for get in studentInfo{
                    
                    let studentDic = get.dictionary
                    
                    self.nameTextFiled.text = studentDic!["name"]!.string
                    self.mobileTextFiled.text = studentDic!["mobile"]!.string
                    self.emailTextField.text = studentDic!["email"]!.string
                    self.courseTF.text = studentDic!["course_name"]!.string
                    self.specialization.text = studentDic!["specialization_name"]!.string
                    self.collegeNameTF.text = studentDic!["collage_name"]!.string
                    print(studentDic!["study_year"]!.string!)
                    self.locationTF.text = studentDic!["location_name"]!.string
                    if let yr = studentDic!["study_year"]!.string{
                        if yr == ""{
                            self.yrTF.text = yr
                            
                            
                        }
                            
                        else{
                            
                            self.yrTF.text = String(yr.first!)
                            
                        }
                        
                        
                    }
                    
                    
                    Specialization.selectedSpecId = studentDic!["specialization_id"]?.string
                    self.branchnameTF.text = studentDic!["branch_name"]?.string
                    Courses.selectedCourseID = studentDic!["course_id"]?.string
                    Location.selectedLocId = studentDic!["location_id"]?.string
                    Location.branchId = studentDic!["branch_id"]?.string
                    
                    let imageRes = ImageResource(downloadURL: URL(string:  (studentDic!["image"]?.string)!)!)
                    
                    print(imageRes)
                    
                    self.userProfileImageView.kf.setImage(with: imageRes)
                    
                    self.userProfileImageView.layer.cornerRadius = 50
                    self.userProfileImageView.clipsToBounds = true
                    
                    
                }
            }
        }
        
    }
    
    @IBAction func tappedOnUpdateProfileImageBtn(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: "Profile Picture", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
            
            //dismiss(animated: true, completion: nil)
        })
        
        let  deleteButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            let picker = UIImagePickerController()
            picker.delegate = self
            
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
            //dismiss(animated: true, completion: nil)
            
            
        })
        let  removePhoto = UIAlertAction(title: "Remove Photo", style: .default, handler: { (action) -> Void in
            
            //            self.userProfileImageView.image = UIImage(named: "dp_old")
            //            self.profileIMageData = ""
            //
            
            
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(removePhoto)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        print(image)
        userProfileImageView.image = image
        userProfileImageView.layer.cornerRadius = 50
        userProfileImageView.clipsToBounds = true
        
        let imageData = image.jpegData(compressionQuality: 0.8)
        self.profileIMageData = imageData!.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        
    }
    
    
    
    func coure(nameCourse: String) {
        
        courseTF.text = nameCourse
        
        
        
    }
    @IBAction func proceedButton(_ sender: UIButton) {
        
        if nameTextFiled.validate() && mobileTextFiled.validate() && emailTextField.validate() && collegeNameTF.validate() && courseTF.validate() && specialization.validate() && locationTF.validate() && branchnameTF.validate() && yrTF.validate(){
            
            proceedService()
            
            
        }
        
    }
    func proceedService(){
        
        KRProgressHUD.show()
        
        Constants.enteredMobileNo = mobileTextFiled.text!
        print(yrTF.text!)
        let parameters:Parameters = ["name":nameTextFiled.text!,"std_dp":self.profileIMageData,"collage_name":collegeNameTF.text!,"location_id":Location.selectedLocId!,"course_id":Courses.selectedCourseID!,"study_year":yrTF.text!,"specialization_id":Specialization.selectedSpecId!,"branch_id":Location.branchId ?? "","user_id":Credentials.userId.string(forKey: "id")!]
        print(parameters)
        Service.shared.POSTService(serviceType: API.update_Profile, parameters: parameters as! [String : String]) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            
            let success = response.dictionary
            
            if let resultValue = success?["success"]?.int{
                
                
                if resultValue == 1{
                    
                    self.toastMessage.makeToast("Profile Updated")
                    //                    self.view.makeToast("", duration: 1.0, point: CGPoint(x: 150, y: 400), title: "profile updated", image: nil)
                    //                    { didTap in
                    //                        if didTap {
                    //                            print("completion from tap")
                    //                        } else {
                    //                            print("completion without tap")
                    //                        }
                    //                    }
                    //
                    
                }else{
                    
                    
                    print()
                    if let message = success?["message"]?.string{
                        
                        self.toastMessage.makeToast(message)
                        
                    }
                }
                
            }
            
            
            
        }
    }
    
    
    func courseData(nameCourse: String) {
        
        courseTF.text = nameCourse
    }
    
    @IBAction func back(_ sender: UIButton) {
        
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)    }
}


extension UpdateProfile:UITextFieldDelegate,CourseNamePass{
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        selectedTF = textField
        
        if textField == courseTF{
            
            KRProgressHUD.show()
            courseTAbleView.isHidden = false
            specTableView.isHidden = true
            locTableview.isHidden = true
            branchTableview.isHidden = true
            
            let parameters:Parameters = ["table":"course"]
            
            Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                
                KRProgressHUD.dismiss()
                print(response)
                
                let success = response.dictionary
                
                if let result = success?["result"]?.array{
                    
                    for i in result{
                        
                        var getCourse = Courses()
                        
                        
                        getCourse.courseName = i["name"].string!
                        getCourse.courseId = i["id"].string!
                        self.courses.append(getCourse)
                        
                        
                    }
                    
                    
                    self.courseTAbleView.reloadData()
                    
                }
            }
            
            return true
            
        }
            
            
            
        else if textField == specialization{
            courses.removeAll()
            
            if courseTF.text?.isEmpty == false{
                
                KRProgressHUD.show()
                
            }
            
            if courseTF.text?.count == 0{
                
                self.toastMessage.makeToast("Select Course")
                
            }
            else{
                courseTAbleView.isHidden = true
                specTableView.isHidden = false
                locTableview.isHidden = true
                branchTableview.isHidden = true
                if let id = Courses.selectedCourseID{
                    //dynamicView.isHidden = false
                    let parameters:Parameters = ["table":"specialization","sid":id]
                    
                    
                    Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                        
                        KRProgressHUD.dismiss()
                        print(response)
                        
                        let success = response.dictionary
                        
                        let result = success!["result"]?.array
                        
                        
                        
                        for i in result!{
                            
                            var getSpeci = Specialization()
                            
                            
                            getSpeci.name = i["name"].string!
                            getSpeci.id = i["id"].string!
                            self.specializationData.append(getSpeci)
                            self.specTableView.reloadData()
                            
                            
                        }
                        
                        
                        
                    }
                }
            }
            
            
            return true
            
        }else if textField == locationTF{
            
            KRProgressHUD.show()
            // dynamicView.isHidden = false
            courseTAbleView.isHidden = true
            specTableView.isHidden = true
            locTableview.isHidden = false
            branchTableview.isHidden = true
            let parameters:Parameters = ["table":"location"]
            
            Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                
                KRProgressHUD.dismiss()
                
                print(response)
                
                let success = response.dictionary
                
                let result = success!["result"]?.array
                
                
                
                for i in result!{
                    
                    var getLocation = Location()
                    
                    
                    getLocation.name = i["name"].string!
                    getLocation.id = i["id"].string!
                    self.locations.append(getLocation)
                    self.locTableview.reloadData()
                    
                    
                }
            }
        }
        else if textField == branchnameTF
            
            
        {
            
            KRProgressHUD.show()
            // dynamicView.isHidden = false
            courseTAbleView.isHidden = true
            specTableView.isHidden = true
            locTableview.isHidden = true
            branchTableview.isHidden = false
            
            if let LocId = Location.selectedLocId{
                
                print(LocId)
                let parameters:Parameters = ["table":"branch","sid":LocId]
                
                Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                    
                    KRProgressHUD.dismiss()
                    
                    print(response)
                    
                    guard let success = response.dictionary else{return}
                    
                    if let result = success["result"]?.array{
                        
                        
                        
                        for i in result{
                            
                            var getBarnch = BranchesGet()
                            
                            
                            getBarnch.branchName = i["name"].string
                            Location.branchId = i["id"].string
                            self.branchesArray.append(getBarnch)
                            self.branchTableview.reloadData()
                            
                            
                        }
                        
                    }
                }
            }
            
        }
        return true
    }
    
    
}


extension UpdateProfile:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == courseTAbleView{
            
            print(courses.count)
            return courses.count
        }else if tableView == specTableView{
            
            
            return specializationData.count
            
        }else if tableView == branchTableview{
            
            return branchesArray.count
            
        }
        else{
            
            return locations.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == courseTAbleView{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCousreCell") as! SelectedCousreCell
            cell.courseLbl.text = courses[indexPath.row].courseName
            return cell
            
        }else if tableView == specTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpecializationSelectedCell") as! SpecializationSelectedCell
            cell.selectedSpe!.text = specializationData[indexPath.row].name
            return cell
            
        }else if tableView == branchTableview{
            
            print(branchesArray)
            let cell = tableView.dequeueReusableCell(withIdentifier: "BranchCell") as! BranchCell
            cell.branchName.text = branchesArray[indexPath.row].branchName
            return cell
            
        }
            
            
            
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedLocationCell") as! SelectedLocationCell
            cell.selectedLoc!.text = locations[indexPath.row].name
            return cell
            
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == courseTAbleView{
            print(courses.count)
            
            Courses.selectedCourseID = courses[indexPath.row].courseId
            
            
            courseTF.text = courses[indexPath.row].courseName
            courseTAbleView.isHidden = true
            courses.removeAll()
        }
            
        else if tableView == specTableView{
            
            
            
            specialization.text = specializationData[indexPath.row].name ?? ""
            
            Specialization.selectedSpecId = specializationData[indexPath.row].id
            
            specializationData.removeAll()
            specTableView.isHidden = true
            
        }
        else if tableView == branchTableview{
            
            
            
            branchnameTF.text = branchesArray[indexPath.row].branchName ?? ""
            
            
            branchesArray.removeAll()
            branchTableview.isHidden = true
            
        }
            
        else{
            
            // dynamicView.isHidden = true
            
            locationTF.text = locations[indexPath.row].name
            Location.selectedLocId = locations[indexPath.row].id
            
            
            locations.removeAll()
            locTableview.isHidden = true
        }
        
        
    }
}

