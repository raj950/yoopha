//
//  CommonStrings.swift
//  VaniInstitute
//
//  Created by Raju Matta on 08/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation

class CommonStrings{
    static var optpopUp = "OTPPopUP"
    static var mainVC = "MainVC"
    static var courseCell = "CourseCell"
    static var faqsCell = "FAQSCell"
    static var programCell = "ProgramCell"
    static var descriptionPopUp = "DescriptionPopUp"
    static var contactUsCell = "ContactUsCell"
    static var achivementCell = "AchivementCell"
    static var publications = "Publications"
    static var aboutUs = "AboutUs"
    static var schedules = "SchedulesViewController"
    static var achivements = "Achivements"
    static var testSeries = "TestSerioes"
    static var news = "News"
    static var library = "Library"
    static var successStories = "SuccessStories"
    static var faqs = "FAQS"
    static var contactUs = "ContactUs"
    static var programs = "Programs"
    static var batches = "Batches"
    
}
