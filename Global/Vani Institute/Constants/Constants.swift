//
//  Constants.swift
//  VaniInstitute
//
//  Created by Raju Matta on 03/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation

class Constants{
    static var baseUrl = "https://www.vaniinstitute.com/app/services/"
    static var enteredMobileNo:String?
    static var fcmtoken:String?
}
struct API {
    
    static var registarion = Constants.baseUrl + "registration.php"
    static var login = Constants.baseUrl + "login.php"
    static var verifyOTP = Constants.baseUrl + "verify_otp.php"
    static var resendOTP = Constants.baseUrl + "resend_otp.php"
    static var update_Profile = Constants.baseUrl + "update_profile.php"
    static var profile = Constants.baseUrl + "profile.php" + "?user_id="
    static var helpUs = Constants.baseUrl + "help-us.php"
    static var successStories = Constants.baseUrl + "success-stories.php"
    static var news = Constants.baseUrl + "news.php"
    static var updates = Constants.baseUrl + "updates.php"
    static var FAQ = Constants.baseUrl + "faq.php"
    static var library = Constants.baseUrl + "library.php"
    static var batches = Constants.baseUrl + "batches.php"
    static var schedule = Constants.baseUrl + "schedules.php"
    static var searchShedule = Constants.baseUrl + "search-schedule.php"
    static var gallery = Constants.baseUrl + "gallery.php"
    static var archievement = Constants.baseUrl + "archivement.php"
    static var dynamictable = Constants.baseUrl + "dynamictables.php"
    static var ranking = Constants.baseUrl + "ranking.php"
    //static var successStories = Constants.baseUrl + "success-stories.php"
    static var yearbaseStories = Constants.baseUrl + "yearbase_stories.php"
    static var locationBasedBatches = Constants.baseUrl + "batches.php"
    static var programmes = Constants.baseUrl + "programmes.php"
    static var socialLinks = Constants.baseUrl + "links.php"
    static var courses = Constants.baseUrl + "course.php"
    static var chat = Constants.baseUrl + "chat.php"
    static var callUs = Constants.baseUrl + "call_us.php"
    static var getoLcation = Constants.baseUrl + "get_location.php"
    static var ocationBasedBrances = Constants.baseUrl + "get_branch.php"
    static var help_us = Constants.baseUrl + "help-us.php"
    static var fcmTokenUpdate = Constants.baseUrl + "update_fcm.php"
    static var getBanners = Constants.baseUrl + "get_banners.php"
    static var contactUs = Constants.baseUrl + "get_branch.php"
    static var aboutUs = Constants.baseUrl + "about-us.php"
    static var getVenu = Constants.baseUrl + "get_venue.php"
    
}
