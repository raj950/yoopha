//
//  PdfCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 13/03/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class PdfCell: UITableViewCell {

    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var bookImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
