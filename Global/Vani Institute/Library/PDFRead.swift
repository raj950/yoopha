//
//  PDFRead.swift
//  VaniInstitute
//
//  Created by Raju Matta on 13/03/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import WebKit

class PDFRead: UIViewController {

    var pdfString:String?
    @IBOutlet weak var readdata: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

    
//        let urlString = pdfString
//        let request = URLRequest(url: URL(string: urlString!)!)
//                self.readdata.load(request)
//
        
        let urlString = "https://www.vaniinstitute.com/app/admin/attachments/library/pdf/5_Maths-2020 Final.pdf"
        print(urlString)
      //  let request = URLRequest(url: URL(string: urlString)!)
      //  self.readdata.load(request)
       
        let webview = WKWebView(frame: UIScreen.main.bounds)
        view.addSubview(webview)
        
        webview.load(URLRequest(url: URL(string: urlString)!))
        
        
        
        
       // downloadPDF(test: urlString!)
        // Do any additional setup after loading the view.
    }
    
//    private func downloadPDF(test:String){
//        print(test)
//
//        guard let url = URL(string: test) else{return}
//        let fileName = String((url.lastPathComponent)) as NSString
//        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
//        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
//
//        let fileURL = URL(string: test)
//        let sessionConfig = URLSessionConfiguration.default
//        let session = URLSession(configuration: sessionConfig)
//        let request = URLRequest(url:fileURL!)
//        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
//            if let tempLocalUrl = tempLocalUrl, error == nil {
//
//                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
//                    print("Successfully downloaded. Status code: \(statusCode)")
//
//                }
//                do {
//                    try? FileManager.default.removeItem(at: destinationFileUrl) // remove the existing file
//                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl) // copy the new file to File Manager
//
//                    do {
//
//                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
//                        for indexx in 0..<contents.count {
//                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
//
//                                print("Pdf Downloaded")
//                                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
//                                self.present(activityViewController, animated: true, completion: nil)
//                            }
//                        }
//                    }
//                    catch (let err) {
//                        print("error: \(err)")
//                    }
//                } catch (let writeError) {
//
//                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
//                }
//            } else {
//
//                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
//            }
//        }
//        task.resume()
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func back(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
