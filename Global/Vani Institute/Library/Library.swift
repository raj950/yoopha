//
//  Library.swift
//  VaniInstitute
//
//  Created by Raju Matta on 08/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import SwiftyJSON
import Kingfisher
import WebKit
import KRProgressHUD
class Library: UIViewController {

    @IBOutlet weak var toastMessage: UILabel!
    @IBOutlet weak var libraryTable: UITableView!
    @IBOutlet weak var webView: WKWebView!
    var booksStore:[PDFFiles] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        getBooks()
 
        
       
        // Do any additional setup after loading the view.
    }
    

   func getBooks(){
    
    let parameters = ["user_id":Credentials.userId.string(forKey: "id")]
    
    Service.shared.POSTService(serviceType: API.library, parameters: parameters as! [String : String]) { (response) -> (Void) in
        
        KRProgressHUD.dismiss()
        guard let books = response.dictionary else{return}
        if let get = books["success"]?.int{
            
            if get == 1{
                
                let ArrayBooks = books["library_details"]?.array
                
                for i in ArrayBooks!{
                    
                    var booksRead = PDFFiles()
                    
                    
                    booksRead.name = i["name"].string
                    booksRead.image = i["image"].string
                    booksRead.pdf_file = i["pdf_file"].string
                    self.booksStore.append(booksRead)
                    
                    
                }
                
                print(self.booksStore.count)
                self.libraryTable.reloadData()
                
            }
            
            
            else{
                
                self.toastMessage.makeToast("No Records Found")
//                self.view.makeToast("", duration: 1.0, point: CGPoint(x: 200, y: 600), title: "NO Records Found", image: nil)
//                { didTap in
//                    if didTap {
//                        print("completion from tap")
//                    } else {
//                        print("completion without tap")
//                    }
//                }
//
            }
            
            
        }
    }
        
    
        
    }
    
    
    
    
    
    @IBAction func libraryAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        //dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Library:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return booksStore.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PdfCell") as! PdfCell
      //  let imageRes = ImageResource(downloadURL: URL(string: booksStore[indexPath.row].image!)!)
        
      //  cell.bookImage?.kf.setImage(with: imageRes, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
            cell.setNeedsLayout()
       // })
        
        //cell.bookImage.kf.setImage(with: imageRes)
        
        cell.bookName.text = booksStore[indexPath.row].name
        
        return cell
        
    }
    
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let passData = storyboard?.instantiateViewController(withIdentifier: "PDFRead")as! PDFRead
        
        passData.pdfString = booksStore[indexPath.row].pdf_file
        present(passData, animated: true, completion: nil)
        
    }
    
}
