//
//  Batches.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import WebKit

class Batches: UIViewController {
    
    var urlDemo : String?
    
    @IBOutlet weak var batchesWebView: WKWebView!
    @IBOutlet weak var getbatchesTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getContactUs()
        
        let urlString = urlDemo
        let request = URLRequest(url: URL(string: urlString!)!)
        self.batchesWebView.load(request)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func getBack(_ sender: Any) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        //dismiss(animated: false, completion: nil)
    }
    
    func getContactUs(){
        
        
        
        Service.shared.GETService(extraParam: API.getoLcation) { (response) -> (Void) in
            
            print(response)
            
            guard let contactUs = response.dictionary else{return}
            
            if let locations = contactUs["location"]!.array{
            
                for getloc in locations{
                
                var getLocation = Locations()
                getLocation.locationsId = getloc["id"].string!
                getLocation.locationsName = getloc["name"].string!
                getLocation.locationImage = getloc["icon"].string!
                // self.getLocationsAll.append(getLocation)
                
                Locations.getLocationsAll.append(getLocation)
                self.getbatchesTbl.reloadData()
                
                
            }
            }
            
            
        }
        
        
    }
    
}

extension Batches:UITableViewDelegate,UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Locations.getLocationsAll.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BatchesCell", for: indexPath) as! BatchesCell
        cell.locName?.text = Locations.getLocationsAll[indexPath.row].locationsName
        cell.locImage.clipsToBounds = true;
        
        cell.locImage.layer.cornerRadius = 20
        cell.locImage.layer.borderWidth = 1.0
        cell.locImage.layer.borderColor = UIColor.black.cgColor
        let filteringString = Locations.getLocationsAll[indexPath.row].locationImage
        
        let filteredString = String(filteringString!.filter{!" ".contains($0)})
        let imageRes = ImageResource(downloadURL: URL(string: filteredString)!)
        // cell.locationImage!.kf.setImage(with: imageRes)
        cell.locImage?.kf.setImage(with: imageRes, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
            cell.setNeedsLayout()
        })
        
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedLoc = storyboard?.instantiateViewController(withIdentifier: "DetailsContactUs")as! DetailsContactUs
        Locations.selectedLocId = Locations.getLocationsAll[indexPath.row].locationsId!
        present(selectedLoc, animated: false, completion: nil)
        
        
    }
    
    
}
