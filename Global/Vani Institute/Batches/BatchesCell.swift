//
//  BatchesCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 17/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class BatchesCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var locName: UILabel!
    @IBOutlet weak var locImage: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
