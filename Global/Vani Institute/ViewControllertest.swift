//
//  ViewControllertest.swift
//  VaniInstitute
//
//  Created by Raju Matta on 03/02/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewControllertest: UIViewController {

    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
      self.locationManager.requestWhenInUseAuthorization()
      if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            }
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ViewControllertest : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = "17.480632"
        print(lat)
        
        let long = "78.419741"
        print(long)
        
        if currentLocation == nil {
            
            
            currentLocation = CLLocation(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!)
            
            let camera = GMSCameraPosition.camera(withLatitude: (currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)! , zoom: 19.0)
            print(camera)
            let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 245, width: 375, height: 178), camera: camera)
            //  mapView.isMyLocationEnabled = true
            
            
            self.view.addSubview(mapView)
            
            
            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(lat)! , CLLocationDegrees(long)!)
            
            print(marker.position)
            
            marker.icon = UIImage(named:"30")
            marker.title = "Hyderabad"
            marker.snippet = "Hyderabad"
            
            
            marker.map = mapView
            
            //marker.map?.addSubview(mapView)
//            DispatchQueue.main.async {
//                self.addressLB.text = "Fetching Address......"
//            }
           // getAddressFromLocation(location: currentLocation!)
            
        }
}
}
