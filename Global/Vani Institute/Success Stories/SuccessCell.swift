//
//  SuccessCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 12/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import WebKit

class SuccessCell: UITableViewCell {

    @IBOutlet weak var yrLbl: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var rankLbl: UILabel!
    @IBOutlet weak var videoPlay: WKWebView!
    @IBOutlet weak var scuccessVideos: WKWebView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
