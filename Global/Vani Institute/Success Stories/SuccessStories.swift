//
//  SuccessStories.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import WebKit
import KRProgressHUD

class SuccessStories: UIViewController {
    
    @IBOutlet weak var yrLbl: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var rankLbl: UILabel!
    @IBOutlet weak var playVideo: WKWebView!
    @IBOutlet weak var vani28yrs: UIImageView!
    @IBOutlet weak var successTable: UITableView!
    var succuessArray:[SuccessStoriesInfo] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        //vani28yrs.layer.cornerRadius = 14
        // vani28yrs.clipsToBounds = true
        getSuccess()
        // Do any additional setup after loading the view.
    }
    
    
    
    func getSuccess(){
        
        Service.shared.GETService(extraParam: API.successStories) { (response) -> (Void) in
            
            
            KRProgressHUD.dismiss()
            print(response)
            
            DispatchQueue.global(qos: .background).async {
                
                DispatchQueue.main.async {
                    guard let SuccessGet = response.dictionary else{return}
                    
                    if  let allSuccess = SuccessGet["success_stories"]!.array {
                    
                        for get in allSuccess{
                        
                        var vaniSuccess = SuccessStoriesInfo()
                        
                        vaniSuccess.rank = get["rank"].string!
                        vaniSuccess.description = get["description"].string!
                        vaniSuccess.link = get["link"].string!
                        vaniSuccess.year = get["year"].string!
                        print(vaniSuccess.link)
                        self.succuessArray.append(vaniSuccess)
                        
                        
                    }
                    self.successTable.reloadData()
                    }
                    
                }
                
                
            }
            
            
            
        }
        
        
    }
    
    
    
    
    
    @IBAction func successAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
       // dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SuccessStories:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(succuessArray.count)
        return succuessArray.count
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuccessCell") as! SuccessCell
        cell.rankLbl.text = succuessArray[indexPath.row].rank
        cell.desLbl.text = succuessArray[indexPath.row].description
        cell.yrLbl.text  = succuessArray[indexPath.row].year
        
        print(cell.rankLbl)
        
        DispatchQueue.main.async {
            //let urlString = self.succuessArray[indexPath.row].link
            
            let urlString = "https://www.youtube.com/watch?v=52Oze6CbGi4?playsinline=1"
            let request = URLRequest(url: URL(string: urlString)!)
            cell.videoPlay.load(request)
cell.videoPlay.configuration.allowsInlineMediaPlayback = false
            
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
    
}
