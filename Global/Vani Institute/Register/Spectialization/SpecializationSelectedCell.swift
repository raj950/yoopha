//
//  SpecializationSelectedCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 21/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class SpecializationSelectedCell: UITableViewCell {

    @IBOutlet weak var selectedSpe: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
