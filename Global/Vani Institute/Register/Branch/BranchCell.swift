//
//  BranchCell.swift
//  Vani Institute
//
//  Created by Raju Matta on 20/03/19.
//  Copyright © 2019 Vani Institute. All rights reserved.
//

import UIKit

class BranchCell: UITableViewCell {

    @IBOutlet weak var branchName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
