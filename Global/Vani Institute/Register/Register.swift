//
//  Register.swift
//  VaniInstitute
//
//  Created by Raju Matta on 05/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EasyTipView
import KRProgressHUD
class Register: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var toastMessage: UILabel!
    var  REGEX_USER_NAME_LIMIT = "^.{3,20}$"
    var REGEX_USER_NAME = "[A-Za-z0-9]{3,20}"
    var REGEX_EMAIL  = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    var REGEX_PHONE_DEFAULT = "[0-9]{10}"
    var REGEX_course = "[A-Za-z]{3,30}"
   
    var profileIMageData:String = ""

    @IBOutlet weak var locTableview: UITableView!
    
    @IBOutlet weak var specTableView: UITableView!
    @IBOutlet weak var upDateProfileImage: UIButton!
    
    @IBOutlet weak var courseTAbleView: UITableView!
    @IBOutlet weak var userProfileImageView: UIImageView!
   
    @IBOutlet weak var collegeNameTF: TextFieldValidator!
    @IBOutlet weak var emailTextField: TextFieldValidator!
    @IBOutlet weak var mobileTextFiled: TextFieldValidator!
    @IBOutlet weak var nameTextFiled: TextFieldValidator!
    
    @IBOutlet weak var specialization: TextFieldValidator!
    
    @IBOutlet weak var yrTF: TextFieldValidator!
    @IBOutlet weak var locationTF: TextFieldValidator!
    @IBOutlet weak var courseTF: TextFieldValidator!
    
    var selectedTF:UITextField?
    var courses:[Courses] = []
    var specializationData:[Specialization] = []
    let picker = UIImagePickerController()
    var TFImagesArray = [#imageLiteral(resourceName: "name_unselected"),#imageLiteral(resourceName: "call_us_unselected"),#imageLiteral(resourceName: "mail_unselected"),#imageLiteral(resourceName: "clg_unselected"),#imageLiteral(resourceName: "crs_unselected"),#imageLiteral(resourceName: "crs_unselected"),#imageLiteral(resourceName: "loc_unselected"),#imageLiteral(resourceName: "crs_unselected")]
    var TFNames:[TextFieldValidator] = []
    var locations:[Location] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
    TFNames  = [nameTextFiled,mobileTextFiled,emailTextField,collegeNameTF,courseTF,specialization,locationTF,yrTF]
        
        assignImagesToTF()

        
    TFvalidations()
        
        nameTextFiled.delegate = self
        courseTAbleView.isHidden = true
        specTableView.isHidden = true
        locTableview.isHidden = true
        picker.delegate = self
        mobileTextFiled.delegate = self
       
        courseTF.delegate = self
        specialization.delegate = self
        locationTF.delegate = self
        yrTF.delegate = self
        emailTextField.delegate = self
        
        collegeNameTF.delegate = self
       
        
        DispatchQueue.main.async {
            var frame = self.courseTAbleView.frame;
            frame.size.height = self.courseTAbleView.contentSize.height;
            self.courseTAbleView.frame = frame;
            
        }
        
        
        //courseview.isHidden = true
    }
    
    
    func assignImagesToTF(){
        
     
        for j in 0..<TFNames.count{
        
            
            
          print(TFNames[j])
            let imageView1 = UIImageView(frame: CGRect(x: -5, y: 0, width: 30, height: 30))
            imageView1.image = TFImagesArray[j]
           
            //imageView1.tintColor = tintColor
            let view1 = UIView(frame : CGRect(x: -5, y: 5, width: 30, height: 30))
            TFNames[j].leftViewMode = .always
           
           
            view1.addSubview(imageView1)
            
        
            TFNames[j].leftView = view1
            
        }
        
    }
    
    func TFvalidations(){
        
        nameTextFiled.addRegx(REGEX_USER_NAME_LIMIT, withMsg: "User name charaters limit should be come between 3-10")
       // nameTextFiled.validateOnResign = false
        mobileTextFiled.addRegx(REGEX_PHONE_DEFAULT, withMsg: "Phone number must be in proper format (eg. ##########)")
       // mobileTextFiled.isMandatory = false
        
       nameTextFiled.addRegx(REGEX_USER_NAME, withMsg: "Only alpha numeric characters are allowed.")
        emailTextField.addRegx(REGEX_EMAIL, withMsg: "Enter valid email.")
        
        
        
        
    }
    
    
    @IBAction func tappedOnUpdateProfileImageBtn(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: "Profile Picture", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
            
            //dismiss(animated: true, completion: nil)
        })
        
        let  deleteButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            let picker = UIImagePickerController()
            picker.delegate = self
            
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
            //dismiss(animated: true, completion: nil)
            
            
        })
        let  removePhoto = UIAlertAction(title: "Remove Photo", style: .default, handler: { (action) -> Void in
            
           
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(removePhoto)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        print(image)
        userProfileImageView.image = image
        userProfileImageView.layer.cornerRadius = 50
        userProfileImageView.clipsToBounds = true
        
        let imageData = image.jpegData(compressionQuality: 0.8)
        self.profileIMageData = imageData!.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func coure(nameCourse: String) {
        
        courseTF.text = nameCourse
        
        
        
    }
    
   
    func addImage(textfield:UITextField,img:UIImage){
        
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20 , height: 15))
        
        imageView.image = img
        
        textfield.rightView = imageView
        
        textfield.rightViewMode = .unlessEditing
        
        
        
    }
    @IBAction func proceedButton(_ sender: UIButton) {
      
        if nameTextFiled.validate() && mobileTextFiled.validate() && emailTextField.validate() && collegeNameTF.validate() && courseTF.validate() && specialization.validate() && locationTF.validate() && yrTF.validate(){
            
            proceedService()

            
        }
       
        
            
  
    
    
    }
    func proceedService(){
        
//        KRProgressHUD
//            .set(style: .custom(background: .white, text: .white, icon: nil))
//            .set(maskType:.custom(color: .white))
//            .show()
        
        KRProgressHUD.show()
//
        Constants.enteredMobileNo = mobileTextFiled.text!
        
        Credentials.storedMobile.set(mobileTextFiled.text, forKey: "mobileNum")

        
        let parameters:Parameters = ["name":nameTextFiled.text!,"mobile":mobileTextFiled.text!
            ,"email":emailTextField.text!,"location_id":Location.selectedLocId!,"collage_name":collegeNameTF.text!,"course_id":Courses.selectedCourseID!,"std_dp":self.profileIMageData,"specialization_id":Specialization.selectedSpecId!,"fcm_token":Constants.fcmtoken!,"is_ios":"Yes"]
        
        
        
        
        Service.shared.POSTService(serviceType: API.registarion, parameters: parameters as! [String : String]) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            
            print(response)
            
            guard let success = response.dictionary else{return}
            
            if let resultValue = success["success"]?.int{
                
                
               if resultValue == 1{
                
                let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: CommonStrings.optpopUp) as! OTPPopUP
                self.addChild(POPUPVC)
                POPUPVC.view.frame = self.view.frame
                self.view.addSubview(POPUPVC.view)
                POPUPVC.didMove(toParent: self)
                    
                    
               }else{
                
                if let message = success["message"]?.string{
                    
                self.toastMessage.makeToast(message)
//                    self.view.makeToast("", duration: 1.0, point: CGPoint(x: 180, y: 430), title: message, image: nil)
//                    { didTap in
//                        if didTap {
//                            print("completion from tap")
//                        } else {
//                            print("completion without tap")
//                        }
//                    }
//
                
                }
                }
                
            }
            
           
        
    }
    }
    
    func courseData(nameCourse: String) {
        
        courseTF.text = nameCourse
        print(courseTF.text)
    }
    
}


extension Register:UITextFieldDelegate,CourseNamePass{

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
        
    }
    
   
    
   /*
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == courseTF{
            
            KRProgressHUD.show()
            
            courseTAbleView.isHidden = false
            specTableView.isHidden = true
            locTableview.isHidden = true
            
            let parameters:Parameters = ["table":"course"]
           
            Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                
                
                KRProgressHUD.dismiss()
                print(response)
                
                guard  let success = response.dictionary else{return}
                
                if let result = success["result"]?.array{
                    
                    for i in result{
                        
                        var getCourse = Courses()
                        
                        getCourse.courseName = i["name"].string!
                        getCourse.courseId = i["id"].string!
                        
                        self.courses.append(getCourse)
                        
                        
                    }
                    
                        self.courseTAbleView.reloadData()
                }
            }
            
            return true
            
        }
            
            
            
        else if textField == specialization{
            
            courses.removeAll()
            if courseTF.text?.isEmpty == false{
                
                KRProgressHUD.show()
            }
            
            if specialization.rightView?.isHidden == false{
                
                specialization.rightView?.isHidden = true
                
            }
            else{
                specialization.rightView?.isHidden = true
                
                
            }
            
            textField.textColor = UIColor.darkGray
            
            
            if courseTF.text?.count == 0{
              //  self.toastMessage .makeToast(<#T##message: String?##String?#>)
                
               self.toastMessage.makeToast("Select Course")
                
                
             //   self.toastMessage.makeToast("", duration: 1.0, point: CGPoint(x: 100, y: 400), title: "select Course", image: nil)
//                { didTap in
//                    if didTap {
//                        print("completion from tap")
//                    } else {
//                        print("completion without tap")
//                    }
//                }
                
            }
            else{
                courseTAbleView.isHidden = true
                specTableView.isHidden = false
                locTableview.isHidden = true
                if let id = Courses.selectedCourseID{
                    //dynamicView.isHidden = false
                    let parameters:Parameters = ["table":"specialization","sid":id]
                    
                    
                    Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                        
                        KRProgressHUD.dismiss()
                        
                        print(response)
                        
                        guard let success = response.dictionary else{return}
                        
                        if let result = success["result"]?.array{
                            
                            
                            
                            for i in result{
                                
                                var getSpeci = Specialization()
                                
                                
                                getSpeci.name = i["name"].string!
                                getSpeci.id = i["id"].string!
                                self.specializationData.append(getSpeci)
                                self.specTableView.reloadData()
                                
                                
                            }
                            
                        }
                        
                    }
                }
            }
            
            return true
            
        }else if textField == locationTF{
            
            KRProgressHUD.show()
            
            if locationTF.rightView?.isHidden == false{
                
                locationTF.rightView?.isHidden = true
                
            }
            else{
                locationTF.rightView?.isHidden = true
                
                
            }
           
            textField.textColor = UIColor.darkGray
            
            // dynamicView.isHidden = false
            courseTAbleView.isHidden = true
            specTableView.isHidden = true
            locTableview.isHidden = false
            let parameters:Parameters = ["table":"location"]
            
            Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
                
                
                KRProgressHUD.dismiss()
                print(response)
                
                guard let success = response.dictionary else{return}
                
                if let result = success["result"]?.array
                    
                {
                    
                    for i in result{
                        
                        var getLocation = Location()
                        
                        
                        getLocation.name = i["name"].string!
                        getLocation.id = i["id"].string!
                        self.locations.append(getLocation)
                        self.locTableview.reloadData()
                        
                        
                    }
                }
            }
        }
        
        return true
    }
 */
    }
    

extension Register:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == courseTAbleView{
            
            print(courses.count)
            return courses.count
          //  return 3
        }else if tableView == specTableView{
            
            
            return specializationData.count
        }
        else{
            
           return locations.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == courseTAbleView{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCousreCell") as! SelectedCousreCell
            cell.courseLbl.text = courses[indexPath.row].courseName
            return cell
            
        }else if tableView == specTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpecializationSelectedCell") as! SpecializationSelectedCell
            cell.selectedSpe!.text = specializationData[indexPath.row].name
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedLocationCell") as! SelectedLocationCell
            cell.selectedLoc!.text = locations[indexPath.row].name
            return cell
            
            
        }
      
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == courseTAbleView{
            print(courses.count)
            
            if let selctedCoursesave = courses[indexPath.row].courseId{
                
                Courses.selectedCourseID = selctedCoursesave
                courseTF.text = courses[indexPath.row].courseName
                courseTAbleView.isHidden = true
                courses.removeAll()

            }
            
            
            // dynamicView.isHidden = true
            
          
        }
            
        else if tableView == specTableView{
            
            
            // dynamicView.isHidden = true
            
            if let selectedData = specializationData[indexPath.row].name{
                
                specialization.text = selectedData
                Specialization.selectedSpecId = specializationData[indexPath.row].id
                
                specializationData.removeAll()
                specTableView.isHidden = true
            }
            
          
            
            
            
        }else{
            
            // dynamicView.isHidden = true
            
            locationTF.text = locations[indexPath.row].name
            Location.selectedLocId = locations[indexPath.row].id
            
            
            locations.removeAll()
            locTableview.isHidden = true
        }
        
        
    }
}

