//
//  Course.swift
//  VaniInstitute
//
//  Created by Raju Matta on 21/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
protocol CourseNamePass {
    
    func courseData(nameCourse:String)

}

class Course: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var courseTbl: UITableView!
    var courses:[Courses] = []
    var coursePro:CourseNamePass!
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.view.backgroundColor = (UIColor.white).withAlphaComponent(0.5)
getCourse()
        
    }
    
    func getCourse(){
        
        let parameters:Parameters = ["table":"course"]
        
                    Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
        
                        print(response)
        
                        let success = response.dictionary
        
                        if let result = success?["result"]?.array{
        
                        for i in result{
        
                            var getCourse = Courses()
        
        
                            getCourse.courseName = i["name"].string!
                            getCourse.courseId = i["id"].string!
                            self.courses.append(getCourse)
                            
        
                        }
        print(self.courses)
                            
//                            self.courseTbl.frame = CGRect(x: self.courseTbl.frame.origin.x, y: self.courseTbl.frame.origin.y, width: self.courseTbl.frame.size.width, height: self.courseTbl.contentSize.height)
                            self.courseTbl.reloadData()
    }
        }
    
}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCousreCell")as! SelectedCousreCell
        cell.courseLbl.text = courses[indexPath.row].courseName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       //let goBack = storyboard?.instantiateViewController(withIdentifier: "Register")as! Register
        coursePro?.courseData(nameCourse: courses[indexPath.row].courseName!)
        Courses.selectedCourseID = courses[indexPath.row].courseId
        self.view.removeFromSuperview()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
    }
    
}
