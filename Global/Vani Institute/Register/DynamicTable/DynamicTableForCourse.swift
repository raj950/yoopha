//
//  DynamicTableForCourse.swift
//  VaniInstitute
//
//  Created by Raju Matta on 05/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class DynamicTableForCourse: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var courseTableView: UITableView!
    var courses:[Courses] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        
        getCourse()
        // Do any additional setup after loading the view.
    }
    

    func getCourse(){
        
        
        let parameters:Parameters = ["table":"course"]
        
        Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in

print(response)

            let success = response.dictionary
            
           let result = success!["result"]?.array
            
     
            
            for i in result!{
                
                var getCourse = Courses()
                
                
                getCourse.courseName = i["name"].string!
                getCourse.courseId = i["id"].string!
                self.courses.append(getCourse)
                
            }
            
            self.courseTableView.reloadData()
           print(self.courses)


        }
    }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return courses.count
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CommonStrings.courseCell) as! CourseCell
            cell.textLabel?.text = courses[indexPath.row].courseName
            return cell
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        print(courses[indexPath.row].courseId)
        Courses.selectedCourseID = courses[indexPath.row].courseId
        
        let parameters:Parameters = ["table":"specialization","sid":Courses.selectedCourseID]
        
        Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
            
            
            print(response)
        }
        
        
        
    }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


