//
//  SelectedLocationCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 21/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class SelectedLocationCell: UITableViewCell {

    @IBOutlet weak var selectedLoc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
