//
//  Locations.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation
struct Locations {
    var locationsId:String?
    var locationsName:String?
    var locationImage:String?
    static var latitude:String = ""
    static var logitude:String = ""
    static var selectedLocId:String = ""
   static var getLocationsAll:[Locations] = []
    init(){}
    
}
