//
//  CallUsModle.swift
//  VaniInstitute
//
//  Created by Raju Matta on 12/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation

struct CallsUsModle {
    
    var technical:String?
    var general:String?
    var suggestion:String?
    var testSeries:String?
    var other:String?
    
    static var supportName:String?
    static var supportNumber:String?
    init(){}
    
}
