//
//  SuccessStoriesInfo.swift
//  VaniInstitute
//
//  Created by Raju Matta on 12/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation

struct SuccessStoriesInfo {

    var rank:String?
    var description:String?
    var link:String?
    var year:String?
    init(){}
    
}
