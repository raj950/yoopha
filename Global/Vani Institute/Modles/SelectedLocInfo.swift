//
//  SelectedLocInfo.swift
//  VaniInstitute
//
//  Created by Raju Matta on 17/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation

struct SelectedLocInfo {
    
    var latitude:String?
    var longitude:String?
    var attachment:String?
    var location:String?
    var email:String?
    var address:String?
    var name:String?
    var office_number:String?
    var mobile:String?
    init(){}
    
    
}
