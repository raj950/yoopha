//
//  StudentProfile.swift
//  VaniInstitute
//
//  Created by Raju Matta on 19/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation
struct StudentProfile {
    
    var studentImage:String?
    var student_id:String?
    var name:String?
    var email:String?
    var mobile:String?
    var study_year:String?
    var collage_name:String?
    var course_id:String?
    var course_name:String?
    var location_id:String?
    var location_name:String?
    var branch_id:String?
    var branch_name:String?
    var specialization_id:String?
    var specialization_name:String?
    var fcm_token:String?
    static var getUserId:String = ""
    static var getName:String?
    static var getEmail:String?
    static var getMobile:String?
    static var getLocation:String?
    static var studentData:[StudentProfile] = []
    static var branchid:String = ""
    init(){}
    
}
