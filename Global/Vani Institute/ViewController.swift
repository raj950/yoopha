//
//  ViewController.swift
//  VaniInstitute
//
//  Created by Raju Matta on 03/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var scrollImagesCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageScroll: UIView!
    
    var imagesArray:[GetBannersShow] = []
    var x:Int!

    @IBOutlet weak var userNameTF: TextFieldHelper!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        getBanners()
        scrollImagesCollection.reloadData()
        x = 0
        setTimer()

        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    func getBanners(){
        
        
        Service.shared.GETService(extraParam: API.getBanners) { (response) -> (Void) in
            
            print(response)
            
            guard let getBanners = response.dictionary else { return }
            
            let allBanners = getBanners["banners"]!.array
           
            
            for get in allBanners!{
                
                var getImage = GetBannersShow()
                
                getImage.image = get["image"].string
                self.imagesArray.append(getImage)
                self.scrollImagesCollection.reloadData()
                
            }
            
            self.pageControl.numberOfPages = self.imagesArray.count
            self.pageControl.currentPageIndicatorTintColor = UIColor.green
            
        }
        
    
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        present(publicationsVC, animated: false, completion: nil)
        
    }
    
    @IBAction func newUserAction(_ sender: Any) {
        
      
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        print(imagesArray)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesScrollCell", for: indexPath) as! ImagesScrollCell
        
        let imageRes = ImageResource(downloadURL: URL(string: imagesArray[indexPath.row].image!)!)
        
       // cell.scrollImg?.kf.setImage(with: imageRes)
        
        
        cell.scrollImg?.kf.setImage(with: imageRes, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
            cell.setNeedsLayout()
        })
        
        
        
        return cell
    }
    
    func setTimer() {
        let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
   
        
    }
    
    
    // var x = 1
    @objc func autoScroll() {
        
        
        if (self.x < self.imagesArray.count) {
            
            pageControl.currentPage = x
            let indexPath = IndexPath(item: x, section: 0)
            self.scrollImagesCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
            
            
            
            
        }
        else {
            self.x = 0
            //            //self.x = self.pageController.currentPage
            //            self.collectionView.scrollToItem(at: IndexPath(item: x, section: 0), at: .centeredHorizontally, animated: true)
            //            self.x = self.x + 1
        }
        
    }
    
    
}

