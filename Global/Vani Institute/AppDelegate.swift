//
//  AppDelegate.swift
//  VaniInstitute
//
//  Created by Raju Matta on 03/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps
import Firebase
import UserNotifications
import TBVideoSplashScreen

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        TBVideoSplashScreen.makeAndPlayVideoSplashScreen(appDelegate: self, videoNameString: "New Vani Logo Animated Video", withExtension: "mp4", launchImageString: nil)
        
        
        
        
        
        
        if let test = Credentials.storedMobile.string(forKey: "mobileNum"){
            
            
            
        }
        
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey("AIzaSyAodXOZTbuwr1lUvOgxez2hIhj-Vl06qxE")
        GMSPlacesClient.provideAPIKey("AIzaSyAodXOZTbuwr1lUvOgxez2hIhj-Vl06qxE")
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        return true
    }
    @objc
    func moviePlayBackDidFinish() {
        //any custom logic for callback goes here
        
        if(Credentials.userId.string(forKey: "id") != nil)
        {
            let vc: UIViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! MainVC
            let navCtrl = UINavigationController(rootViewController: vc)
            window?.rootViewController! = navCtrl
            NotificationCenter.default.removeObserver(self)
        }else{
            let vc: UIViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GoToApp") as! GoToApp
            let navCtrl = UINavigationController(rootViewController: vc)
            window?.rootViewController! = navCtrl
            NotificationCenter.default.removeObserver(self)
            
        }
        
       
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    
        print("User Info = \(userInfo), Application State \(application.applicationState.rawValue)")
        
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        if state != .active {
            UIApplication.shared.applicationIconBadgeNumber = 0
            if let message = userInfo["message"] as? String {
                print(message)
                let localNotification = UILocalNotification()
                localNotification.alertBody = message
                localNotification.soundName = UILocalNotificationDefaultSoundName
                UIApplication.shared.scheduleLocalNotification(localNotification)
            }
        } else if state == .active {
            if let message = userInfo["message"] as? String {
                print(message)
                let localNotification = UILocalNotification()
                localNotification.alertBody = message
                localNotification.soundName = UILocalNotificationDefaultSoundName
                UIApplication.shared.scheduleLocalNotification(localNotification)
            }
        }
        
        guard let message = userInfo["message"] as? String else { return }
        print("Notification Recieved ==> \(message)")
        
       print(message)
        completionHandler(.newData) // there will be above cases right? yes can you tell me what, message check contains 1#,2#,3# then use it, ok wait we are sending noti wait ok done tatsolve i ll send the details in watsup ok
        return
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
// [END ios_10_message_handling]


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        Constants.fcmtoken = "\(fcmToken)"
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

