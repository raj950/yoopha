//
//  EasyTipView.swift
//  VaniInstitute
//
//  Created by Raju Matta on 27/02/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation
import EasyTipView
import UIKit
class Validations{
    
    var preferences = EasyTipView.Preferences()
    var selected = true
    static let shared = Validations()
    
    init() {}
    
    func easyTip(){
        

        preferences.drawing.font = UIFont(name: "Futura-Medium", size: 13)!
        
        preferences.drawing.foregroundColor = UIColor.white
        
        preferences.drawing.backgroundColor = UIColor(hue:0.46, saturation:0.99, brightness:0.6, alpha:1)
        
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        
        EasyTipView.globalPreferences = preferences
        
        
        
    }
    
    func addImage(textfield:UITextField,img:UIImage){
        
       
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25 , height: 15))
        
        imageView.image = img
        
        textfield.rightView = imageView
        
        textfield.rightViewMode = .unlessEditing
            
       
        
    }
    
    func alertShow(selecteTF:UITextField,title:String)
    {
       selecteTF.font = UIFont(name: "System", size: 16.0)
        
        let wrongImage = UIImage(named: "wrong")
        addImage(textfield: selecteTF, img: wrongImage!)
        
        let tipView = EasyTipView(text: title, preferences: preferences)
        
        tipView.show(forView: selecteTF)
        
        tipView.backgroundColor = UIColor.black
        
       preferences.animating.dismissOnTap = true
        
        
    }
    
    func show(selecteTF:UITextField,title:String)
    {
        
        let tipView = EasyTipView(text: title, preferences: preferences)
        
        tipView.show(forView: selecteTF)
        
        tipView.backgroundColor = UIColor.black
        
        tipView.removeFromSuperview()
        
        //preferences.animating.dismissOnTap = true
        
        
    }
    
    func selectedTFFont(selecteTF:UITextField,colr:UIColor,title:String){
        selecteTF.font = UIFont(name: "System", size: 16.0)
        selecteTF.textColor = UIColor.darkGray
        selecteTF.text = ""

        
    }
    
    func deSelectFont(selecteTF:UITextField,colr:UIColor,title:String){
        
        selecteTF.font = UIFont(name: "System", size: 10)
        selecteTF.textColor = UIColor.red
        selecteTF.text = title
    }
   
    

}
