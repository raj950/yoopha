//
//  SplashViewControler.swift
//  VaniInstitute
//
//  Created by Raju Matta on 20/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var expandableView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.animate(withDuration: 5.0, animations: {
            self.expandableView.transform = CGAffineTransform(scaleX: 2.5, y: 1.8)
        }) { (true) in
            let initialVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.present(initialVC, animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }
    
    
    
}
