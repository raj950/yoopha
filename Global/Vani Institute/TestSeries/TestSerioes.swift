//
//  TestSerioes.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class TestSerioes: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        //dismiss(animated: false, completion: nil)
    }
    @IBAction func testSeries(_ sender: UIButton) {
        
        dismiss(animated: false, completion: nil)
    }
    @IBAction func gateCourse(_ sender: ASButton) {
        
        
        let gateCourseInfo = storyboard?.instantiateViewController(withIdentifier: "Publications") as! Publications
        gateCourseInfo.urlDemo = "https://www.aspirebuzz.com/vaniinstitute/gate-online-preparation?src=vaniinstitute"
        
        present(gateCourseInfo, animated: false, completion: nil)
    }
    
    
    @IBAction func IESCourse(_ sender: ASButton) {
        
        let gateCourseInfo = storyboard?.instantiateViewController(withIdentifier: "Publications") as! Publications
        gateCourseInfo.urlDemo = "https://www.aspirebuzz.com/vaniinstitute/gate-online-preparation?src=vaniinstitute"
        present(gateCourseInfo, animated: false, completion: nil)
    }
}
