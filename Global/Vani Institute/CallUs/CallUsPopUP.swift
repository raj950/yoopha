//
//  CallUsPopUP.swift
//  VaniInstitute
//
//  Created by Raju Matta on 12/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class CallUsPopUP: UIViewController {

    @IBOutlet weak var supportNumber: UILabel!
    @IBOutlet weak var callSupport: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        callSupport.text = CallsUsModle.supportName
        supportNumber.text = CallsUsModle.supportNumber
        self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var okAction: UILabel!
    
    @IBAction func ok(_ sender: Any) {
        self.view.removeFromSuperview()
        
    }
    
    @IBAction func callAction(_ sender: UIButton) {
       

    
        guard let url = URL(string : "tel://" + supportNumber.text!) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
   
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}
