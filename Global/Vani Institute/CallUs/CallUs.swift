//
//  CallUs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 12/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class CallUs: UIViewController {
    
    @IBOutlet weak var vani28yrs: UIImageView!
    
    var callUsArry:[CallsUsModle] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        callUs()
        
    }
    
    @IBAction func contactUsAction(_ sender: UIButton) {
        
        
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)    }
    
    func callUs(){
        
        Service.shared.GETService(extraParam: API.callUs) { (response) -> (Void) in
            
            print(response)
            
            guard let getCallNUmbers = response.dictionary else{return}
            
            if let allNumbers = getCallNUmbers["call_us"]!.array{
            
                for get in allNumbers{
                
                var custmerNo = CallsUsModle()
                
                custmerNo.technical = get["technical"].string!
                custmerNo.general = get["general"].string!
                custmerNo.suggestion = get["suggetion"].string!
                custmerNo.testSeries = get["test_series"].string!
                custmerNo.other = get["other"].string!
                self.callUsArry.append(custmerNo)
                
                
            }
            }
            print(self.callUsArry)
            
            
            
            
        }
        
        
    }
    
    
    
    @IBAction func callNumbers(_ sender: UIButton) {
        
        
        let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallUsPopUP") as! CallUsPopUP
        CallsUsModle.supportName = "Technical"
        
        if sender.tag == 1{
            
            let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallUsPopUP") as! CallUsPopUP
            CallsUsModle.supportName = "Technical"
            
            if let number = callUsArry[0].technical{
                CallsUsModle.supportNumber = number
                
            }
            self.addChild(POPUPVC)
            POPUPVC.view.frame = self.view.frame
            self.view.addSubview(POPUPVC.view)
            POPUPVC.didMove(toParent: self)
            
        }else if sender.tag == 2{
            
            let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallUsPopUP") as! CallUsPopUP
            CallsUsModle.supportName = "General"
            if let number = callUsArry[0].general{
                CallsUsModle.supportNumber = number
                
            }
            self.addChild(POPUPVC)
            POPUPVC.view.frame = self.view.frame
            self.view.addSubview(POPUPVC.view)
            POPUPVC.didMove(toParent: self)
            
            
            
        }else if sender.tag == 3{
            
            let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallUsPopUP") as! CallUsPopUP
            CallsUsModle.supportName = "suggestion"
            if let number = callUsArry[0].suggestion{
                CallsUsModle.supportNumber = number
                
            }
            self.addChild(POPUPVC)
            POPUPVC.view.frame = self.view.frame
            self.view.addSubview(POPUPVC.view)
            POPUPVC.didMove(toParent: self)
            
            
        }else if sender.tag == 4{
            let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallUsPopUP") as! CallUsPopUP
            CallsUsModle.supportName = "test series"
            if let number = callUsArry[0].testSeries{
                CallsUsModle.supportNumber = number
                
            }
            self.addChild(POPUPVC)
            POPUPVC.view.frame = self.view.frame
            self.view.addSubview(POPUPVC.view)
            POPUPVC.didMove(toParent: self)
            
        }
            
        else{
            
            let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallUsPopUP") as! CallUsPopUP
            CallsUsModle.supportName = "other"
            if let number = callUsArry[0].other{
                CallsUsModle.supportNumber = number
                
            }
            self.addChild(POPUPVC)
            POPUPVC.view.frame = self.view.frame
            self.view.addSubview(POPUPVC.view)
            POPUPVC.didMove(toParent: self)
            
        }
        
        
    }
    
    
    
}
