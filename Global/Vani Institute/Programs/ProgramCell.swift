//
//  ProgramCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class ProgramCell: UITableViewCell {

    @IBOutlet weak var programCell: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
