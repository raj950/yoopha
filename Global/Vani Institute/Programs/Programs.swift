//
//  Programs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import KRProgressHUD
class Programs: UIViewController {
    
    
    var getProgramInfo:[GetPrograms] = []
    @IBOutlet weak var programTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        getPrograms()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func programsAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
       // dismiss(animated: false, completion: nil)
    }
    
    func getPrograms(){
        
        Service.shared.GETService(extraParam: API.programmes) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            
            guard let contactUs = response.dictionary else {return}
            
            if let programs = contactUs["programmes"]!.array{
            
                for get in programs{
                
                var getPro = GetPrograms()
                getPro.id = get["id"].string!
                getPro.name = get["name"].string!
                getPro.description = get["description"].string!
                self.getProgramInfo.append(getPro)
                // self.contactUsTable.reloadData()
                
            }
            print(self.getProgramInfo)
            self.programTable.reloadData()
            }
            
        }
        
        
        
        
    }
}

extension Programs:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getProgramInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommonStrings.programCell, for: indexPath) as! ProgramCell
        
        cell.programCell.text = getProgramInfo[indexPath.row].name
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        GetPrograms.showDescription = getProgramInfo[indexPath.row].description
        GetPrograms.showName = getProgramInfo[indexPath.row].name
        
        
        let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: CommonStrings.descriptionPopUp) as! DescriptionPopUp
        self.addChild(POPUPVC)
        POPUPVC.view.frame = self.view.frame
        self.view.addSubview(POPUPVC.view)
        POPUPVC.didMove(toParent: self)
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
        
    }
    
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


