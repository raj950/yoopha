//
//  DescriptionPopUp.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class DescriptionPopUp: UIViewController {

    @IBOutlet weak var ok: UIButton!
    @IBOutlet weak var programDescription: UITextView!
    @IBOutlet weak var programName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        programName.text = GetPrograms.showName
        programDescription.text =
            GetPrograms.showDescription
        
        
 self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func OkAction(_ sender: Any) {
        
       // let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DescriptionPopUp") as! DescriptionPopUp
        //self.addChild(POPUPVC)
       self.view.removeFromSuperview()
//        POPUPVC.view.frame = self.view.frame
//        self.view.addSubview(POPUPVC.view)
//        POPUPVC.didMove(toParent: self)
        
        
      // self.dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
