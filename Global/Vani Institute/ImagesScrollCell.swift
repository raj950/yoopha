//
//  ImagesScrollCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 21/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class ImagesScrollCell: UICollectionViewCell {
    
    @IBOutlet weak var scrollImg: UIImageView!
}
