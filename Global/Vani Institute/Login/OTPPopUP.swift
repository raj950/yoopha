//
//  OTPPopUP.swift
//  VaniInstitute
//
//  Created by Raju Matta on 06/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import  KRProgressHUD

class OTPPopUP: UIViewController {

    @IBOutlet weak var toastMessage: UILabel!
    @IBOutlet weak var otpTextFiled: UITextField!
    var storedeOTP = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func ok(_ sender: Any) {
        
        if otpTextFiled.text?.isEmpty == false{
            
            KRProgressHUD.show()
            
        }
        
        Credentials.storeOTP.set(otpTextFiled.text, forKey: "OTP")
//        let parameters:Parameters = ["mobile":Constants.enteredMobileNo,"otp" : otpTextFiled.text]
        
        let parameters : Parameters = ["mobile":Credentials.storedMobile.string(forKey: "mobileNum"),"otp" : Credentials.storeOTP.string(forKey: "OTP")]
        
        if otpTextFiled.text?.isEmpty == true{
            
            self.toastMessage.makeToast("Enter OTP")
            
//            self.view.makeToast("", duration: 1.0, point: CGPoint(x: 200, y: 400), title: "enter OTP", image: nil)
//            { didTap in
//                if didTap {
//                    print("completion from tap")
//                } else {
//                    print("completion without tap")
//                }
//            }
//
            
        }
        else{
            
            print(parameters)
        Service.shared.POSTService(serviceType: API.verifyOTP, parameters: parameters as! [String : String]) { (response) -> (Void) in
            
            print(response)
            KRProgressHUD.dismiss()
            
            let success = response.dictionary
            
            if success!["message"]!.string! == "Invalid otp"{
                
                self.toastMessage.makeToast("Invalid OTP")
                
//                self.view.makeToast("", duration: 1.0, point: CGPoint(x: 200, y: 400), title: "invalid OTP", image: nil)
//                { didTap in
//                    if didTap {
//                        print("completion from tap")
//                    } else {
//                        print("completion without tap")
//                    }
//                }
                
            }
            
            else{
            
            if let result = success?["logindetails"]?.array{
                
                
                
                for i in result{
                    
                    var student = StudentProfile()
                    
                    
                    student.branch_id = i["branch_id"].string!
                    
                    StudentProfile.branchid = i["branch_id"].string!
                    Credentials.branchId.set(i["branch_id"].string!, forKey: "branchId")
                    student.branch_name = i["branch_name"].string
                    student.collage_name = i["collage_name"].string!
                    student.course_id = i["course_id"].string!
                    
                    student.course_name = i["course_name"].string!
                    
                    
                    
                    student.email = i["email"].string!
                    
                    Credentials.name.set(i["email"].string, forKey: "email")
                    StudentProfile.getEmail = student.email
                    student.fcm_token = i["fcm_token"].string
                    student.location_id = i["location_id"].string
                    student.location_name = i["location_name"].string
                    
                    Credentials.loc.set(i["location_name"].string, forKey: "loc")
                    
                    StudentProfile.getLocation = student.location_name
                    student.mobile = i["mobile"].string
                    
                    StudentProfile.getMobile = student.mobile
                    
                    
                    
                    student.name = i["name"].string
                    
                    Credentials.email.set(i["name"].string!, forKey: "name")
                    
                    StudentProfile.getName = student.name
                    
                    student.specialization_id = i["specialization_id"].string
                    student.specialization_name = i["specialization_name"].string
                    student.student_id = i["student_id"].string
                    
                    
                    StudentProfile.getUserId = student.student_id!
                    
                    Credentials.userId.set(student.student_id, forKey: "id")

                    student.studentImage = i["mobile"].string
                    
                    Credentials.storeImage.set(i["image"].string, forKey: "studentImg")
                    
                    student.study_year  = i["study_year"].string
                    
                    
                    StudentProfile.studentData.append(student)
                    
                }
            }
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: CommonStrings.mainVC)
            self.present(mainVC!, animated: true, completion: nil)
            
        }
            
        }
        }
        
//        let mainVC = storyboard?.instantiateViewController(withIdentifier: "MainVC")
//        present(mainVC!, animated: true, completion: nil)
//
        
    }
    
    @IBAction func resend(_ sender: Any) {
        
        let parameters:Parameters = ["mobile":Credentials.storedMobile.string(forKey: "mobileNum")]
        
        Service.shared.POSTService(serviceType: API.resendOTP, parameters: parameters as! [String : String]) { (response) -> (Void) in
            
            print(response)
            
        }
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
