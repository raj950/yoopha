//
//  Login.swift
//  VaniInstitute
//
//  Created by Raju Matta on 06/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Login: UIViewController {

    @IBOutlet weak var toastMessage: UILabel!
    @IBOutlet weak var loginTF: TextFieldHelper!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func login(_ sender: UIButton) {
        Credentials.storedMobile.set(loginTF.text, forKey: "mobileNum")
        if loginTF.text?.isEmpty == true{
            
            self.toastMessage.makeToast("Enter MobileNumber")
            
//            self.view.makeToast("", duration: 1.0, point: CGPoint(x: 200, y: 450), title: "enter Mobilenumber", image: nil)
//            { didTap in
//                if didTap {
//                    print("completion from tap")
//                } else {
//                    print("completion without tap")
//                }
//            }
            
            
        }
        
        else{
       Constants.enteredMobileNo = loginTF.text
        let parameters:Parameters = ["mobile":loginTF.text,"fcm_token":Constants.fcmtoken]
        
        Service.shared.POSTService(serviceType: API.login, parameters: parameters as! [String : String]) { (response) -> (Void) in
            
            print(response)
            
            guard let success = response.dictionary else {return}

            if success["message"]!.string! == "Invalid Mobile"{
                
                self.toastMessage.makeToast("Invalid MobileNumber")
                
//                self.view.makeToast("", duration: 1.0, point: CGPoint(x: 200, y: 450), title: "Invalid Mobilenumber", image: nil)
//                { didTap in
//                    if didTap {
//                        print("completion from tap")
//                    } else {
//                        print("completion without tap")
//                    }
//                }
                
            }
            
            
            else{
                
                let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: CommonStrings.optpopUp) as! OTPPopUP
                self.addChild(POPUPVC)
                POPUPVC.view.frame = self.view.frame
                self.view.addSubview(POPUPVC.view)
                POPUPVC.didMove(toParent: self)
                
            }
        
        }
        
        
        
        
        
    }
        
        
    }
}
