//
//  FAQS.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import KRProgressHUD
class FAQS: UIViewController {
    
    @IBOutlet weak var FAQSTableView: UITableView!
    var allFaqs:[getFAQS] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        getFAQSAll()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func faqsAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        
        //dismiss(animated: false, completion: nil)
    }
    
    func getFAQSAll(){
        
        
        
        Service.shared.GETService(extraParam: API.FAQ) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            
            guard let faq = response.dictionary else {return}
            
            if let getFaqs = faq["faq"]!.array{
                
                for get in getFaqs{
                    
                    var getFaq = getFAQS()
                    
                    getFaq.question = get["question"].string!
                    getFaq.answer = get["answer"].string!
                    self.allFaqs.append(getFaq)
                    
                    
                }
            }
            print(self.allFaqs)
            self.FAQSTableView.reloadData()
            
            
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension FAQS:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allFaqs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommonStrings.faqsCell, for: indexPath) as! FAQSCell
        
        cell.name.text = allFaqs[indexPath.row].question
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        getFAQS.showAnswer = allFaqs[indexPath.row].answer
        getFAQS.showName = allFaqs[indexPath.row].question
        
        
        let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FAQSPopUp") as! FAQSPopUp
        self.addChild(POPUPVC)
        POPUPVC.view.frame = self.view.frame
        self.view.addSubview(POPUPVC.view)
        POPUPVC.didMove(toParent: self)
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
