//
//  FAQSPopUp.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class FAQSPopUp: UIViewController {

    @IBOutlet weak var showTextView: UIView!
    @IBOutlet weak var FAQDes: UITextView!
    @IBOutlet weak var nameFAQ: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        FAQDes.text = getFAQS.showAnswer
        nameFAQ.text = getFAQS.showName
        showTextView.layer.cornerRadius = 10
        
        self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func OkAction(_ sender: Any) {
        
        self.view.removeFromSuperview()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
