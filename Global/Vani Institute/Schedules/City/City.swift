//
//  City.swift
//  VaniInstitute
//
//  Created by Raju Matta on 27/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

protocol passCity {
    
    func passCityName(cityName:String?,locId:String?)
    
}


class City: UIViewController {

    var cityPro:passCity?
    @IBOutlet weak var cityLoadTableView: UITableView!
     var getStore:[Location] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)

    getCity()
        
        // Do any additional setup after loading the view.
    }
    




func getCity(){
    

    let parameters:Parameters = ["table":"location"]
    
    Service.shared.POSTService(serviceType: API.dynamictable, parameters: parameters as! [String : String]) { (response) -> (Void) in
        
        print(response)
        
        let success = response.dictionary
        
        if let result = success!["result"]?.array
        {
    
        
        for i in result{
            
            var getLocation = Location()
            getLocation.name = i["name"].string!
            getLocation.id = i["id"].string!
            if  getLocation.name != "--Select Location--" && getLocation.name != "Other City"{
                self.getStore.append(getLocation)

            }else{
            }
            
        }
    }
        print(self.getStore)
        self.cityLoadTableView.reloadData()

    }
}
}
extension City:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(getStore)
        return getStore.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell")as! CityCell
        cell.nameCity?.text = getStore[indexPath.row].name
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        cityPro?.passCityName(cityName: getStore[indexPath.row].name, locId: getStore[indexPath.row].id)
          self.view.removeFromSuperview()
    
        
    }
    
    
    
    
}
