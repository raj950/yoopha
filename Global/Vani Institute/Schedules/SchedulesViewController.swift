//
//  SchedulesViewController.swift
//  VaniInstitute
//
//  Created by Raju Matta on 06/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import WebKit

class SchedulesViewController: UIViewController{

    
    @IBOutlet weak var schedulesWebView: WKWebView!
    var urlDemo : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        let urlString = urlDemo
        let request = URLRequest(url: URL(string: urlString!)!)
        self.schedulesWebView.load(request)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func scheduleAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        //dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


