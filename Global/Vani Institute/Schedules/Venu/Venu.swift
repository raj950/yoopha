//
//  Venu.swift
//  VaniInstitute
//
//  Created by Raju Matta on 27/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import Toast_Swift

protocol passVenu {
    
    func passVenuName(cityName:String?,locId:String?)
    
}

class Venu: UIViewController {

    var getStore:[Location] = []
    @IBOutlet weak var venuTBL: UITableView!
    
    var falure:Int?
    var venues:passVenu?
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)

        getVenu()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func getVenu(){
        
        
        let parameters:Parameters = ["sid":Location.selectedLocId as Any]
        
        Service.shared.POSTService(serviceType:API.getVenu , parameters: parameters as! [String : String]) { (response) -> (Void) in
            
          
            
            let success = response.dictionary
            
            if success!["success"] == 0{
                
            print("NO records found")
                
                self.dismiss(animated: true, completion: nil)
            
               
                self.falure = success!["success"]?.int
                self.view.makeToast("", duration: 1.0, point: CGPoint(x: 200, y: 600), title: " NO records found", image: nil)
                {
                    didTap in
                    if didTap {
                        print("completion from tap")
                    } else {
                        print("completion without tap")
                    }
                }
                self.venuTBL.reloadData()

            }
            
            
          else
          {
            
            if let result = success!["result"]?.array{
            for i in result{
                
                var getLocation = Location()
                getLocation.name = i["name"].string!
                getLocation.id = i["id"].string!
               
                    self.getStore.append(getLocation)
            
                
            }
            
            print(self.getStore)
                self.venuTBL.reloadData()

            }
            
            
            
    }
           
            
            
        }
    }
}
extension Venu:UITableViewDelegate,UITableViewDataSource{
   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if getStore.count == 0{
            //self.view.removeFromSuperview()
            self.venuTBL.isHidden = true
           // self.venuTBL.backgroundColor = UIColor.clear
            self.dismiss(animated: true, completion: nil)
            
            
        return getStore.count
        }else{
            self.venuTBL.isHidden = false
            
            return getStore.count
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VenuCell")as! VenuCell
        cell.venuCell?.text = getStore[indexPath.row].name
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    venues?.passVenuName(cityName: getStore[indexPath.row].name, locId: getStore[indexPath.row].id)
       
        self.view.removeFromSuperview()
        
        
    }
    
    
    
    
}
