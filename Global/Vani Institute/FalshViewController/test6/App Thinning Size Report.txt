
App Thinning Size Report for All Variants of Vani Institute

Variant: Vani Institute-19BD8F8C-2903-4919-B01D-4DED37D1BB70.ipa
Supported variant descriptors: [device: iPhone8,2, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-253BB57E-20CD-4F08-B7AA-6E436A86C3E7.ipa
Supported variant descriptors: [device: iPhone7,1, os-version: 11]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-68B4DAE4-C948-4BA5-972B-3D3E403E32F0.ipa
Supported variant descriptors: [device: iPad8,2, os-version: 12], [device: iPad8,4, os-version: 12], [device: iPad7,1, os-version: 12], [device: iPad8,8, os-version: 12], [device: iPad8,5, os-version: 12], [device: iPad6,4, os-version: 12], [device: iPad7,2, os-version: 12], [device: iPad7,3, os-version: 12], [device: iPad8,3, os-version: 12], [device: iPad8,7, os-version: 12], [device: iPad8,1, os-version: 12], [device: iPad7,4, os-version: 12], [device: iPad8,6, os-version: 12], and [device: iPad6,3, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-68C3146F-5B46-4AE9-A827-BADE981FA3CC.ipa
Supported variant descriptors: [device: iPhone11,8, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-7196AFDA-1A46-44E5-B9EF-82E0D4402497.ipa
Supported variant descriptors: [device: iPad4,7, os-version: 12], [device: iPad4,9, os-version: 12], [device: iPad7,5, os-version: 12], [device: iPad4,6, os-version: 12], [device: iPad6,12, os-version: 12], [device: iPad5,2, os-version: 12], [device: iPad4,8, os-version: 12], [device: iPad5,3, os-version: 12], [device: iPad6,8, os-version: 12], [device: iPad5,1, os-version: 12], [device: iPad4,5, os-version: 12], [device: iPad4,4, os-version: 12], [device: iPad6,7, os-version: 12], [device: iPad5,4, os-version: 12], [device: iPad4,1, os-version: 12], [device: iPad4,2, os-version: 12], [device: iPad6,11, os-version: 12], [device: iPad4,3, os-version: 12], and [device: iPad7,6, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-98A0E378-E511-4E81-9A8B-B05E7DBEAE2C.ipa
Supported variant descriptors: [device: iPhone10,4, os-version: 12], [device: iPhone9,1, os-version: 12], [device: iPhone9,3, os-version: 12], and [device: iPhone10,1, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-ABFCFD9E-41CF-4BD7-A0F9-10111AC6C945.ipa
Supported variant descriptors: [device: iPhone10,3, os-version: 12], [device: iPhone11,4, os-version: 12], [device: iPhone10,6, os-version: 12], [device: iPhone9,2, os-version: 12], [device: iPhone10,2, os-version: 12], [device: iPhone10,5, os-version: 12], [device: iPhone11,6, os-version: 12], [device: iPhone9,4, os-version: 12], and [device: iPhone11,2, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute-AFB42C30-9A1C-46A9-BDEA-5C0178002CB1.ipa
Supported variant descriptors: [device: iPhone7,2, os-version: 12], [device: iPhone6,2, os-version: 12], [device: iPhone6,1, os-version: 12], [device: iPod7,1, os-version: 12], [device: iPhone8,1, os-version: 12], and [device: iPhone8,4, os-version: 12]
App + On Demand Resources size: 16.5 MB compressed, 62.6 MB uncompressed
App size: 16.5 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Vani Institute.ipa
Supported variant descriptors: Universal
App + On Demand Resources size: 24.3 MB compressed, 62.6 MB uncompressed
App size: 24.3 MB compressed, 62.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed
