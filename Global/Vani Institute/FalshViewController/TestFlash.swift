//
//  TestFlash.swift
//  Vani Institute
//
//  Created by Raju Matta on 27/03/19.
//  Copyright © 2019 Vani Institute. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import WebKit
class TestFlash: UIViewController {
    var player: AVPlayer?

    @IBOutlet weak var webtest: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
     playVideo()
    }
    
    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "flash", ofType:".mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
