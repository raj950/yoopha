//
//  SplashViewController.swift
//  VaniInstitute
//
//  Created by Raju Matta on 21/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
class SplashViewController: UIViewController {

    @IBOutlet weak var expandableView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.animate(withDuration: 3.0, animations: {
            self.expandableView.transform = CGAffineTransform(scaleX: 2.5, y: 1.8)
        }) { (true) in
            if(Credentials.userId.string(forKey: "id") != nil)
            {
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
             self.present(mainVC, animated: true, completion: nil)
            }
            else{
            let initialVC = self.storyboard?.instantiateViewController(withIdentifier: "GoToApp") as! GoToApp
            self.present(initialVC, animated: true, completion: nil)
        }
        }
        // Do any additional setup after loading the view.
   }

    
    
}
