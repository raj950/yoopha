//
//  GoToApp.swift
//  VaniInstitute
//
//  Created by Raju Matta on 24/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import KRProgressHUD
class GoToApp: UIViewController {

    @IBOutlet weak var vani28yrs: UIImageView!
    @IBOutlet weak var newstable: UITableView!
    
    var newsArray:[getNewsVani] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        getNews()
        
        self.navigationController?.isNavigationBarHidden = true
     
    }
    
    @IBAction func newsAction(_ sender: UIButton) {
        
        exit(0)
    }
    
    
    
    func getNews(){
        
        Service.shared.GETService(extraParam: API.news) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            
           guard let newsGet = response.dictionary else { return }
            
           if let allNews = newsGet["news"]!.array{

            for get in allNews{
                
                var vaniNews = getNewsVani()
                print(get["image"].string)
                vaniNews.image = get["image"].string!
                vaniNews.title = get["title"].string
                vaniNews.description = get["description"].string
                vaniNews.postdate = get["postdate"].string
                
                self.newsArray.append(vaniNews)
                
                
            }
            self.newstable.reloadData()
        }
            
        }
        
        
    }
    
    
}


extension GoToApp:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GoToAppCell")as! GoToAppCell
        cell.topperLabel.text = newsArray[indexPath.row].title
        
        let dateTrim = newsArray[indexPath.row].postdate?.prefix(11)
        cell.postDateLabel.text = String(dateTrim!)
        
        let trimDescription = newsArray[indexPath.row].description?.prefix(16)
        
        
        
        cell.descriptionLbel.text = String(trimDescription!)
        cell.continueReading.text = "Continue Reading"
        
        print(newsArray[indexPath.row].image)
        
        
         let filteringString = newsArray[indexPath.row].image
        
        let filteredString = String(filteringString.filter{!" ".contains($0)})
        
        
        let imageRes = ImageResource(downloadURL: URL(string: filteredString)!)
        
        cell.studentProfileImg.kf.setImage(with: imageRes)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        
        let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GoToAppPopUp") as! GoToAppPopUp
        getNewsVani.titleName = newsArray[indexPath.row].title
        getNewsVani.datePost = newsArray[indexPath.row].postdate
        getNewsVani.des = newsArray[indexPath.row].description
        self.addChild(POPUPVC)
        POPUPVC.view.frame = self.view.frame
        self.view.addSubview(POPUPVC.view)
        POPUPVC.didMove(toParent: self)
        
      
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    
}
