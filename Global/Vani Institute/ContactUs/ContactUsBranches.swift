//
//  ContactUsBranches.swift
//  Vani Institute
//
//  Created by Raju Matta on 20/03/19.
//  Copyright © 2019 Vani Institute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KRProgressHUD
class ContactUsBranches: UIViewController {
    var alllocs:[SelectedLocInfo] = []

    var branchNames:[String] = []
   // var branchId:[String] = []
    @IBOutlet weak var contactBranchTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        var frame = contactBranchTableView.frame
//        frame.size.height = contactBranchTableView.contentSize.height
//        contactBranchTableView.frame = frame
KRProgressHUD.show()
        getLocationSelected()
        self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)

        // Do any additional setup after loading the view.
    }
    
    func getLocationSelected(){
        
                Service.shared.GETService(extraParam:"https://www.vaniinstitute.com/app/services/get_branch.php?location_id=\(Locations.selectedLocId)") { (response) -> (Void) in
        
        
                    KRProgressHUD.dismiss()
                    print(response)
        
                    guard let getDetails = response.dictionary else{return}
        
                    print(getDetails)
        
                    if let allDetails = getDetails["branch"]?.array{
        
                        for get in allDetails{
        
                        var loc = SelectedLocInfo()
                        loc.name = get["name"].string!
                        loc.attachment = get["attachment"].string!
                        loc.latitude = get["latitude"].string!
                        loc.longitude = get["longitude"].string!
//                        Locations.latitude = get["latitude"].string!
//                        Locations.logitude = get["longitude"].string!
                         
        
        
                        loc.location = get["location"].string!
                        loc.address = get["address"].string!
                        loc.office_number = get["office_number"].string!
                        loc.mobile = get["mobile"].string!
                        loc.email = get["email"].string!
                        self.alllocs.append(loc)
        
                       
        
                    }
        
        self.contactBranchTableView.reloadData()
                       // contactBranchTableView.reloadData()
                        self.contactBranchTableView.layoutIfNeeded()
                        self.contactBranchTableView.heightAnchor.constraint(equalToConstant: self.contactBranchTableView.contentSize.height).isActive = true
                }
                }
        
    }
    
    

}

extension ContactUsBranches:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return alllocs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "BracnhesCellContact")as! BracnhesCellContact
        cell.branchNameLabl.text = alllocs[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let selectedLoc = storyboard?.instantiateViewController(withIdentifier: "DetailsContactUs")as! DetailsContactUs
        
        
                        selectedLoc.city = alllocs[indexPath.row].location
                       selectedLoc.adress = alllocs[indexPath.row].address
                       selectedLoc.office = alllocs[indexPath.row].office_number
                       selectedLoc.mobile = alllocs[indexPath.row].mobile
                       selectedLoc.web = alllocs[indexPath.row].email
                       selectedLoc.locimg = alllocs[indexPath.row].attachment
        print(alllocs[indexPath.row].latitude!)
        print(alllocs[indexPath.row].longitude!)
        Locations.latitude = alllocs[indexPath.row].latitude!
        Locations.logitude = alllocs[indexPath.row].longitude!

        
        
        
        //Locations.selectedLocId = branchId[indexPath.row]
        present(selectedLoc, animated: false, completion: nil)
        
        self.view.removeFromSuperview()
        
    }
    
   
}
