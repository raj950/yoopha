//
//  DetailsContactUs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 17/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import SwiftyJSON
import Kingfisher
import KRProgressHUD

class DetailsContactUs: UIViewController {

    @IBOutlet weak var mapingView: UIView!
    @IBOutlet weak var vani28yrs: UIImageView!
    @IBOutlet weak var webAdress: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var officeNumber: UILabel!
    @IBOutlet weak var adressLbl: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var locImage: UIImageView!
    
    
    var web:String?
    var mobile:String?
    var office:String?
    var adress:String?
    var city:String?
    var locimg:String?
   
    var selectedLoc:[Locations] = []
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var alllocs:[SelectedLocInfo] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        KRProgressHUD.show()
      
        getLocationSelected()


        
        
    }
    

    @IBAction func backGo(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    func getLocationSelected(){
        
     self.locationManager.requestWhenInUseAuthorization()

                    if CLLocationManager.locationServicesEnabled() {
                        self.locationManager.delegate = self
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        self.locationManager.startUpdatingLocation()

                    }
        let imageRes = ImageResource(downloadURL: URL(string: locimg!)!)

                self.locImage?.kf.setImage(with: imageRes)
               self.cityName.text = city!.capitalized
                self.adressLbl.text = adress
                self.officeNumber.text = office
                self.mobileNumber.text = mobile
                self.webAdress.text = web
        

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DetailsContactUs : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

       
        let lat = Locations.latitude.trimmingCharacters(in: CharacterSet.whitespaces)
        
        
        print(lat)
        let long = Locations.logitude
        print(long)


        if currentLocation == nil {


            currentLocation = CLLocation(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!)

            let camera = GMSCameraPosition.camera(withLatitude: (currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)! , zoom: 19.0)
            let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: 414, height: 166), camera: camera)
           // self.view.addSubview(mapView)

            self.mapingView.addSubview(mapView)
            KRProgressHUD.dismiss()


            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(lat)! , CLLocationDegrees(long)!)

            print(marker.position)

            marker.map = mapView

           
        }
    }
}
