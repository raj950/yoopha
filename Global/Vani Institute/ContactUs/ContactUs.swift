//
//  ContactUs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import KRProgressHUD
class ContactUs: UIViewController {

    
    @IBOutlet weak var vani28yrs: UIImageView!
    @IBOutlet weak var contactUsTable: UITableView!
    var getLocationsAll:[Locations] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        KRProgressHUD.show()
//        vani28yrs.layer.cornerRadius = 14
        //vani28yrs.clipsToBounds = true
      getContactUs()
        
        contactUsTable.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func contactUsAction(_ sender: UIButton) {
        
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
       // dismiss(animated: false, completion: nil)
    }
    func getContactUs(){
        
        
        
            Service.shared.GETService(extraParam: API.getoLcation) { (response) -> (Void) in
                
                
                KRProgressHUD.dismiss()
                print(response)
                
                guard let contactUs = response.dictionary else{return}
                
                if let locations = contactUs["location"]!.array
               {
               
                    for getloc in locations{
                        
                        var getLocation = Locations()
                        getLocation.locationsId = getloc["id"].string!
                        getLocation.locationsName = getloc["name"].string!
                        getLocation.locationImage = getloc["icon"].string!
                        self.getLocationsAll.append(getLocation)
                        
                       // Locations.getLocationsAll.append(getLocation)
                        
                        self.contactUsTable.reloadData()
                        
                        
                        
                        
                        
                    }
        }
                
                
        }
//            DispatchQueue.main.async {
//                print(self.getLocationsAll)
//                self.contactUsTable.reloadData()
//            }
//
        
        }
        
        
        
        
    }


extension ContactUs:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getLocationsAll.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsCell", for: indexPath) as! ContactUsCell
        cell.locationName?.text = getLocationsAll[indexPath.row].locationsName
        cell.locationImage.clipsToBounds = true;
        
        cell.locationImage.layer.cornerRadius = 20
        cell.locationImage.layer.borderWidth = 1.0
        cell.locationImage.layer.borderColor = UIColor.black.cgColor
        let filteringString = getLocationsAll[indexPath.row].locationImage
        
        let filteredString = String(filteringString!.filter{!" ".contains($0)})
        let imageRes = ImageResource(downloadURL: URL(string: filteredString)!)
           // cell.locationImage!.kf.setImage(with: imageRes)
        cell.locationImage?.kf.setImage(with: imageRes, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, URL) in
            cell.setNeedsLayout()
        })
       
            cell.accessoryType = .disclosureIndicator
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            Locations.selectedLocId = getLocationsAll[indexPath.row].locationsId!

        
        let POPUPVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUsBranches") as! ContactUsBranches
        
        self.addChild(POPUPVC)
        POPUPVC.view.frame = self.view.frame
        
       
        self.view.addSubview(POPUPVC.view)
        POPUPVC.didMove(toParent: self)
        
//            let selectedLoc = storyboard?.instantiateViewController(withIdentifier: "DetailsContactUs")as! DetailsContactUs
//            Locations.selectedLocId = getLocationsAll[indexPath.row].locationsId!
//            present(selectedLoc, animated: false, completion: nil)
            
            
    
     
       
//       let selectedLoc = storyboard?.instantiateViewController(withIdentifier: "DetailsContactUs")as! DetailsContactUs
//       Locations.selectedLocId = getLocationsAll[indexPath.row].locationsId!
//      present(selectedLoc, animated: false, completion: nil)
//
        
        
    }
    
    
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


