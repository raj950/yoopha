//
//  MainVC.swift
//  VaniInstitute
//
//  Created by Raju Matta on 06/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SideMenuSwift
import JTMaterialTransition
import Kingfisher
import StoreKit

class MainVC: UIViewController {
    
    @IBOutlet weak var vani28yrsImage: UIImageView!
    @IBOutlet weak var footerLable: UILabel!
    weak var presentControllerButton: UICollectionViewCell?
    var transition: JTMaterialTransition?
    @IBOutlet weak var mainCollectionView: UICollectionView!
    var flashtext:String?
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var newsLabel: UILabel!
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    var namesArray = ["ABOUT US","SCHEDULES","ACHIEVEMENTS","TEST SERIES","NEWS","LIBRARY","FAQS","SUCCESS STORIES","CONTACT US","PROGRAMS","BATCHES","PUBLICATIONS"]
    var sideMenuList = ["MY PROFILE","INVITE FRIENDS","ONLINE ENROLLMENT","FOLLOW US","RATE US","HELP US IMPROVE","CALL US","CHAT WITH US","LOGOUT"]
    var sideMenuIcons = [#imageLiteral(resourceName: "profile_unselected"),#imageLiteral(resourceName: "invite_unselected"),#imageLiteral(resourceName: "web"),#imageLiteral(resourceName: "follow_us"),#imageLiteral(resourceName: "rate_unselected"),#imageLiteral(resourceName: "help_unselected"),#imageLiteral(resourceName: "call_us_unselected"),#imageLiteral(resourceName: "chat_unselected"),#imageLiteral(resourceName: "logout_unselected")]
    
    var imageArray = [#imageLiteral(resourceName: "about_us"),#imageLiteral(resourceName: "schedule"),#imageLiteral(resourceName: "achievements"),#imageLiteral(resourceName: "test"),#imageLiteral(resourceName: "news_icon"),#imageLiteral(resourceName: "library"),#imageLiteral(resourceName: "faq"),#imageLiteral(resourceName: "success"),#imageLiteral(resourceName: "contact-2"),#imageLiteral(resourceName: "program"),#imageLiteral(resourceName: "batch"),#imageLiteral(resourceName: "vani_pub")]
    var bannerArray = [String]()
    var bannerCount = 0
    var timer:Timer?
    override func viewWillAppear(_ animated: Bool) {
        getUpdates { (true) in
            if self.timer?.isValid == false{
                self.timer = Timer(timeInterval: 6.0, target: self, selector: #selector(self.changeNewsUpdates), userInfo: nil, repeats: true)
                RunLoop.current.add(self.timer!, forMode: RunLoop.Mode.common)
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.navigationBar.isHidden = true
        footerLable.footerFontSize(lableName: footerLable, viewsize: self.view)
       // self.newsLabel.startBlink()
        sideMenuView.isHidden = true
        
        mainCollectionView.backgroundColor = .clear
        self.bannerArray.removeAll()
        getUpdates { (true) in
            self.timer = Timer(timeInterval: 6.0, target: self, selector: #selector(self.changeNewsUpdates), userInfo: nil, repeats: true)
            RunLoop.current.add(self.timer!, forMode: RunLoop.Mode.common)
        }
        
        
    }
    
    @objc func changeNewsUpdates(){
        print("timer triggered")
        self.newsLabel.stopBlink()
        if bannerArray.count > 0{
            if bannerCount != bannerArray.count{
                self.newsLabel.stopBlink()
                UIView.animate(withDuration: 0.3) {
                    self.newsLabel.text = self.bannerArray[self.bannerCount]
                }
                bannerCount += 1
                self.newsLabel.startBlink()
            }else{
                bannerCount = 0
            }
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.bannerArray.removeAll()
        self.timer?.invalidate()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.transition(with: sideMenuView, duration: 30, options: .curveEaseInOut, animations: {
            self.sideMenuView.isHidden = true
            
        })
    }
    @IBAction func revealSidemenu(_ sender: Any) {
        
        
        
        if sideMenuView.isHidden == true{
            UIView.transition(with: sideMenuView, duration: 30, options: .curveEaseInOut, animations: {
                self.sideMenuView.isHidden = false
                
            })
        }
            
        else{
            
            UIView.transition(with: sideMenuView, duration: 30, options: .curveEaseInOut, animations: {
                self.sideMenuView.isHidden = true
                
            })
            
            
        }
        sideMenuTableView.reloadData()
    }
    
    
    
    
    
    func getUpdates(completion: @escaping (Bool)->()){
        
        Service.shared.GETService(extraParam: API.updates) { (response) -> (Void) in
            
            guard  let getBanners = response.dictionary else {return}
            
            if let allBanners = getBanners["updates"]!.array{
                
                for i in 0..<allBanners.count{
                        self.bannerArray.append(allBanners[i]["title"].stringValue)
                   }
                print(self.bannerArray)
                 completion(true)
            }
            
            
        }
    }
    
 
    
}
extension MainVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIViewControllerTransitioningDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCell", for: indexPath) as! MainCell
        cell.mainNames.text = namesArray[indexPath.row]
        cell.mainNames.fontSize(lableName: cell.mainNames, viewsize: self.view)
        
        
        cell.mainViewImage.image = imageArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((mainCollectionView.bounds.width)/3)-15, height: ((mainCollectionView.bounds.height)/4)-15)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(namesArray[indexPath.row])
        
        if namesArray[indexPath.row] == "ABOUT US"{
            
            let aboutUs = storyboard?.instantiateViewController(withIdentifier: CommonStrings.aboutUs)
            
            let cell = UICollectionViewCell()
            self.transition = JTMaterialTransition(animatedView: cell)
            let controller = aboutUs
            
            controller!.modalPresentationStyle = .formSheet
            controller!.transitioningDelegate = self.transition
            
            present(aboutUs!, animated: true, completion: nil)
            
        }else if namesArray[indexPath.row] == "SCHEDULES"{
            
            let schedulesVC = storyboard?.instantiateViewController(withIdentifier: CommonStrings.schedules) as! SchedulesViewController
            schedulesVC.urlDemo = "https://www.vaniinstitute.com/schedules.php"
            present(schedulesVC, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "ACHIEVEMENTS"{
            
            let achievementsVC = storyboard?.instantiateViewController(withIdentifier: CommonStrings.achivements)
            present(achievementsVC!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "TEST SERIES"{
            let testSeriesVC = storyboard?.instantiateViewController(withIdentifier: CommonStrings.testSeries)
            present(testSeriesVC!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "NEWS"{
            let newsVC = storyboard?.instantiateViewController(withIdentifier: CommonStrings.news)
            present(newsVC!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "LIBRARY"{
            let libraryVC = storyboard?.instantiateViewController(withIdentifier: CommonStrings.library)
            present(libraryVC!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "FAQS"{
            let aboutUs = storyboard?.instantiateViewController(withIdentifier: CommonStrings.faqs)
            present(aboutUs!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "SUCCESS STORIES"{
            
            let aboutUs = storyboard?.instantiateViewController(withIdentifier: CommonStrings.successStories)
            present(aboutUs!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "CONTACT US"{
            
            let aboutUs = storyboard?.instantiateViewController(withIdentifier: CommonStrings.contactUs)
            present(aboutUs!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "PROGRAMS"{
            
            let aboutUs = storyboard?.instantiateViewController(withIdentifier: CommonStrings.programs)
            present(aboutUs!, animated: true, completion: nil)
        }else if namesArray[indexPath.row] == "BATCHES"{
            
            let aboutUs = storyboard?.instantiateViewController(withIdentifier: CommonStrings.batches) as! Batches
            aboutUs.urlDemo = "https://www.vaniinstitute.com/gate-ese-batches.php"
            
            present(aboutUs, animated: true, completion: nil)
        }
        else{
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: CommonStrings.publications) as! Publications
            publicationsVC.urlDemo = "http://vanipublications.in"
            present(publicationsVC, animated: true, completion: nil)
            
        }
        
    }
}
extension MainVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return sideMenuList.count + 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
            
            if Credentials.userId.string(forKey: "id")
                != nil{
            
                Service.shared.GETService(extraParam: API.profile + Credentials.userId.string(forKey: "id")!) { (response) -> (Void) in
                    
                    guard  let getData = response.dictionary else {return}
                    
                    if let studentInfo = getData["profile_details"]!.array{
                        
                        for get in studentInfo{
                            
                            let studentDic = get.dictionary
                            
                           // Credentials.storeImage.set(studentDic!["image"]!.string, forKey: "studentImg")
                            
                            let imageRes = ImageResource(downloadURL: URL(string: studentDic!["image"]!.string!)!)
                            
                            print(imageRes)
                            
                            cell.profilePic.kf.setImage(with: imageRes)
                            
                            cell.profilePic.layer.cornerRadius = 50
                            cell.profilePic.clipsToBounds = true
                            cell.name.text = studentDic!["name"]!.string!
                            cell.emailId.text = studentDic!["email"]!.string!
                            
                        }
                    }
                }
                
                
            }
            
            
          return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuListCell") as! SideMenuListCell
            
            cell.sideMenuIcon.image = sideMenuIcons[indexPath.row - 1]
            cell.sideMenuName.text = sideMenuList[indexPath.row - 1]
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            
            return 180
            
        }else{
            return 60
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 8{
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "ChatwithUs") as! ChatwithUs
            present(publicationsVC, animated: false, completion: nil)
            
            
        }else if indexPath.row == 9{
            
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            present(publicationsVC, animated: false, completion: nil)
            
            
        }
        else if indexPath.row == 7{
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "CallUs") as! CallUs
            present(publicationsVC, animated: false, completion: nil)
            
            
        }
        else if indexPath.row == 6{
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "HelpUs") as! HelpUs
            present(publicationsVC, animated: false, completion: nil)
            
            
        }
            
        else if indexPath.row == 3{
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "OnlineEnrollment") as! OnlineEnrollment
            present(publicationsVC, animated: true, completion: nil)
            
            
            
            
        }else if indexPath.row == 1{
            
            let publicationsVC = storyboard?.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
            present(publicationsVC, animated: true, completion: nil)
            
            
        }
            
        else if indexPath.row == 2{
            
            let activityVc = UIActivityViewController(activityItems: ["hey check out vaniInstitute app at:https://itunes.apple.com/us/app/VANI INSTITUTE/id1455303627?ls=1&mt=8"], applicationActivities: nil)
            activityVc.popoverPresentationController?.sourceView = self.view
            self.present(activityVc, animated: true, completion: nil)
            
        }
        else if indexPath.row == 5{
            let url = URL(string: "itms-apps://itunes.apple.com/app/id1455303627")
            if UIApplication.shared.canOpenURL(url!) == true { UIApplication.shared.openURL(url!)
                
            }
            
   
        }
            
         else if indexPath.row == 4{
            
            
            let followUs = UIAlertController(title: "FOLLOW US", message: "", preferredStyle: .actionSheet)
            
            followUs.addAction(UIAlertAction(title: "Facebook", style: .default , handler:{ (UIAlertAction)in
                
                
                let gateCourseInfo = self.storyboard?.instantiateViewController(withIdentifier: "Publications") as! Publications
                gateCourseInfo.urlDemo = "https://www.facebook.com/vaniinstituteofficial/"
                
                self.present(gateCourseInfo, animated: false, completion: nil)
                
            }))
            
            followUs.addAction(UIAlertAction(title: "Twitter", style: .default , handler:{ (UIAlertAction)in
                let gateCourseInfo = self.storyboard?.instantiateViewController(withIdentifier: "Publications") as! Publications
                gateCourseInfo.urlDemo = "https://twitter.com/_vaniinstitute_"
                
                self.present(gateCourseInfo, animated: false, completion: nil)
            }))
            //
            followUs.addAction(UIAlertAction(title: "Youtube", style: .default , handler:{ (UIAlertAction)in
                let gateCourseInfo = self.storyboard?.instantiateViewController(withIdentifier: "Publications") as! Publications
                gateCourseInfo.urlDemo = "https://www.youtube.com/watch?v=kc_8YPz4D7I&amp;feature=youtu.be"
                
                self.present(gateCourseInfo, animated: false, completion: nil)
            }))
            
            
            followUs.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(followUs, animated: true, completion:nil)
            
            
            
            
        }
        
        
    }
    
    
    
    func getNewInfo(){
        
        
       
    }
    
}
