//
//  SideMenuListCell.swift
//  VaniInstitute
//
//  Created by Shivakumar Sunkari on 10/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class SideMenuListCell: UITableViewCell {

    @IBOutlet weak var sideMenuName: UILabel!
    @IBOutlet weak var sideMenuIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
