//
//  MainCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class MainCell: UICollectionViewCell {
    @IBOutlet weak var mainViewImage: UIImageView!
    
    @IBOutlet weak var mainNames: UILabel!
}
