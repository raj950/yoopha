//
//  HelpUs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 19/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class HelpUs: UIViewController {
    
    @IBOutlet weak var toastMessage: UILabel!
    @IBOutlet weak var vani28yrs: UIImageView!
    @IBOutlet weak var helpUsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        helpUsTextView.delegate = self
        helpUsTextView.text = "Type your text here...."
        helpUsTextView.textColor = UIColor.lightGray
        
        helpUsTextView.layer.borderColor = UIColor.black.cgColor
        helpUsTextView.layer.borderWidth = 0.5
        helpUsTextView.layer.cornerRadius = 5.0
        
    }
    
    @IBAction func clearAction(_ sender: Any) {
        
        helpUsTextView.text = nil
        
    }
    
    @IBAction func backGo(_ sender: UIButton) {
        
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        
    }
    @IBAction func submitAction(_ sender: Any) {
        
        print(StudentProfile.getUserId)
        print(helpUsTextView.text.count)
        if  helpUsTextView.text == "Type your text here...."{
            
            self.toastMessage.makeToast("Enter Mandatory Fields")
       
            
        }else{
            let parameters:Parameters = ["user_id":Credentials.userId.string(forKey: "id"),"comment":helpUsTextView.text]
            
            
            Service.shared.POSTService(serviceType: API.help_us, parameters: parameters as! [String : String]) { (response) -> (Void) in
                
                
                print(response)
                let success = response.dictionary
                
                if success!["success"] == 1{
                    
                    self.helpUsTextView.text = ""
                    
                    self.toastMessage.makeToast("Thank You For FeedBack")

                    
                    
                }
            }
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension HelpUs:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if helpUsTextView.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    
}

