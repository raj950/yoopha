//
//  OnlineEnrollment.swift
//  VaniInstitute
//
//  Created by Raju Matta on 13/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import WebKit
class OnlineEnrollment: UIViewController {

    @IBOutlet weak var vani28yrs: UIImageView!
    @IBOutlet weak var onlineWebView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let request = URLRequest(url: URL(string: "https://www.vaniinstitute.com/postal-online.php")!)
        self.onlineWebView.load(request)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
