//
//  Achivements.swift
//  VaniInstitute
//
//  Created by Raju Matta on 07/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import KRProgressHUD

class Achivements: UIViewController {
    
    
    @IBOutlet weak var ranksCollectionView: UICollectionView!
    var totalRanks:[Ranking] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        KRProgressHUD.show()
        getAchivements()
        // Do any additional setup after loading the view.
    }
    @IBAction func achivementsAction(_ sender: UIButton) {
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
    }
    
    @IBAction func goBackAction(_ sender: Any) {
        
        let backVC = storyboard?.instantiateViewController(withIdentifier: "Achivements")
        present(backVC!, animated: true, completion: nil)
    }
    func getAchivements(){
        
        
        
        Service.shared.GETService(extraParam: API.ranking) { (response) -> (Void) in
            
            KRProgressHUD.dismiss()
            print(response)
            
            guard let rankers = response.dictionary else{return}
            
            if let ranks = rankers["rankers"]!.array
                
            {
                
                for rank in ranks{
                    
                    
                    var getRank = Ranking()
                    getRank.rankingPersonImage = rank["image"].string!
                    self.totalRanks.append(getRank)
                    
                }
                print(self.totalRanks)
                
                DispatchQueue.main.async {
                    self.ranksCollectionView.reloadData()
                    
                }
            }
        }
        
        
        
    }
    
}
extension Achivements:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalRanks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CommonStrings.achivementCell, for: indexPath) as! AchivementCell
        
        // let filteringString = totalRanks[indexPath.row].rankingPersonImage
        
        // let filteredString = String(filteringString.filter{!" ".contains($0)})
        
        
        
        let imageRes = ImageResource(downloadURL: URL(string: totalRanks[indexPath.row].rankingPersonImage)!)
        
        print(imageRes)
        
        cell.achivecell.kf.setImage(with: imageRes)
        // cell.contentMode = .scaleToFill
        
        
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((ranksCollectionView.bounds.width)/3)-2, height: ((ranksCollectionView.bounds.width)/3)-2)
        
    }
    
    
    
    
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


