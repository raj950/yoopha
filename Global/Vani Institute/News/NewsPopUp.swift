//
//  NewsPopUp.swift
//  VaniInstitute
//
//  Created by Raju Matta on 11/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class NewsPopUp: UIViewController {

    @IBOutlet weak var newsView: UIView!
    @IBOutlet weak var descriptionInfo: UITextView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var toppers: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionInfo.text = getNewsVani.des
        print(getNewsVani.datePost!)
       // let datetrim = getNewsVani.datePost?.prefix(11)
        date.text = getNewsVani.datePost!
        toppers.text = getNewsVani.titleName
        newsView.layer.cornerRadius = 10
        self.view.backgroundColor = (UIColor.black).withAlphaComponent(0.5)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func ok(_ sender: Any) {
        self.view.removeFromSuperview()

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
