//
//  NewsCell.swift
//  VaniInstitute
//
//  Created by Raju Matta on 11/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var studentProfileImg: UIImageView!
    @IBOutlet weak var continueReading: UILabel!
    @IBOutlet weak var descriptionLbel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var topperLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
