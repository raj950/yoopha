//
//  ChatwithUs.swift
//  VaniInstitute
//
//  Created by Raju Matta on 19/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import KRProgressHUD

class ChatwithUs: UIViewController {
    
    @IBOutlet weak var toastMessage: UILabel!
    @IBOutlet weak var vani28yrsImage: UIImageView!
    @IBOutlet weak var textViewChat: UITextView!
    @IBOutlet weak var SUBJECTTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewChat.delegate = self
        textViewChat.text = "Type your text here...."
        textViewChat.textColor = UIColor.lightGray
        
        textViewChat.layer.borderColor = UIColor.black.cgColor
        textViewChat.layer.borderWidth = 0.5
        textViewChat.layer.cornerRadius = 5.0
        
      
    }
    @IBAction func contactUsAction(_ sender: UIButton) {
        
        
        let movetoMain = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        present(movetoMain, animated: true, completion: nil)
        
    }
    @IBAction func clearAction(_ sender: Any) {
        
        textViewChat.text = ""
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        
        
        if  textViewChat.text == "Type your text here...."{
            
            self.toastMessage.makeToast("Enter Mandatory Fields")
 
            
            
        }else{
            
            let sub = SUBJECTTF.text
            let parameters:Parameters = ["subject":"\(sub)" ,"message":textViewChat.text,"name":Credentials.name.string(forKey: "name"),"email":Credentials.email.string(forKey: "email"),"mobile":Credentials.storedMobile.string(forKey: "mobileNum"),"location":Credentials.loc.string(forKey: "loc")]
            
            
            Service.shared.POSTService(serviceType: API.chat, parameters: parameters as! [String : String]) { (response) -> (Void) in
                
                
                print(response)
                guard let success = response.dictionary else {return}
                
                if success["success"] == 1{
                    
                    self.textViewChat.text = ""
                    self.SUBJECTTF.text = ""
                    
                    
                    self.toastMessage.makeToast("Thank You For Message")

                    
                }
            }
        }
        
        
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ChatwithUs:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textViewChat.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
}
