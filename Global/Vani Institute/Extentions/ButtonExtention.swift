//
//  ButtonExtention.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit

@IBDesignable class ASButton: UIButton {
    ///////////Custom Attributes for Button corner Radius , border width, border color ,shadow
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderColor:UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    
    @IBInspectable var ShadowColor:UIColor = UIColor.clear{
        didSet{
            self.layer.shadowColor=ShadowColor.cgColor
        }
    }
    @IBInspectable var shadowOpacity:Float = 0{
        didSet{
            self.layer.shadowOpacity=shadowOpacity
        }
    }
    @IBInspectable var shadowOffset:CGSize = .zero{
        didSet{
            self.layer.shadowOffset = shadowOffset
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 0.0{
        didSet{
            self.layer.shadowRadius = shadowRadius
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
        },
                       completion: { finish in
                        UIView.animate(withDuration: 0.2, animations: {
                            self.transform = CGAffineTransform.identity
                        })
        })
        self.PulseEffect()
        super.touchesBegan(touches, with: event)
    }
    func PulseEffect()  {
        let pulseAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        pulseAnimation.duration = 1.0
        pulseAnimation.toValue = NSNumber(value: 1.0)
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = FLT_MAX
        self.layer.add(pulseAnimation, forKey: nil)
        
    }
    
    
}

