//
//  TextFieldExtention.swift
//  VaniInstitute
//
//  Created by Raju Matta on 17/02/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import UIKit



extension UITextField {
   
    func isValidTextField() -> Bool {
        if self.text == "" {
            
            return false
            
        } else {
            
            return true
            
        }
        
    }
    
    func isvalidName() -> Bool{
    
        if let nameCount = self.text{
            
            if nameCount.count < 4{
                return false
                
            }else{
                
                return true
            }
            
            
        }else{
            
            return false
        }
    
    }
    
    func isValidNameTextField() -> Bool {
        let emailRegEx = "/^[a-zA-Z ]+$/"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text)
        
    }
    
    func isValidMobileNumberTextField() -> Bool {
        if let digitsCount = self.text {
            
            if digitsCount.count == 10 {
                
                return true
                
            } else {
                
                return false
                
            }
         } else {
            
            return false
            
        }
        
    }
    
    
    
    func isValidEmailTextField() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text)
        
    }
    
    func textFieldAsLine() {
        self.borderStyle = .none
        
        let border = CALayer()
        
        let borderWidth: CGFloat = 1.0
        
        border.borderColor = UIColor.white.cgColor
        
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = borderWidth
        
        self.layer.addSublayer(border)
        
        self.layer.masksToBounds = true
        
    }
    
}
