//
//  LableExtention.swift
//  VaniInstitute
//
//  Created by Raju Matta on 09/01/19.
//  Copyright © 2019 VaniInstitute. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    
    func startBlink() {
        UIView.animate(withDuration: 1.5,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
    
    
    func fontSize(lableName:UILabel,viewsize:UIView){
        if (viewsize.frame.width == 320) {
            
            lableName.font = UIFont(name: lableName.font.fontName, size: 10)
            
        } else if (viewsize.frame.width == 375) {
            
            lableName.font = UIFont(name: lableName.font.fontName, size: 13)
            
        } else if (viewsize.frame.width == 414) {
            
            lableName.font = UIFont(name: lableName.font.fontName, size: 13)
            
        }
        
        
    }
    
    // iphone 5s,SE screen width 320
    // iphone 6,6s,7,etc width 375
    // iphone 7 plus, 8 plus, xs max,etc width 414
    
    func footerFontSize(lableName:UILabel,viewsize:UIView){
        if (viewsize.frame.width == 320) {
            
            lableName.font = UIFont(name: lableName.font.fontName, size: 7.5)
            
        } else if (viewsize.frame.width == 375) {
            
            lableName.font = UIFont(name: lableName.font.fontName, size: 7)
            
        } else if (viewsize.frame.width == 414) {
            
            lableName.font = UIFont(name: lableName.font.fontName, size: 10)
            
        }
        
        
    }
    
    
}
