//
//  BookingPopUpView.swift
//  Yoopha
//
//  Created by Yoopha on 01/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit

class BookingPopUpView: UIView {
    
    static let bookingView = BookingPopUpView()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backgroundVIew: UIView!
    @IBOutlet weak var successStatusLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        Bundle.main.loadNibNamed("BookingPopUpView", owner: self, options: nil)
        setImage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setImage(){
        
        imageView.layer.cornerRadius = 75
        backgroundVIew.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        backgroundVIew.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    
    enum status{
        case success
        case failure
    }
    
    func showAlert(title:String,message:String,alertType:status){
        
        self.successStatusLabel.text = title
        self.statusLabel.text = message
        switch alertType {
        case .success:
           imageView.image = UIImage(named: "user")
        case .failure:
            imageView.image = UIImage(named: "user_profile")
        }
        
        UIApplication.shared.keyWindow?.addSubview(backgroundVIew)
    }
    @IBAction func tappedOnViewBooking(_ sender: Any) {
    }
    
    
}
