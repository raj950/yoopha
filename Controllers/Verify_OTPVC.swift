//
//  Verify_OTPVC.swift
//  Yoopha
//
//  Created by Yoopha on 03/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class Verify_OTPVC: UIViewController {

    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var VerifyOTPTF: CustomTF!
    var time = 30
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Verify_OTPVC.action), userInfo: nil, repeats: true)
        
        
        DispatchQueue.main.async {
            if self.timerLbl.text == String(self.time > 1){
                
                print(self.timerLbl.text)
            }
        }
        
        resendOTPBtn.isEnabled = true
        resendOTPBtn.isUserInteractionEnabled = true
    }
    
    @IBAction func tappedOnResendOTP(_ sender: Any) {
       
        resendOTPBtn.isUserInteractionEnabled = true
        OTPResultModel.shared.getOTP(MobileString: OTPResultModel.mobile_no!)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Verify_OTPVC.action), userInfo: nil, repeats: true)
    
        DispatchQueue.main.async {
            if self.timerLbl.text == String(self.time > 1){
                
                print(self.timerLbl.text)
            }
        }
    }
    
    
        
   
    
    @IBAction func tappedOnVerifyBtn(_ sender: Any) {
        
        if VerifyOTPTF.text?.isEmpty == true{
            simpleAlert(message: "Please Enter Your OTP")
        }else{
            verifyOTP()
//            let navToLogin = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.forgotPasswordVC) as! ForgotPasswordVC
//            self.present(navToLogin, animated: false, completion: nil)
        }
    }
    @IBAction func tappedOnLogin(_ sender: Any) {
        
        let navToLogin = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
        self.present(navToLogin, animated: false, completion: nil)
    }
    
    @objc func action(){
        time -= 1
        timerLbl.text = String(time)
        if time <= 30{
            
          //  self.resendOTPBtn.isUserInteractionEnabled = false
            self.resendOTPBtn.backgroundColor = .gray
            resendOTPBtn.isEnabled = false
        }
        if time < 0 {
            time = 30
            resendOTPBtn.isEnabled = true
            timer.invalidate()
            timerLbl.text = "30"
          //  self.resendOTPBtn.isUserInteractionEnabled = true
            self.resendOTPBtn.backgroundColor = UIColor.appGreenColor
        }
    }
    
    func verifyOTP(){
        
        let myParam = NSMutableDictionary()
        VerifyOTPTF.text = VerifyOTPTF.text
       print(OTPResultModel.mobile_no)
       
      //  let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y01_common/verify_otp/"
        let finalUrl1 = "\(Constants.API.base_URL)\(Constants.API.verifyOTP)" + "" + "\(OTPResultModel.mobile_no!)" + "" + Constants.ConstantStrings.slash
        let finalUrl2 = VerifyOTPTF.text! + "" + Constants.ConstantStrings.slash
        let finalUrl3 = finalUrl1 + finalUrl2
        print(finalUrl3)
        
        WebApiCallBack.makeGetCall(webUrl: finalUrl3, paramData: myParam) { (response, error) in
            print(response as Any)
            
             let response = JSON(response).dictionary
            if let message = response!["message"]?.string{
            
                if message == "OTP verification failed"{
                    DispatchQueue.main.async {
                         AlertView.instance.showAlert(title: "WRONG OTP", message: "Please Try Again", alertType: .failure)
                        AlertView.instance.tokenLbl.isHidden = true
                        AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                    }
                   
                }
                else{
                    
                    DispatchQueue.main.async {
                        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.forgotPasswordVC) as! ForgotPasswordVC
                            navTo.oldPasswordTFStatus = true
                            navTo.oldPasswordViewStatus = true
                        self.present(navTo, animated: false, completion: nil)
                      //  self.navigationController?.pushViewController(loginVC, animated: true)
                    }
                   
                }
            }
        
            }
        }
}



struct verifyOTPResultModel {
    let mobile_no : Int
    let message: String
}
