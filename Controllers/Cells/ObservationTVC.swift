//
//  ObservationTVC.swift
//  Yoopha
//
//  Created by Yoopha on 16/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class ObservationTVC: UITableViewCell {

    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var drEducationLbl: UILabel!
    
    @IBOutlet weak var bookingDateLbl: UILabel!
    @IBOutlet weak var patientNameLbl: UILabel!
    
    @IBOutlet weak var patientIdLbl: UILabel!
    
    @IBOutlet weak var symptomsLbl: UILabel!
    @IBOutlet weak var viewBtn: CustomButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
