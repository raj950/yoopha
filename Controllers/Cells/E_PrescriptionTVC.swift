//
//  E_PrescriptionTVC.swift
//  Yoopha
//
//  Created by Yoopha on 25/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class E_PrescriptionTVC: UITableViewCell {

    @IBOutlet weak var symptomLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var prescriptionIdLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
