//
//  HealthRecordsPopUpView.swift
//  Yoopha
//
//  Created by Yoopha on 27/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit

class HealthRecordsPopUpView: UIView {

    @IBOutlet var parentView: UIView!
    static let instance = HealthRecordsPopUpView()

    override init(frame: CGRect){
        super.init(frame: frame)
        Bundle.main.loadNibNamed("HealthRecordsPopUpView", owner: self, options: nil)
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
       
    }
    
    func addParentView(){
         UIApplication.shared.keyWindow?.addSubview(parentView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBAction func onClickDismissBtn(_ sender: Any) {
        parentView.removeFromSuperview()

    }
}
