//
//  AppointmentsCVC.swift
//  Yoopha
//
//  Created by Yoopha on 09/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class AppointmentsCVC: UICollectionViewCell {

    @IBOutlet weak var drProfile: CustomImageView!
    @IBOutlet weak var appointmentStatusView: UIView!
    @IBOutlet weak var clinicAddressLbl: UILabel!
    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var tokenLbl: UILabel!
    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var specializationLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
