//
//  ShareAndLearnCVC.swift
//  Yoopha
//
//  Created by Yoopha on 09/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class ShareAndLearnCVC: UICollectionViewCell {

    @IBOutlet weak var messageCountLbl: UILabel!
    @IBOutlet weak var messageIconImage: CustomImageView!
    @IBOutlet weak var authorImage: CustomImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
