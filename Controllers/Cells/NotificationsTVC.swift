//
//  NotificationsTVC.swift
//  Yoopha
//
//  Created by Yoopha on 01/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class NotificationsTVC: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        moreBtn.underline()
    }
    @IBAction func tappedOnMoreBtn(_ sender: Any) {
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
