//
//  TrendingTopicsCVC.swift
//  Yoopha
//
//  Created by Yoopha on 24/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
protocol ReadCell {
    func onClickCell(index:Int)
}

class TrendingTopicsCVC: UICollectionViewCell {

    @IBOutlet weak var readNowBtn: CustomButton!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var titleLbl1: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var coverImage: UIImageView!
    
    var cellDelegate:ReadCell?
    var index:IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func tappedOnReadNowBtn(_ sender: UIButton) {
        
        cellDelegate?.onClickCell(index: (index?.row)!)
    }
}
