//
//  ClinicTVC.swift
//  Yoopha
//
//  Created by Yoopha on 20/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class ClinicTVC: UITableViewCell {

    
    @IBOutlet weak var clinicDayLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var timingLbl: UILabel!
    @IBOutlet weak var clinicName: UILabel!
    @IBOutlet weak var locationLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func tappedOnDirectionBtn(_ sender: Any) {
    }
    
    @IBAction func tappedOnBookBtn(_ sender: Any) {
    }
}
