//
//  calenderCVC.swift
//  Yoopha
//
//  Created by Yoopha on 09/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class calenderCVC: UICollectionViewCell {
    
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var morningBtn: CustomButton!
    
    @IBOutlet weak var noonBtn: CustomButton!
    
    @IBOutlet weak var eveningBtn: CustomButton!
    
    
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var morningSlotBtn: CustomButton!
   
    @IBOutlet weak var noonSlotBtn: CustomButton!
    
    @IBOutlet weak var eveningSlotBtn: CustomButton!
    
    
    let borderColor = UIColor.appGreenColor
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    override func draw(_ rect: CGRect) {
        

     
        
        
        self.dateLbl.layer.masksToBounds = true
        self.dateLbl.layer.cornerRadius = 10
    }
    override var isSelected: Bool {
        didSet {
            if dateLbl.backgroundColor == UIColor.white{
                
                self.dateLbl.backgroundColor = UIColor.orange
                
            }
            else{
                self.dateLbl.backgroundColor = UIColor.white
                
            }
            
//            if morningBtn.backgroundColor == UIColor.white{
//
//                self.morningBtn.backgroundColor = UIColor.orange
//
//            }
//            else{
//                self.morningBtn.backgroundColor = UIColor.white
//
//            }
            
            
            
            
        }
    }
}
