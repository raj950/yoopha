//
//  SearchMedicineTVC.swift
//  Yoopha
//
//  Created by Yoopha on 12/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

protocol OptionsCell {
    func onClickCell(index:Int)
}
class SearchMedicineTVC: UITableViewCell {
    
    let price:Int = 1
    let initialPrice:Int = 10
    @IBOutlet weak var cronicBtn: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var decrementBtn: UIButton!
    @IBOutlet weak var incrementBtn: UIButton!
    @IBOutlet weak var deleteCronic: UIButton!
    
        var cellDelegate:OptionsCell?
     var index:IndexPath?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    @IBAction func tappedOnIncrement(_ sender: Any) {
        let Quantity = Int(quantityLbl.text!)! + price
         quantityLbl.text! = "\(Quantity)"
        
        let pricecal = Int(quantityLbl.text!)! * 10
        priceLbl.text = "\(pricecal)"
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
