//
//  SearchDoctorsCell.swift
//  Yoopha
//
//  Created by Yoopha on 20/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit


protocol tableviewCell{
    func onClickCell(index:Int)
}
class SearchDoctorsCell: UITableViewCell {

  //  static let searchDoctor = searchDoctorsCell
    
    @IBOutlet weak var profileImage: CustomImageView!
    
    @IBOutlet weak var tagBtn: UIButton!
    
    @IBOutlet weak var doctorNameLbl: UILabel!
    
    @IBOutlet weak var doctorSpecialityLbl: UILabel!
    
    @IBOutlet weak var drEducationLbl: UILabel!
    @IBOutlet weak var clinicName: UILabel!
    
    @IBOutlet weak var moreBtn: UIButton!
    
    
    @IBOutlet weak var experienceLbl: UILabel!
    
    
    @IBOutlet weak var feeLbl: UILabel!
    
    
    @IBOutlet weak var bookBtn: CustomButton!
    
    @IBOutlet weak var dayAndTimeLbl: UILabel!
    
    var cellDelegate:tableviewCell?
    var index:IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
       selectionStyle = .none
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    @IBAction func tappedOnTagBtn(_ sender: Any) {
    }
    
    @IBAction func tappedOnMoreBtn(_ sender: Any) {
    }
    
    
    
    
    
    @IBAction func bookBtn(_ sender: Any) {
       
        // Nav to Doctor DetailsVC
        cellDelegate?.onClickCell(index:(index?.row)!)
      //  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
     //   let raj = storyBoard.instantiateViewController(withIdentifier: "BookAppointmentVC") as! BookAppointmentVC
        
        
        print("Tapped on ME")
        
    }
    
}
