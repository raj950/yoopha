//
//  UserAppoinmentsCell.swift
//  Yoopha
//
//  Created by Yoopha on 15/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class UserAppointmentsCell: UITableViewCell {

    @IBOutlet weak var doctorProfile: CustomImageView!
    @IBOutlet weak var clinicNameLbl: UILabel!
    @IBOutlet weak var symptomLbl: UILabel!
    @IBOutlet weak var clinicAddressLbl: UILabel!
    @IBOutlet weak var drEducationLbl: UILabel!
    @IBOutlet weak var appointmentStatusLbl: UILabel!
    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var tokenLbl: UILabel!
    
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var userAppoinmentBackgroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
    
}

