//
//  PrescriptionTVC.swift
//  Yoopha
//
//  Created by Yoopha on 13/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class PrescriptionTVC: UITableViewCell {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var drugNameLbl: UILabel!
    
    @IBOutlet weak var afterFoodBtn: UIButton!
    @IBOutlet weak var beforeFoodBtn: UIButton!
    @IBOutlet weak var evenBtn: UIButton!
    @IBOutlet weak var noonBtn: UIButton!
    @IBOutlet weak var mornBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
