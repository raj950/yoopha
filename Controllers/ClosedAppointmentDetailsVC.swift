//
//  ClosedAppointmentDetailsVC.swift
//  Yoopha
//
//  Created by Yoopha on 20/09/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class ClosedAppointmentDetailsVC: UIViewController {

    @IBOutlet weak var cancelBtn: CustomButton!
    @IBOutlet weak var getDirectionBtn: CustomButton!
    @IBOutlet weak var clinicLocationLbl: UILabel!
    @IBOutlet weak var clinicImage: CustomImageView!
    @IBOutlet weak var doctorDescriptionLbl: UILabel!
    @IBOutlet weak var tagBtn: UIButton!
    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var doctorProfileImage: CustomImageView!
    @IBOutlet weak var viewBtn: CustomButton!
    @IBOutlet weak var prescriptionDateLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var prescriptionLbl: UILabel!
    @IBOutlet weak var bodyTemperatureLbl: UILabel!
    @IBOutlet weak var sugarLevelLbl: UILabel!
    @IBOutlet weak var bloodPresureLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var observationLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var appointmentLbl: UILabel!
    
    var selectedPatientDetails:Int?
    var savedetails:[patientsList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func navBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnGetDirectionBtn(_ sender: Any) {
    }
    @IBAction func tappedOnTagBtn(_ sender: Any) {
    }
    @IBAction func tappedOnViewBtn(_ sender: Any) {
    }
    @IBAction func tappedOnCancelAppointmentBtn(_ sender: Any) {
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
