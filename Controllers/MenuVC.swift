//
//  MenuVC.swift
//  Yoopha
//
//  Created by Yoopha on 18/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Foundation


class MenuVC: UIViewController {

    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var userProfileImage: CustomImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var menuTV: UITableView!
    
  //  var tapGesture = UITapGestureRecognizer()

   
    
    var sections = Constants.ConstantStrings.menuSections
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileView.backgroundColor = UIColor.appGreenColor
        menuView.slideInFromRight()
    // userName.text = SignUpModel.mobileNumber
        print(ProfileModel.name)
     userName.text = loginModel.mobileNumber

     
    }
    
   

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//            dismiss(animated: false, completion: nil)
//        if  self.menuView.isHidden == true{
//            self.menuView.isHidden = false
//            dismiss(animated: false, completion: nil)
//        }
//        else{
//            self.menuView.isHidden = true
//            dismiss(animated: false, completion: nil)
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
          self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func tappedOnEditProfile(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.edit_ProfileVC) as! Edit_ProfileVC
        present(navTo, animated: false, completion: nil)
      
        
    }
 
}

extension MenuVC:UITableViewDelegate,UITableViewDataSource,ExpandableHeaderViewDelegate,UIGestureRecognizerDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].options.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (sections[indexPath.section].expanded){
            return 50
        } else {
            return 0
        }
    }
    
   
    private func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//        let imageViewGame = UIImageView(frame:  CGRect(x: 0, y: 20, width: 20, height: 20));
//        let image = UIImage(named: "user");
//        imageViewGame.image = image;
//        header.contentView.addSubview(imageViewGame)
//        return header
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let header = ExpandableHeaderView()
        if section == 4{
            header.view.frame = CGRect(x: 0, y: 48, width: UIScreen.main.bounds.width, height: 1)
            header.view.backgroundColor = UIColor.appGreenColor
            header.addSubview(header.view)
        }
        if section == 5{
            header.image.frame = CGRect(x:12.5, y: 17.5, width: 15, height: 15)
            header.image.image = UIImage(named: "logout")
            header.addSubview(header.image)
        }
        
        if section == 6{
           
           
            header.image.frame = CGRect(x: 12.5, y: 17.5, width: 15, height: 15)
            header.image.image = UIImage(named: "setting")
            header.addSubview(header.image)
            
            
            
          
        }
        header.customInit(title: sections[section].quastion, section: section, delegate: self)
        header.button.frame = CGRect(x: 230, y: 15, width: 20, height: 20)
        header.button.setImage(UIImage(named: "next"), for: .normal)
        header.button.setImage(UIImage(named: "previous"), for: .highlighted)
        header.addSubview(header.button)
        return header
    }
//    @objc func handleTap(gestureRecognizer: UIGestureRecognizer)
//    {
//
//            print("tapped")
//
//    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.menuMainTVC) as! MenuMainTVC
        cell.menuLbl.text = sections[indexPath.section].options[indexPath.row]
        return cell
    }
    
    func toggleSection(header: ExpandableHeaderView, section: Int, button:UIButton, view:UIView, image:UIImageView) {
        sections[section].expanded = !sections[section].expanded
        
//        if section == 0 {
//            let navToHealthRecords = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.yourAppointmentVC) as! YourAppointmentVC
//            present(navToHealthRecords, animated: false, completion: nil)
//        }
        
        if section == 1 {
            let navToTags = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.tagsVC) as! TagsVC
            present(navToTags, animated: false, completion: nil)
            navToTags.backNavBtn.isHidden = false
            
        }
        if section == 2 {
            UIApplication.shared.open(URL(string: blogsUrl)! as URL, options: [:], completionHandler: nil)
        }
        if section == 3{
            UIApplication.shared.open(URL(string: communityUrl)! as URL, options: [:], completionHandler: nil)
        }
        if section == 5{
            
            let navToLogin = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.loginVC) as! LoginVC
            self.present(navToLogin, animated: false, completion: nil)
          
            let url = URL(string: "\(Constants.API.base_URL)\(Constants.API.logOut)")
            if let usableUrl = url {
                var request = URLRequest(url: usableUrl)
                request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
                let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data {
                        if let resposeDictionary = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{

                            print(resposeDictionary)


                        }

                    }

                })
                task.resume()

            }
            
        }
        if section == 6{
            
            let navToSettings = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.settingsVC) as! SettingsVC
            self.present(navToSettings, animated: false, completion: nil)
            
            
        }
       
        menuTV.beginUpdates()
        for i in 0 ..< sections[section].options.count {
            menuTV.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        menuTV.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if indexPath.row == 0{
            let navToHealthRecords = storyboard?.instantiateViewController(withIdentifier: "HealthRecordsVC") as! HealthRecordsVC
            present(navToHealthRecords, animated: false, completion: nil)
        }
        if indexPath.row == 1{
            let navToHealthRecords = storyboard?.instantiateViewController(withIdentifier: "HealthRecordsVC") as! HealthRecordsVC
            present(navToHealthRecords, animated: false, completion: nil)
        }
        if indexPath.row == 2{
            let navToHealthRecords = storyboard?.instantiateViewController(withIdentifier: "HealthRecordsVC") as! HealthRecordsVC
            present(navToHealthRecords, animated: false, completion: nil)
        }
        
//        if indexPath.row == 6{
//             let navToSettings = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.settingsVC) as! SettingsVC
//            present(navToSettings, animated: false, completion: nil)
//
//        }
        
        
    }

}
//extension UIViewController{
//    @objc func swipeAction(swipe:UISwipeGestureRecognizer){
//        switch swipe.direction.rawValue {
//        case 1:
//            performSegue(withIdentifier: "1", sender: self)
//        case 2:
//            performSegue(withIdentifier: "2", sender: self)
//        default:
//            break
//        }
//
//    }
//}
