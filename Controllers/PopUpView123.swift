//
//  PopUpView.swift
//  Yoopha
//
//  Created by Yoopha on 27/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit

class PopUpView:UIView{
    
    static let instance = PopUpView()
    
    @IBOutlet weak var popUpView: CustomView!
    @IBOutlet weak var parentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("PopUpView", owner: self, options: nil)
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func tappedOnDownArrow(_ sender: Any) {
        parentView.removeFromSuperview()
        print("tapped")
    }
    
    func showPopUP(){
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
}
