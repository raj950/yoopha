//
//  Edit_ProfileVC.swift
//  Yoopha
//
//  Created by Yoopha on 01/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Parchment
import SideMenuSwift

class Edit_ProfileVC: UIViewController {

    
    var itemArray=["Personal","Medical"]
    var fixedpgController:FixedPagingViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let personal_Info = storyboard.instantiateViewController(withIdentifier: Constants.ViewController.personal_InfoVC)
        let medical_Info = storyboard.instantiateViewController(withIdentifier:Constants.ViewController.medical_InfoVC)

        
        fixedpgController=FixedPagingViewController(viewControllers: [personal_Info,medical_Info])
        addChild(fixedpgController)
        view.addSubview(fixedpgController.view)
        view.constrainToEdges(fixedpgController.view)
        fixedpgController.didMove(toParent: self)
        fixedpgController.dataSource=self

        
    }

    @IBAction func navBack(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.sideMenuController) as! SideMenuController
        present(navTo, animated: false, completion: nil)
    }
}

extension Edit_ProfileVC:PagingViewControllerDataSource{
    
    func numberOfViewControllers<T>(in pagingViewController: PagingViewController<T>) -> Int where T : PagingItem, T : Comparable, T : Hashable {
        return itemArray.count
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController where T : PagingItem, T : Comparable, T : Hashable {
        return fixedpgController.viewControllers[index]
        
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T where T : PagingItem, T : Comparable, T : Hashable {
        return PagingIndexItem(index: index, title: itemArray[index]) as! T
    }
    
    
}
