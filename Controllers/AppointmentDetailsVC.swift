//
//  AppointmentDetailsView.swift
//  Yoopha
//
//  Created by Yoopha on 27/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toast_Swift
import Kingfisher

class AppointmentDetailsVC: UIViewController {
    
    @IBOutlet weak var clinicImage: CustomImageView!
    @IBOutlet weak var appointmentIDLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var tagBtn: UIButton!
    let date = Date()
    @IBOutlet weak var drEducationLbl: UILabel!
    @IBOutlet weak var tokenLbl: UILabel!
    var doctorExp:Int?
    var clinicName:String?
    var slotID:Int?
    var slotDetailsArray:[SlotDeatils]=[]
    @IBOutlet weak var profileImage: CustomImageView!
    @IBOutlet weak var drNameLbl: UILabel!
  //  @IBOutlet weak var drExpLbl: UILabel!
    @IBOutlet weak var drSpecilityLbl: UILabel!
    @IBOutlet weak var clinicNameLbl: UILabel!
  //  @IBOutlet weak var clinicTimeLbl: UILabel!
    
  //  @IBOutlet weak var feeLbl: UILabel!
    
    @IBOutlet weak var drDiscriptionLbl: UILabel!
    @IBOutlet weak var rescheduleBtn: CustomButton!
    @IBOutlet weak var cancelBtn: CustomButton!
    @IBOutlet weak var locationLbl: UILabel!
    
   
    var savedetails:[patientsList] = []
    var doctorSavedDetails:[DRInfo] = []
    var tagsData:[TagsResultModel]=[]
    var selectedPatientDetails:Int?
    var selectedDoctroDetails:Int?
    var patientDoctorDetails:[String] = []
  
    var drID:Int?
    var appointmentId:Int?
    var nameDoctor:String?
    override func viewDidLoad() {
        super.viewDidLoad()

      
         // dateLbl.text = savedetails[selectedPatientDetails!].bookingDate
      //  drExpLbl.text = savedetails[selectedPatientDetails!].doctorSpeciality!
       // clinicNameLbl.text = savedetails[selectedPatientDetails!].clinicName
        
     //   print(clinicNameLbl.text)

        tokenLbl.text = "\(savedetails[selectedPatientDetails!].patientToken ?? 0)"
        appointmentIDLbl.text = savedetails[selectedPatientDetails!].appointmentId
        profileImage.kf.setImage(with: URL(string: savedetails[selectedPatientDetails!].doctroProfile ?? Constants.ConstantStrings.defaultImage))
     
        
        getAppointmentDetails()
    
    }
    
    
    
    func getAppointmentDetails(){
        
        let myParams = NSMutableDictionary()
        let url = "\(Constants.API.base_URL)\(Constants.API.get_doctor_appointment_by_slug)"
        let finalUrl = url + savedetails[selectedPatientDetails!].patientSlug!
        print(finalUrl)
        WebApiCallBack.shared.GETService(serviceType: finalUrl , parameters: myParams as! [String : String]) { (response) -> (Void) in
            
            print(response)
            
            if response != nil{
                
                let apiResponse = JSON(response)
                
                if apiResponse.dictionary != nil{
                    if let results = apiResponse["results"].array{
                        for result in results{
                            
                            let res = result.dictionary
                            self.appointmentId = res!["id"]?.int
                            let doctorData = result["doctor"].dictionary
                            var doctorDetails = DoctorDeatils()
                            let doctorProfileImg = doctorData!["doctor_profile_image"]?.array
                            for getImage in doctorProfileImg!{
                                doctorDetails.doctorProfileImage = getImage["upload"].string ?? "user_profile"
                            }
                            var draddress = DoctorAddress()
                            var dradd = doctorData!["address"]?.dictionary
                            draddress.address_line2 = dradd!["address_line2"]?.string
                            draddress.state = dradd!["state"]?.string
                            
//
//                            let drSlot = result["clinic_slot"].dictionary
//                            self.slotID = drSlot!["id"]?.int
//
//
//                            let clinic = drSlot!["clinic"]?.dictionary
//                            self.clinicName = clinic!["clinic_name"]?.string
//
//                            let slot = drSlot!["slot"]?.dictionary
//                            let startTime = slot!["start_time"]?.string
//
//                            let start = startTime!.timeConversion12(time24:startTime!)
//                            let endTime = slot!["end_time"]?.string
//                            let end = endTime!.timeConversion12(time24:endTime!)
//
//
//                            let startTrimClinic = start.stringTrims(trim: start)
//                            let endTrimClinic = end.stringTrims(trim: end)
//
//                            let day = slot!["day"]?.string
//                            let schedule = slot!["schedule"]?.string
//
//                            var slotData = SlotDeatils()
//                            slotData.schedule   = schedule
//                            slotData.start_time = startTime
//                            slotData.day        = day
//                            slotData.end_time   = endTime
//
//                            self.slotDetailsArray.append(slotData)
                            
                            DispatchQueue.main.async {
                             //   self.clinicNameLbl.text = self.clinicName
                               //    self.feeLbl.text = Constants.ConstantStrings.rupee + " " + fee!
                             //   self.clinicTimeLbl.text = day! + "-" + schedule! + "-" + "" + "\(startTrimClinic)" + "-" + "\(endTrimClinic)"
                              //  self.timeLbl.text = "\(startTrimClinic)" + "-" + "\(endTrimClinic)"
                              //  self.locationLbl.text = add2! + "," + state!
                           
                              //  let url = URL(string:doctorDetails.doctorProfileImage!)
                             
                                //self.profileImage.kf.setImage(with:url)
                               // var doctorDetails = DoctorDeatils()
                                self.locationLbl.text = draddress.address_line2! + Constants.ConstantStrings.comma + draddress.state!
                            
                             //    let exp = self.date.GetYear() - (doctorData!["practice_started_year"]?.int)!
                                
                                self.drNameLbl.text! = (doctorData!["doctor_name"]?.string)!
                                self.drEducationLbl.text = doctorData!["doctor_education"]?.string
                                self.doctorExp = doctorData!["practice_started_year"]?.int
                                
                                self.drID = (doctorData!["id"]?.int)!
                            //    self.drExpLbl.text! = (doctorData!["doctor_education"]?.string)! + " " + "\(exp)" + " " + Constants.ConstantStrings.year
                               
                               
                                
                                
                                let monthNameAndDate = result["booking_date"].string
                                let startIndex = monthNameAndDate!.index(monthNameAndDate!.startIndex, offsetBy: 5)
                                let endIndex = monthNameAndDate!.index(monthNameAndDate!.startIndex, offsetBy: 6)
                                let dateStart =  monthNameAndDate!.index(monthNameAndDate!.startIndex, offsetBy: 8)
                                let dateEnd =  monthNameAndDate!.index(monthNameAndDate!.startIndex, offsetBy: 9)
                                let yearStart = monthNameAndDate!.index(monthNameAndDate!.startIndex, offsetBy: 2)
                                let yearEnd = monthNameAndDate!.index(monthNameAndDate!.startIndex, offsetBy: 3)
                                let year = String(monthNameAndDate![yearStart...yearEnd])
                                let dateTrim = String(monthNameAndDate![dateStart...dateEnd])
                                let monthtrim = String(monthNameAndDate![startIndex...endIndex])
                                let monthNameGet = DateFormatter().shortMonthSymbols[Int(monthtrim)! - 1]
                                
                                self.monthLbl.text =   dateTrim + " " + monthNameGet + " " + year
                            //    self.dateLbl.text =
            
                                var drspeciality = DoctorSpecialities()
                                var drsp = doctorData!["doctor_speciality"]?.dictionary
                                
                                drspeciality.desc = drsp!["desc"]?.string
                                self.drSpecilityLbl.text = drsp!["speciality_name"]?.string
                               
                                self.drDiscriptionLbl.text = drspeciality.desc
                                
                                
                            
                                for result in results{
                                    var doctorClinic = DoctorClinicResult()
                                    doctorClinic.id = result["id"].int
                                    doctorClinic.slug = result["slug"].string
                                }
                           
                            }
                        }
                    }
                }
            }

        }
    }
    
    @IBAction func tappedOnNavBack(_ sender: Any) {
      //  let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.yourAppointmentVC) as! YourAppointmentVC
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnTagBtn(_ sender: UIButton) {
        
       
        if sender.currentImage == UIImage(named: "Fovourite-final"){
            
            sender.setImage(UIImage(named: "fovourite-selected"), for: .normal)
            
            let myParams = ["consumer":ConsumerModel.consumerId,"doctor":drID]
            print(myParams)
            let url = "\(Constants.API.base_URL)\(Constants.API.create_consumerDoctorTag)"
            WebApiCallBack.shared.POSTServiceHeader(serviceType: url, parameters: myParams as [String : Any], token:loginModel.token) { (response) -> (Void) in
                print(response)
            }
            
        }else{
            
//            for i in 0..<tagsData.count{
//
//
//                print(tagsData[i].id)
//
//            }
            
            sender.setImage(UIImage(named: "Fovourite-final"), for: .normal)
            let params = NSMutableDictionary()
            let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y06_tags/"
            let url2 = ConsumerModel.consumerId
            let finalUrl = url + "\(url2)" + "/ConsumerDoctor_untag/"
            print(finalUrl)
            WebApiCallBack.makeDeleteCall(webUrl: finalUrl, paramData: params, token:loginModel.token) { (response, error) in
                
                print(response)
                
             
                
            }
        }
    }
    
    func cancelAppointment(){
        
        let url1 = "y07_appointment/"
        //let url2 = AppointmentStatusModel.patientAppointmentId
        let url2 = appointmentId!
        let url3 = "/Doctor_appoinment_edit/"
        let finalUrl = url1 + "\(url2)" + url3
        
        print(finalUrl)
        let params = ["allergy":[],"appointment_status":3] as NSObject
       
        WebApiCallBack.makePatchCall(webUrl:"\(Constants.API.base_URL)\(finalUrl)", paramData: params, token: loginModel.token) { (response, error) in
            
            print(response)
        }
    }
    
    @IBAction func tappedOnDoctor(_ sender: Any) {
        
//        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.doctorDetailedVC) as! DoctorDetailedVC
//        self.navigationController?.pushViewController(navTo, animated: true)
    }
    
    @objc func resheduleAppointment(){
        let url1 = "y07_appointment/"
        //print(appointmentId)
        let url2 = appointmentId!
        let url3 = "/Doctor_appoinment_edit/"
        let url4 = url1 + "\(url2)" + url3
        let finalUrl = Constants.API.base_URL + url4
        print(slotID)
        print(CalenderPopUp.calenderInstance.dateTF.text!)
        print(finalUrl)
        let params = ["allergy":[],"appointment_status":1,"clinic_slot":slotID as Any,"booking_date":CalenderPopUp.calenderInstance.dateTF.text! as String] as NSObject
        WebApiCallBack.makePatchCall(webUrl:finalUrl, paramData: params, token: loginModel.token) { (response, error) in
     
            print(response)
            
            
            
            if response != nil{
                
                
                if let response = JSON(response).dictionary{
                    let appointment_status = response["appointment_status"]?.int
                    if appointment_status == 1{
                           self.view.makeToast("Your Appointment has been RESHEDULED to \(CalenderPopUp.calenderInstance.dateTF.text!)",duration:7.0,position:.center)
                    }
                }
            }
        
    }
    }
    @IBAction func tappedOnRescheduleBtn(_ sender: Any) {
        
//        CalenderPopUp.calenderInstance.showAlert(title: "SUCCESS", message: "Appointment Created Successfully", alertType: .success,clinicName:clinicName!,slotData: slotDetailsArray)
//
//        CalenderPopUp.calenderInstance.confirmBtn.addTarget(self, action: #selector(resheduleAppointment), for: .touchUpInside)
    
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.bookAppointmentVC) as! BookAppointmentVC
     //   navTo.doctorExperience = self.date.GetYear() - doctorExp!
       // navTo.drEducationLbl.text = DRInfo.doctorEducation
        navTo.doctorName = DRInfo.doctorName
        navTo.doctorExp = "\(DRInfo.doctorExp)"
        navTo.doctorSpeciality = DRInfo.doctorSpeciality
    
        self.navigationController?.pushViewController(navTo, animated: true)
        
    }

   

    @IBAction func tappedOnCancelBtn(_ sender: Any) {
    alertWithMessage(title: "Cancel Appointment", message: "Do you want to cancel your appointment", cancelButtonTitle: "No", buttonTitle: "Yes", handler: { (action) in
        
        self.cancelAppointment()
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.appointmentStatusVC) as! AppointmentStatusVC
        
        navTo.appointmentStatus = false
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(navTo, animated: true)
        
        }) { (action) in
            self.resignFirstResponder()
        }
       
    }
    @IBAction func tappedOnGetDirectionBtn(_ sender: Any) {
    }
}



struct TagsResultModel{
    
    var id:Int?
    var consumer:Int?
    var slug:String?
    var visits:Int?
   // var tagsResults:[String]
}


