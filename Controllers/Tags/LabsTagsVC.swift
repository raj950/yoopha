//
//  LabsTagsVC.swift
//  Yoopha
//
//  Created by Yoopha on 26/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class LabsTagsVC: UIViewController {

    let labnames = ["Kumaran Clinical Laboratory","Regi Clinical Lab","Annai Clinical Lab","Aati Labs"]
    @IBOutlet weak var labTagsTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCell()
    }
    func registerCell(){
        let pharmaciesCell = UINib(nibName: Constants.Cell.tagsTVC, bundle: nil)
        labTagsTV.register(pharmaciesCell, forCellReuseIdentifier: Constants.Cell.tagsTVC)
        
    }
}

extension LabsTagsVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.tagsTVC) as! TagsTVC
        cell.pharmacyNameLbl.text = labnames[indexPath.row]
        return cell
    }
    
    
}
