//
//  PharmaciesTagsVC.swift
//  Yoopha
//
//  Created by Yoopha on 26/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class PharmaciesTagsVC: UIViewController {

    let pnames = ["Doctors Pharmacy","Sri Ramdev Pharmacy","Mani's Pharmacy","Apollo pharmacy"]
    @IBOutlet weak var pharmaciesTagsTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCell()
        
    }
    
    func registerCell(){
        let pharmaciesCell = UINib(nibName: Constants.Cell.tagsTVC, bundle: nil)
        pharmaciesTagsTV.register(pharmaciesCell, forCellReuseIdentifier: Constants.Cell.tagsTVC)
        
    }

}

extension PharmaciesTagsVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:Constants.Cell.tagsTVC) as! TagsTVC
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.pharmacyNameLbl.text = pnames[indexPath.row]
        
        return cell
    }
    
    
}
