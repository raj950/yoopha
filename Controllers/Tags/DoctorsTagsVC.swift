//
//  DoctorsTagsVC.swift
//  Yoopha
//
//  Created by Yoopha on 26/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class DoctorsTagsVC: UIViewController {
 var tagsData:[TagsResultModel]=[]
    var drNames:[DoctorDeatils] = []
    @IBOutlet weak var doctorTagsTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        doctorTags()
        registerCell()
       
     //   print(ConsumerModel.consumerId)
     
    }

//    func getConsumer(){
//        let url2 = "\(Constants.API.base_URL)\(Constants.API.consumer_consumer)"
//        let finalUrl = url2 + "\(loginModel.userid)/"
//        print(finalUrl)
//
//
//
//        DispatchQueue.main.async {
//            let url = URL(string: finalUrl)
//            if let usableUrl = url {
//                var request = URLRequest(url: usableUrl)
//                request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
//                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//                let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
//                    if let data = data {
//                        if let response = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
//                            print(response)
//
//
//                        }
//
//                    }
//                })
//                task.resume()
//            }
//
//            self.doctorTags()
//        }
//    }
    
    func registerCell(){
        let doctorCell = UINib(nibName: Constants.Cell.searchDoctorsCell, bundle: nil)
        doctorTagsTV.register(doctorCell, forCellReuseIdentifier: Constants.Cell.searchDoctorsCell)
        doctorTagsTV.showsVerticalScrollIndicator = false
        doctorTagsTV.separatorStyle = .none
    }
    
   
    
    func doctorTags(){
        let url1 = "\(Constants.API.base_URL)\(Constants.API.consumerDoctorTag_view_by_consumer)"
        let finalUrl = url1 + "\(ConsumerModel.consumerId)" + Constants.ConstantStrings.slash
         print(finalUrl)
        
        WebApiCallBack.shared.GETServiceHeader(url: finalUrl, token: loginModel.token) { (response) -> (Void) in
            print(response)
            
            let apiResponse = JSON(response)
            if let serverMessage = apiResponse.dictionary{
                if let results = apiResponse["results"].array{
                    for result in results{
                        let doctorData = result["doctor"].dictionary
                        var doctorInfo = DoctorDeatils()
                        doctorInfo.doctor_name = doctorData!["doctor_name"]?.string
                        print(doctorInfo.doctor_name)
                        self.drNames.append(doctorInfo)
                    }
                    DispatchQueue.main.async {
                        self.doctorTagsTV.reloadData()
                    }
                    
                }
            }
        }
    }
}
extension DoctorsTagsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        print(drNames.count)
        return drNames.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.searchDoctorsCell) as! SearchDoctorsCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.tagBtn.setImage(UIImage(named: "fovourite-selected"), for: .normal)
        cell.doctorNameLbl.text = drNames[indexPath.row].doctor_name
        return cell
    }
    
    
}


struct ConsumerAddressModel {
    static var id = Int()
    var address_line1:String?
    var address_line2:String?
    var address_line3:String?
    var pincode:Int?
    var district:String?
    var state:String?
    var country:String?
    var long:String?
    var lat:String?
    var address_type:String?
}
struct ConsumerModel{

    var id:Int?
    var consumerID:String?
    var name:String?
    

    static var consumerId = Int()
    static var doctorId = Int()
    static var consumerName = String()

}
