//
//  TagsVC.swift
//  Yoopha
//
//  Created by Yoopha on 18/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Parchment
import Alamofire
import SVProgressHUD

class TagsVC: UIViewController {

    var backButtonStatus = false
    @IBOutlet weak var backNavBtn: UIButton!
    var itemArray=["Doctor","Pharmacies","Lab"]
    var fixedpgController:FixedPagingViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        if backButtonStatus == false{
            backNavBtn.isHidden = true
        }
        backNavBtn.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let doctorsTagsVC = storyboard.instantiateViewController(withIdentifier: "DoctorsTagsVC")
        let pharmaciesTagsVC = storyboard.instantiateViewController(withIdentifier: "PharmaciesTagsVC")
        let labsTagsVC = storyboard.instantiateViewController(withIdentifier: "LabsTagsVC")
        
        fixedpgController=FixedPagingViewController(viewControllers: [doctorsTagsVC,pharmaciesTagsVC,labsTagsVC])
        addChild(fixedpgController)
        view.addSubview(fixedpgController.view)
        view.constrainToEdges(fixedpgController.view)
        fixedpgController.didMove(toParent: self)
        fixedpgController.dataSource=self
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
          self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func tappedOnBackNavBtn(_ sender: Any) {
        
//        let navToMainMenu = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.mainMenuTBC) as! MainMenuTBC
//        //AppointmentsVC.appointmentVC.myColor = true
//        present(navToMainMenu, animated: false, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
    
}


extension TagsVC:PagingViewControllerDataSource{
    
    func numberOfViewControllers<T>(in pagingViewController: PagingViewController<T>) -> Int where T : PagingItem, T : Comparable, T : Hashable {
        return itemArray.count
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController where T : PagingItem, T : Comparable, T : Hashable {
        return fixedpgController.viewControllers[index]

    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T where T : PagingItem, T : Comparable, T : Hashable {
         return PagingIndexItem(index: index, title: itemArray[index]) as! T
    }
    
    
}
