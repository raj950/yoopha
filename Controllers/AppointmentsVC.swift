//
//  AppointmentVC.swift
//  Yoopha
//
//  Created by Yoopha on 14/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON
import Kingfisher
import Alamofire
import AudioToolbox
import SVProgressHUD
import SystemConfiguration
import SideMenuSwift

class AppointmentsVC: UIViewController,UITabBarControllerDelegate {

    @IBOutlet weak var appointmentStatusView: CustomView!
    @IBOutlet weak var appointmentStatusStack: UIStackView!
    @IBOutlet weak var notificationsLbl: CustomLabel!
    @IBOutlet weak var locationsBtn: UIButton!
    @IBOutlet weak var SOS_Constraint: NSLayoutConstraint!
    @IBOutlet weak var appView: UIView!
    @IBOutlet weak var slLbl: UILabel!
    static let appointmentVC = AppointmentsVC()
   // var get = MenuVC()
    var indexPath:Int?
    @IBOutlet weak var SOSBtn: CustomButton!
    @IBOutlet weak var shareAndLearnCV: UICollectionView!
    @IBOutlet weak var appointmentsCV: UICollectionView!
    @IBOutlet weak var trendingTopicsCV: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    var appointmentDetails:[patientsList]=[]
    var doctorData:[DoctorDeatils] = []
    var trendingTopicsArray:[PostsModel] = []
    var appointmentStatusData:[AppointmentStatusModel]=[]
    var communityModelArray:[CommunityModel]=[]
    var clinicInfo:[clinic]=[]
    var clinicData:[ClinicAddress]=[]
    var appointmentStatus = [Int] ()
    var appointActiveState:String?
    var x:Int?
    var y:Int?
    var month:String?
 //   let user = ["User","Labs","Clinic"]
    var sosButtonStatus = false
    var longPress:UILongPressGestureRecognizer?
    var selectedUrl:Int?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    navigationController?.isNavigationBarHidden = true
        
        SideMenuController.preferences.basic.direction = .left
        


        
        
        
        
        
//        let modelName = UIDevice.current.modelName
//        if modelName == "iPad" {
//           // SOS_Constraint.constant =  42
//        } else if modelName == "iPhone Xr"{
//            SOS_Constraint.constant =  100
//        }else if modelName == "iPhone Xs"{
//            SOS_Constraint.constant = 100
//        }
      

        let SOS_Status = UserDefaults.standard.bool(forKey: "status")
        
     
        if SOS_Status == true{
            
            
            SOSBtn.setImage(UIImage(named: "SOS_Active"), for:
                .normal)
            longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
            longPress?.minimumPressDuration = 2.0
            self.view.addGestureRecognizer(longPress!)
            
        }else{
            SOSBtn.setImage(UIImage(named: "SOS_Disable"), for: .normal)
        }
        searchBar.searchBarFont()
    
        self.searchBar.layer.cornerRadius = self.searchBar.bounds.height/2.0
        self.searchBar.clipsToBounds = true
        self.searchBar.delegate = self
        
        self.tabBarController?.delegate = UIApplication.shared.delegate as? UITabBarControllerDelegate
        
        cellRegistration()
       
        x = 0
        y = 0
      
       
           // self.getData()
    
       // get_SOS_By_UserId()
        
       
        
     //   SOSBtn.setImage(UIImage(named: "SOS_Disable"), for: .normal)
        
    }
    

    
    
    
    
    
    @objc func longPressAction(){
        
    }
    
 
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if sideMenu{
//            sideMenuConstraint.constant = 0
//        }else{
//            sideMenuConstraint.constant = -240
//        }
//
//        sideMenu = !sideMenu
//    }
    @IBAction func clk_menu(_ sender: Any) {
            sideMenuController?.revealMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       // getConsumer()
        getBlogsData()
        getDoctorAppointmentList()
        prescriptionList()
        getCommunityData()

    }

    @IBAction func tappedOnPreviousBtn(_ sender: Any) {
    
        let visibleItems: NSArray = self.trendingTopicsCV.indexPathsForVisibleItems as NSArray
        
        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        for itr in visibleItems {
            
            if minItem.row < (itr as AnyObject).row {
                minItem = itr as! NSIndexPath
            }
        }
        
        let nextItem = NSIndexPath(row: minItem.row - 1, section: 0)
        self.trendingTopicsCV.scrollToItem(at: nextItem as IndexPath, at: .right, animated: true)
    }
    
    @IBAction func tappedOnNextBtn(_ sender: Any) {
        
        
        let visibleItems: NSArray = self.trendingTopicsCV.indexPathsForVisibleItems as NSArray

        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        for itr in visibleItems {

            if minItem.row > (itr as AnyObject).row {
                minItem = itr as! NSIndexPath
            }
        }

        let nextItem = NSIndexPath(row: minItem.row + 1, section: 0)
        self.trendingTopicsCV.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
    }
    
    
    @IBAction func tappedOnLeftBtn(_ sender: Any) {
        let visibleItems: NSArray = self.appointmentsCV.indexPathsForVisibleItems as NSArray
        
        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        for itr in visibleItems {
            
            if minItem.row < (itr as AnyObject).row {
                minItem = itr as! NSIndexPath
            }
        }
        
        let nextItem = NSIndexPath(row: minItem.row - 1, section: 0)
        self.appointmentsCV.scrollToItem(at: nextItem as IndexPath, at: .right, animated: true)
    }
    
    @IBAction func tappedOnRightBtn(_ sender: Any) {
        if self.y == doctorData.count{
          //  simpleAlert(message: "No More Appointments")
            self.y = 0
           
        }else{
            let indexPath = IndexPath(item: y!, section: 0)
            self.appointmentsCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.y = self.y! + 1
        }
//        let visibleItems: NSArray = self.appointmentsCV.indexPathsForVisibleItems as NSArray
//
//        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
//        for itr in visibleItems {
//
//            if minItem.row > (itr as AnyObject).row {
//                minItem = itr as! NSIndexPath
//            }
//        }
//
//        let nextItem = NSIndexPath(row: minItem.row + 1, section: 0)
//        self.appointmentsCV.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
    }
    
    
   
    func prescriptionList(){

       // let baseUrl = "y08_eprescription/prescription_view_by_user_id/"
        let finalUrl = "\(Constants.API.base_URL)\(Constants.API.prescription_view_by_user_id)" + "\(loginModel.userid)/"
        print(finalUrl)
        WebApiCallBack.shared.GETServiceHeader(url: finalUrl, token: loginModel.token) { (response) -> (Void) in
            print(response)

            if response != nil{
                if let response = JSON(response).dictionary{

                    if let results = response["results"]?.array{

                        for result in results{
                            let userAppID = result["appointment"].int
                            print(userAppID)
                            
                            AppointmentStatusModel.patientAppointmentId = userAppID
                            print(AppointmentStatusModel.patientAppointmentId)
                            
                        }
                    }
                }
            }

        }

    }
    @IBAction func tappedOnLeftBtn1(_ sender: Any) {
        let visibleItems: NSArray = self.shareAndLearnCV.indexPathsForVisibleItems as NSArray
        
        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        for itr in visibleItems {
            
            if minItem.row < (itr as AnyObject).row {
                minItem = itr as! NSIndexPath
            }
        }
        
        let nextItem = NSIndexPath(row: minItem.row - 1, section: 0)
        self.shareAndLearnCV.scrollToItem(at: nextItem as IndexPath, at: .right, animated: true)
    }
    
    
    @IBAction func tappedOnRightBtn1(_ sender: Any) {
        
        let visibleItems: NSArray = self.shareAndLearnCV.indexPathsForVisibleItems as NSArray
        
        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        for itr in visibleItems {
            
            if minItem.row > (itr as AnyObject).row {
                minItem = itr as! NSIndexPath
            }
        }
        
        let nextItem = NSIndexPath(row: minItem.row + 1, section: 0)
        self.shareAndLearnCV.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
    }
  
       func cellRegistration(){
       
        let nib = UINib(nibName: Constants.Cell.trendingTopicsCVC, bundle: nil)
        trendingTopicsCV.register(nib, forCellWithReuseIdentifier: Constants.Cell.trendingTopicsCVC)
        
        let appointmentsCVC = UINib(nibName: "AppointmentsCVC", bundle: nil)
        appointmentsCV.register(appointmentsCVC, forCellWithReuseIdentifier: "AppointmentsCVC")
        
        let shareAndLearnCVC = UINib(nibName: "ShareAndLearnCVC", bundle: nil)
        shareAndLearnCV.register(shareAndLearnCVC, forCellWithReuseIdentifier: "ShareAndLearnCVC")
        
    }
    
    func getCommunityData(){
       
        let parameters = ["query": "query{ getThread(group: \"CONSUMER\", limit: 2) {cover title createdAt author{username} replies{id}}}"] as [String : Any]

        WebApiCallBack.shared.POSTService(serviceType:"https://api.community.yoopha.com", parameters: parameters as! [String : String]) { (response) -> (Void) in
            print(parameters)
            print(response)
            
            if response != nil{
                
               if let response = JSON(response).dictionary{
                    
                if  let data = response["data"]?.dictionary{
                    
                    if let getThread = data["getThread"]?.array{
                        
                        var communityData = CommunityModel()
                        for info in getThread{
                            
                            
                           
                            communityData.cover = info["cover"].string
                            communityData.title = info["title"].string
                            communityData.createdAt = info["createdAt"].string
                            let IntValue = Int(communityData.createdAt!)
                            let date = NSDate(timeIntervalSince1970: TimeInterval(IntValue!))
                            print(date)
                            
                            
                           // let replies = info["replies"].array
                          //  communityData.replies = replies!["replies"].string
                            
                           
                            
                               //  print(communityData.cover)
                            let authorName = info["author"].dictionary
                            communityData.userName = authorName!["username"]?.string
                            
                            self.communityModelArray.append(communityData)
                            
                        }
                        DispatchQueue.main.async {
                            self.shareAndLearnCV.reloadData()
                        }
                   
                    }
                        
                    }
                }
            }
        }
    }
    @IBAction func tappedOnOrderMedicine(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.E_PrescriptionVC) as! E_PrescriptionVC
        navTo.backButtonStatus = true
        navigationController?.pushViewController(navTo, animated: true)
    }
    
    @IBAction func teppedOnLabs(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.labResultsVC) as! LabResultsVC
        navTo.backButtonStatus = true
        navigationController?.pushViewController(navTo, animated: true)
    }
    @IBAction func tappedOnHealthRecords(_ sender: Any) {
//        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.labResultsVC) as! LabResultsVC
//        navTo.backButtonStatus = true
//        navigationController?.pushViewController(navTo, animated: true)
    }
    @IBAction func tappedOnAppointments(_ sender: Any) {
        
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.yourAppointmentVC) as! YourAppointmentVC
            navTo.backButtonStatus = true
        navigationController?.pushViewController(navTo, animated: true)
        
    }
    func getBlogsData(){
        let params = NSMutableDictionary()
     
        WebApiCallBack.makeGetCall(webUrl: Constants.API.blogs, paramData: params) { (response, error) in
            print(response)
            
            if response != nil{
                if let blogResponse = JSON(response).dictionary{
                    
                    if  let posts = blogResponse["posts"]?.array{
                        var postData = PostsModel()
                        for post in posts{
                            
                            postData.title = post["title"].string
                            postData.excerpt = post["excerpt"].string
                            postData.feature_image = post["feature_image"].string
                            
                            let postDate = post["published_at"].string
                            let first4 = postDate!.prefix(4)
                            
                            let startIndex = postDate!.index(postDate!.startIndex, offsetBy: 5)
                            let endIndex = postDate!.index(postDate!.startIndex, offsetBy: 6)
                            let monthtrim = String(postDate![startIndex...endIndex])
                            let monthNameGet = DateFormatter().monthSymbols[Int(monthtrim)! - 1]
                            
                            
                            let one = postDate!.index(postDate!.startIndex, offsetBy: 8)
                            let two = postDate!.index(postDate!.startIndex, offsetBy: 9)
                            let dates = String(postDate![one...two])
                            
                            self.month = first4 + "-" + monthNameGet + "-"  + dates
                             postData.published_at = self.month
                            postData.url = post["url"].string
                            self.trendingTopicsArray.append(postData)
                            
                        }
                        DispatchQueue.main.async {
                            self.trendingTopicsCV.reloadData()
                        }  
                    }
                }
            }
        }
    }
    //MARK: Doctor Appointments List
    func getDoctorAppointmentList(){
       
        let url1 = "\(Constants.API.base_URL)\(Constants.API.doctor_appointment_view_by_user)" + Constants.ConstantStrings.slash
        let finalUrl = url1 + "\(loginModel.userid)" + Constants.ConstantStrings.slash
            let url = URL(string: finalUrl)
        
        WebApiCallBack.shared.GETServiceHeader(url: finalUrl, token: loginModel.token) { (response) -> (Void) in
                print(response)
            
            if let response = JSON(response).dictionary{
                print(response)
                
                if let results = response["results"]?.array{
                    var getPatientDeatils = patientsList()
                    for result in results{
                        
                        let bookingDate = result["booking_date"].string
                        let slug = result["slug"].string
                        PatientAppointmentsModel.patientSlugId = slug
                        print(PatientAppointmentsModel.patientSlugId!)
                        let appointmentToken = result["token_no"].int
                        let doctorData = result["doctor"].dictionary
                        var doctorDetails = DoctorDeatils()
                        doctorDetails.doctor_name = doctorData!["doctor_name"]?.string
                        doctorDetails.doctor_education = doctorData!["doctor_education"]?.string
                        doctorDetails.id = doctorData!["id"]?.int
                        let doctorProfileImg = doctorData!["doctor_profile_image"]?.array
                        for getImage in doctorProfileImg!{
                            doctorDetails.doctorProfileImage = getImage["upload"].string ?? "user_profile"
                        }
                        self.doctorData.append(doctorDetails)
                        ConsumerModel.doctorId = doctorDetails.id!
//                        let clinicData = result["clinic_slot"].dictionary
//                        let slot = clinicData!["slot"]?.dictionary
//                        let startTime = slot!["start_time"]?.string
//                        let endTime = slot!["end_time"]?.string
//                        var clinicAdd = clinic()
//                        var clinicAddLine = ClinicAddress()
//                        let clinic = clinicData!["clinic"]?.dictionary
//                        clinicAdd.clinic_name = clinic!["clinic_name"]?.string
//                        let clinicAddress = clinic!["clinic_address"]?.dictionary
//                        let addLine = clinicAddress!["address_line3"]?.string
//                        clinicAddLine.address_line3 = addLine
                        var doctorSpeciality = DoctorSpecialities()
                        let doctorSpecialityData = doctorData?["doctor_speciality"]?.dictionary
                        doctorSpeciality.speciality_name = doctorSpecialityData!["speciality_name"]?.string ?? ""
                        var appointmentStatusDetails = AppointmentStatusModel()
                        let appointmentStatus = result["appointment_status"].dictionary
                        appointmentStatusDetails.status = appointmentStatus!["status"]?.string
                      //  print(appointmentStatusDetails.status)
                        getPatientDeatils.patientToken = appointmentToken
                        getPatientDeatils.doctorName = doctorDetails.doctor_name
                        getPatientDeatils.doctorSpeciality = doctorSpeciality.speciality_name
                        getPatientDeatils.bookingDate = bookingDate
                      //  getPatientDeatils.startTime = startTime
                       // getPatientDeatils.endTime = endTime
                //        getPatientDeatils.clinicName = clinicAdd.clinic_name
                        getPatientDeatils.patientSlug = slug
                   //
                     //   self.clinicInfo.append(clinicAdd)
                       // self.clinicData.append(clinicAddLine)
                        self.appointmentDetails.append(getPatientDeatils)
                        self.appointmentStatusData.append(appointmentStatusDetails)
                    }
                    //                            print(self.appointmentDetails)
                    //                            print(self.appointActiveState as Any)
                    DispatchQueue.main.async {
                        self.appointmentsCV.reloadData()
                    }
                }
            }
        }
        
    }
   
    @objc func navTo(_ sender:UIButton){
        print(loginModel.profile)
        
        if loginModel.userProfile == "Y"{
            let navTo =  self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.searchDoctorVC) as! SearchDoctorVC
            if WebApiCallBack.isConnectedToNetwork() != true {
                simpleAlert(message: "Please verify internet connectivity.")
                return
            }else{
            self.navigationController?.pushViewController(navTo, animated: true)
            }
        }
        else{
        alertWithMessage(title: "Create Profile", message: "Create your profile for book an appointment", cancelButtonTitle: "Cancel", buttonTitle: "Create", handler: { (action) in
            let navTo = self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.profileVC) as! ProfileVC
            
            self.navigationController?.pushViewController(navTo, animated: true)
          
        }) { (action) in
          
            self.resignFirstResponder()
        }
        
        }
        
      
    }
    
    
    @IBAction func tappedOnLocationsBtn(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.locationsVC) as! LocationsVC
        self.navigationController?.pushViewController(navTo, animated: true)
        
    }
    
    @IBAction func tappedOnNotificationsBtn(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.notificationVC) as! NotificationVC
        self.navigationController?.pushViewController(navTo, animated: true)
        
        
    }
    @IBAction func tappedOnSOSBtn(_ sender: UIButton) {
        
        let SOS_Status = UserDefaults.standard.bool(forKey: "status")
        
        if SOS_Status == true{
                let navTo = self.storyboard?.instantiateViewController(withIdentifier:"SOS_StateVC") as! SOS_StateVC
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            self.navigationController?.pushViewController(navTo, animated: true)
      
        }else{
            
            let navTo = self.storyboard?.instantiateViewController(withIdentifier:"CreateEmegencySOS_VC") as! CreateEmegencySOS_VC
            self.navigationController?.pushViewController(navTo, animated: true)
            
        }
    
    }

}
extension AppointmentsVC:UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        let navToConsumerSearchVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.consumerSearchVC) as! ConsumerSearchVC
        if WebApiCallBack.isConnectedToNetwork() != true {
            simpleAlert(message: "Please verify internet connectivity.")
            return
        }else{
            self.navigationController?.pushViewController(navToConsumerSearchVC, animated: true)
            self.searchBar.endEditing(true)
        }
    }
    
}

extension AppointmentsVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == appointmentsCV{
           print(doctorData.count)
           // let t = NSIndexPath(index: doctorData.count)
           // print(t)
            return doctorData.count
        }else if collectionView == trendingTopicsCV{
           // print(trendingTopicsArray.count)
            return trendingTopicsArray.count
        }else if collectionView == shareAndLearnCV{
            return communityModelArray.count
        }
        else{
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        self.indexPath = indexPath.row
        if collectionView == appointmentsCV{
            
            let url = URL(string: doctorData[indexPath.row].doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
          //  let clinicAddress = clinicData[indexPath.row].address_line3
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppointmentsCVC", for: indexPath) as! AppointmentsCVC
            cell.drNameLbl.text = doctorData[indexPath.row].doctor_name
            cell.tokenLbl.text = "\(appointmentDetails[indexPath.row].patientToken!)"
            cell.datelbl.text = appointmentDetails[indexPath.row].bookingDate
            cell.specialityLbl.text = appointmentDetails[indexPath.row].doctorSpeciality
            cell.specializationLbl.text = doctorData[indexPath.row].doctor_education
         //   cell.startTimeLbl.text = appointmentDetails[indexPath.row].startTime!
        //    cell.clinicAddressLbl.text = clinicInfo[indexPath.row].clinic_name! + "," + clinicAddress!
            cell.drProfile.kf.setImage(with: url!)
            
            
            let start = appointmentDetails[indexPath.row].startTime?.timeConversion12(time24:(appointmentDetails[indexPath.row].startTime)!)
            let end  = appointmentDetails[indexPath.row].endTime?.timeConversion12(time24:(appointmentDetails[indexPath.row].endTime)!)
            
            let trim = start?.stringTrims(trim: start!)
            let trim1 = end?.stringTrims(trim: end!)
            
         //  cell.startTimeLbl.text = trim! + "-" + trim1!
           // cell.endTimeLbl.text = trim1
            
             return cell
        }else if collectionView == shareAndLearnCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShareAndLearnCVC", for: indexPath) as! ShareAndLearnCVC
            let url = URL(string: communityModelArray[indexPath.row].cover!)
            cell.titleLbl.text = communityModelArray[indexPath.row].title
            cell.nameLbl.text = communityModelArray[indexPath.row].userName
            cell.dateLbl.text = communityModelArray[indexPath.row].createdAt
          //  cell.messageCountLbl.text = communityModelArray[indexPath.row].replies
            cell.coverImage.kf.setImage(with: url)
            
            return cell
        }else if collectionView == trendingTopicsCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingTopicsCVC", for: indexPath) as! TrendingTopicsCVC
            let url = URL(string: trendingTopicsArray[indexPath.row].feature_image!)
            cell.titleLbl1.text = trendingTopicsArray[indexPath.row].title
            cell.descriptionLbl.text = trendingTopicsArray[indexPath.row].excerpt
         //   cell.dateLbl.text = trendingTopicsArray[indexPath.row].published_at
            cell.coverImage.kf.setImage(with: url)
           // cell.readNowBtn.addTarget(self, action: #selector(tappedOn), for: .touchUpInside)
         //   cell.readNowBtn.tag = indexPath.row
           //  cell.profileImage.kf.setImage(with: url)
          //    let url = URL(string: final[indexPath.row].doctor[0].doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
            
          //  cell.cellDelegate = self
            cell.index = indexPath
            
            return cell
        }
        
        return UICollectionViewCell()
    }

    @objc func tappedOn(_ sender:UIButton){
            UIApplication.shared.open(URL(string: trendingTopicsArray[sender.tag].url!)! as URL, options: [:], completionHandler: nil)
    print("rajasekhar")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == appointmentsCV{
            let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.appointmentDetailsVC) as! AppointmentDetailsVC
            
            navTo.savedetails = appointmentDetails
            navTo.selectedPatientDetails = indexPath.row
            self.navigationController?.pushViewController(navTo, animated: true)
        }else if collectionView == trendingTopicsCV{
         UIApplication.shared.open(URL(string: trendingTopicsArray[indexPath.row].url!)! as URL, options: [:], completionHandler: nil)
       // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingTopicsCVC", for: indexPath) as! TrendingTopicsCVC
          //  print(cell.readNowBtn.tag)
        }else if collectionView == shareAndLearnCV{
            UIApplication.shared.open(URL(string: communityUrl)!, options: [:], completionHandler: nil)
        }
        else{
            
        }
    }
    
   
    
}

struct AppointmentStatusModel{
    var id:Int?
    var status:String?
    var description:String?
    var appointmentId:Int?
    static var tagStatus:Bool?
    static var patientAppointmentId:Int?
   

}


struct PostsModel{
    var title:String?
    var excerpt:String?
    var feature_image:String?
    var url:String?
    var published_at:String?
}

struct CommunityModel {
    var cover:String?
    var title:String?
    var createdAt:String?
    var replies:String?
    var userName:String?
}



//
//func getData(){
//    let url = URL(string: "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y07_appointment/Doctor_appointment_view_by_user/6/")
//    URLSession.shared.dataTask(with:url!, completionHandler: {(data, response, error) in
//        guard let data = data, error == nil else { return }
//
//
//        do {
//            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
//            print(json)
//
//
//            let posts = json["posts"] as? [[String: Any]] ?? []
//            print(posts)
//        } catch let error as NSError {
//            print(error)
//        }
//    }).resume()
//}
