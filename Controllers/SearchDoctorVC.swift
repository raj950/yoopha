//
//  SearchDoctorVC.swift
//  Yoopha
//
//  Created by Yoopha on 20/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON
import Kingfisher
import SVProgressHUD


class SearchDoctorVC: UIViewController,passTagIndex{

    func passIndexValue(name: String?, img: UIImage?) {
        print(name)
        print(img)
        updateTag = name!
        setImg = img
        self.searchDoctorTV.reloadData()
   
    }

    var final:[DoctorClinicResult] = []
    var test2:[clinicDetails] = []
    var clinicAddressarr:[ClinicAddress] = []
    var doctorSpecialities:[DoctorSpecialities] = []
    var doctorAddreee:[DoctorAddress] = []
    var doctorDetails:[DoctorDeatils] = []
    var tagsData:[TagsResultModel]=[]
    var searchList = [DoctorSpecialities]()
    var searchResultArr = [DoctorClinicResult]()
    
    var count :String?
    var nextt : String?
    var previous : String?
    var slugs = String()
    let date = Date()
    var updateTag:String?
    var setImg:UIImage?
    var doctor_speciality = NSMutableDictionary()
    var address = NSMutableDictionary()
    
    var backButtonStatus = true
    var searchStatus = true
    var searching = false
    
    @IBOutlet weak var searchDoctorTV: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        cellRegistration()
        DispatchQueue.main.async {
            self.getData()
          
        }
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.searchDoctorTV.reloadData()
    }
    
    func cellRegistration(){
        
        let searchDoctorCell = UINib(nibName: Constants.Cell.searchDoctorsCell, bundle: nil)
        searchDoctorTV.register(searchDoctorCell, forCellReuseIdentifier: Constants.Cell.searchDoctorsCell)
        searchDoctorTV.showsVerticalScrollIndicator = false
        searchDoctorTV.separatorStyle = .none
    
    }
    
    @IBAction func navBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }

    func getData(){
        
        let myParam = NSMutableDictionary()
        WebApiCallBack.shared.GETService(serviceType: Constants.API.searchDoctor1, parameters: myParam as! [String : String]) { (response) -> (Void) in
            print(response)
            if response != nil {
                  DispatchQueue.global(qos: .userInteractive).sync {
                let mresponse = JSON(response)
                if let serverMessage = mresponse.dictionary{
                    self.count = serverMessage["count"]?.stringValue
                    self.nextt = serverMessage["next"]?.stringValue
                    self.previous = serverMessage["previous"]?.stringValue
                    if let results = serverMessage["results"]?.array{
                        // print(results)
                        
                        for result in results{
                            var doctorClinic = DoctorClinicResult()
                            doctorClinic.id = result["id"].int
                            doctorClinic.slug = result["slug"].string
                            self.slugs = doctorClinic.slug!
                            
                          //  print(self.slugs)
                
                            let clinicData = result["clinic_slot"].array
                            for getinfo in clinicData!{
                                var clinicData = clinicDetails()
                                clinicData.id = getinfo["id"].int
                                clinicData.slotID =  getinfo["slotID"].string
                                clinicData.slung = getinfo["slug"].string
                                var clinicvalues = getinfo["clinic"].dictionary
                                var clinics = clinic()
                                clinics.id = clinicvalues!["id"]?.int
                                clinics.clinicID = clinicvalues!["clinicID"]?.string
                                clinics.clinic_name = clinicvalues!["clinic_name"]?.string
                                clinics.clinic_contact_no = clinicvalues!["clinic_contact_no"]?.string
                                var getAddress = clinicvalues!["clinic_address"]?.dictionary
                                var clinicAdd = ClinicAddress()
                                clinicAdd.id = getAddress!["id"]?.int
                                clinicAdd.address_line1 = getAddress!["address_line1"]?.string
                                clinicAdd.address_line2 = getAddress!["address_line2"]?.string
                                clinicAdd.address_line3 = getAddress!["address_line3"]?.string
                                clinicAdd.pincode = getAddress!["pincode"]?.int
                                clinicAdd.district = getAddress!["district"]?.string
                                clinicAdd.state = getAddress!["state"]?.string
                                clinicAdd.country = getAddress!["country"]?.string
                                clinicAdd.long = getAddress!["long"]?.string
                                clinicAdd.lat = getAddress!["lat"]?.string
                                clinicAdd.address_type = getAddress!["address_type"]?.int
                                clinics.clinic_address.append(clinicAdd)
                                clinicData.clinicData.append(clinics)
                                var slotValue = getinfo["slot"].dictionary
                                var slotDetails = SlotDeatils()
                                slotDetails.id = slotValue!["id"]?.int
                                slotDetails.day = slotValue!["day"]?.string
                                slotDetails.schedule = slotValue!["schedule"]?.string
                                slotDetails.start_time = slotValue!["start_time"]?.string
                                slotDetails.end_time = slotValue!["end_time"]?.string
                                clinicData.slot.append(slotDetails)
                                doctorClinic.clinic_slot.append(clinicData)
                                
                            }
                            let doctorData = result["doctor"].dictionary
                            var doctorDetails = DoctorDeatils()
                            doctorDetails.id = doctorData!["id"]?.int
                            doctorDetails.doctorID = doctorData!["doctorID"]?.string
                            doctorDetails.doctor_name = doctorData!["doctor_name"]?.string
                            doctorDetails.doctor_education = doctorData!["doctor_education"]?.string
                           
                            
                            doctorDetails.practice_started_year =  doctorData!["practice_started_year"]?.int
                            doctorDetails.gender = doctorData!["gender"]?.string
                            doctorDetails.doctor_license = doctorData!["doctor_license"]?.string
                            doctorDetails.doctor_desc = doctorData!["doctor_desc"]?.string
                            let doctorProfileImg = doctorData!["doctor_profile_image"]?.array
                        //    print(doctorDetails.id as Any)
                            for getImage in doctorProfileImg!{
                                doctorDetails.doctorProfileImage = getImage["upload"].string ?? "user_profile"
                            }
                         
                            var drspeciality = DoctorSpecialities()
                            var drsp = doctorData!["doctor_speciality"]?.dictionary
                            drspeciality.speciality_name = drsp!["speciality_name"]?.string
                            drspeciality.id = drsp!["id"]?.int
                            drspeciality.desc = drsp!["desc"]?.string
                            doctorDetails.doctor_speciality.append(drspeciality)
                            doctorDetails.membership_details = doctorData!["membership_details"]?.string
                            doctorDetails.registration_no = doctorData!["registration_no"]?.string
                            doctorDetails.registration_council = doctorData!["registration_council"]?.string
                            doctorDetails.languages_known = doctorData!["languages_known"]?.string
                            doctorDetails.consultation_fee = doctorData!["consultation_fee"]?.string
                            doctorDetails.payment_type = doctorData!["payment_type"]?.string
                            doctorDetails.user = doctorData!["user"]?.int
                            var draddress = DoctorAddress()
                            var dradd = doctorData!["address"]?.dictionary
                            draddress.id = dradd!["id"]?.int
                            //   print(draddress.id)
                            draddress.address_line1 = dradd!["address_line1"]?.string
                            draddress.address_line2 = dradd!["address_line2"]?.string
                            draddress.address_line3 = dradd!["address_line3"]?.string
                            draddress.pincode = dradd!["pincode"]?.int
                            draddress.district = dradd!["district"]?.string
                            draddress.state = dradd!["state"]?.string
                            draddress.country = dradd!["country"]?.string
                            draddress.long = dradd!["long"]?.string
                            draddress.lat = dradd!["lat"]?.string
                            draddress.address_type = dradd!["address_type"]?.int
                            doctorDetails.address.append(draddress)
                            doctorClinic.doctor.append(doctorDetails)
                            self.final.append(doctorClinic)
            
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.searchDoctorTV.reloadData()
                  //  print(self.final.count)
                }
                
            }
            
            }
            
          
        }
            
        }

}

extension SearchDoctorVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchStatus{

            return self.final.count

        }else{
            
            return final.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let AddressLine = final[indexPath.row].clinic_slot[0].clinicData[0].clinic_address[0].address_line3
        
        if searchStatus == true{
            let cell = tableView.dequeueReusableCell(withIdentifier:   Constants.Cell.searchDoctorsCell) as! SearchDoctorsCell
           
         
            let url = URL(string: final[indexPath.row].doctor[0].doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
            cell.doctorNameLbl.text = Dr + final[indexPath.row].doctor[0].doctor_name!
            cell.profileImage.kf.setImage(with: url)
            cell.clinicName.text = final[indexPath.row].clinic_slot[0].clinicData[0].clinic_name! + " -" + AddressLine!
            cell.doctorSpecialityLbl.text = final[indexPath.row].doctor[0].doctor_speciality[0].speciality_name!
            cell.drEducationLbl.text = final[indexPath.row].doctor[0].doctor_education
            
//
//            if updateTag ==  cell.doctorNameLbl.text{
//                print(updateTag!)
//                print(final[indexPath.row].doctor[0].doctor_name!)
//              //  cell.tagBtn.setImage(setImg, for: .normal)
//
//
//            }

            
            
            
           // cell.experienceLbl.text = "\(exp)" + " " +  Constants.ConstantStrings.year
            
//            if updateTag == cell.doctorNameLbl.text{
//
//                cell.tagBtn.setImage(setImg, for: .normal)
//
//
//            }
            
//            cell.cellDelegate = self
//            cell.index = indexPath
//            cell.bookBtn.tag = indexPath.row
//            cell.tagBtn.tag = indexPath.row
            
            
            return cell
            
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier:   Constants.Cell.searchDoctorsCell) as! SearchDoctorsCell
            
            let url = URL(string: final[indexPath.row].doctor[0].doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
            cell.doctorNameLbl.text = final[indexPath.row].doctor[0].doctor_name
            cell.profileImage.kf.setImage(with: url)
            cell.clinicName.text = final[indexPath.row].clinic_slot[0].clinicData[0].clinic_name! + " -" + AddressLine!
            cell.doctorSpecialityLbl.text = final[indexPath.row].doctor[0].doctor_speciality[0].speciality_name
            cell.drEducationLbl.text = final[indexPath.row].doctor[0].doctor_education
            
//            if updateTag! == cell.doctorNameLbl.text!{
//                print(updateTag!)
//                print(final[indexPath.row].doctor[0].doctor_name!)
//                cell.tagBtn.setImage(setImg, for: .normal)
//
//                
//            }
//

//            cell.feeLbl.text = Constants.ConstantStrings.rupee + " " + doctorFee!
//
            
            
        //    let exp = date.GetYear() - final[indexPath.row].doctor[0].practice_started_year!
            
          //  cell.experienceLbl.text = "\(exp)" + " " +  Constants.ConstantStrings.year
//            if updateTag == cell.doctorNameLbl.text{
//
//                cell.tagBtn.setImage(setImg, for: .normal)
//
//
//            }
//
//            cell.cellDelegate = self
//            cell.index = indexPath
//            cell.bookBtn.tag = indexPath.row
//            cell.tagBtn.tag = indexPath.row
//
            
            return cell
            
        }
//        if tableView == sortingTV{
//            let cell = tableView.dequeueReusableCell(withIdentifier:Constants.Cell.sortTVC) as! SortTVC
//            cell.sortLbl.text = "\(priceArray[indexPath.row])"
//            return cell
//        }
        if tableView == searchDoctorTV{
            let cell = tableView.dequeueReusableCell(withIdentifier:   Constants.Cell.searchDoctorsCell) as! SearchDoctorsCell
            let doctorFee = final[indexPath.row].doctor[0].consultation_fee
            let url = URL(string: final[indexPath.row].doctor[0].doctorProfileImage ??  Constants.ConstantStrings.defaultImage)
//            cell.bookBtn.addTarget(self, action: #selector(navTo), for: .touchUpInside)
//            cell.tagBtn.addTarget(self, action: #selector(tag), for: .touchUpInside)
//            cell.doctorNameLbl.text = final[indexPath.row].doctor[0].doctor_name
            
//            
//            if updateTag! == cell.doctorNameLbl.text{
//                
//           //     cell.tagBtn.setImage(setImg, for: .normal)
//                
//                
//            }
//            cell.profileImage.kf.setImage(with: url)
//            cell.doctorSpecialityLbl.text = final[indexPath.row].doctor[0].doctor_speciality[0].speciality_name
//            cell.feeLbl.text = Constants.ConstantStrings.rupee + " " + doctorFee!
//            cell.clinicName.text = final[indexPath.row].clinic_slot[0].clinicData[0].clinic_name
//
        
            
            let exp = date.GetYear() - final[indexPath.row].doctor[0].practice_started_year!
            
          //  cell.experienceLbl.text = "\(exp)" + " " +  Constants.ConstantStrings.year
            
           
//            cell.cellDelegate = self
//            cell.index = indexPath
//            cell.bookBtn.tag = indexPath.row
//            cell.tagBtn.tag = indexPath.row
//

            return cell
        }
    }
     @objc func tag(_ sender:UIButton){
        print(sender.tag)
        AppointmentStatusModel.tagStatus = true
        sender.buttonTag(btnImg: sender, consumerId: ConsumerModel.consumerId, DRId: final[sender.tag].doctor[0].id!)
        
//        if sender.currentImage == UIImage(named: "Fovourite-final"){
//
//            sender.setImage(UIImage(named: "fovourite-selected"), for: .normal)
//
//            let myParams = ["consumer":ConsumerModel.consumerId,"doctor":final[sender.tag].doctor[0].id]
//            print(myParams)
//            let url = "\(Constants.API.base_URL)\(Constants.API.create_consumerDoctorTag)"
//            WebApiCallBack.shared.POSTServiceHeader(serviceType: url, parameters: myParams as [String : Any], token:loginModel.token) { (response) -> (Void) in
//                print(response)
//            }
//
//        }else{
//
////            for i in 0..<tagsData.count{
////
////
////                print(tagsData[i].id)
////
////            }
//
//            sender.setImage(UIImage(named: "Fovourite-final"), for: .normal)
//            let params = NSMutableDictionary()
//            let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y06_tags/"
//            let url2 = ConsumerModel.consumerId
//            let finalUrl = url + "\(url2)" + "/ConsumerDoctor_untag/"
//            print(finalUrl)
//            WebApiCallBack.makeDeleteCall(webUrl: finalUrl, paramData: params) { (response, error) in
//
//                print(response)
//
//
//
//            }
//        }
    }
//    @objc func navTo(_ sender:UIButton){
//
//
//        let navTo = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.doctorDetailedVC) as! DoctorDetailedVC
//        navTo.goPath = self
//        navTo.tagIndex = sender.tag
//         let url = URL(string: final[sender.tag].doctor[0].doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
//
//          DRInfo.doctorProfileImage = "\(url!)"
//          DRInfo.doctorName = final[sender.tag].doctor[0].doctor_name
//        navTo.drName = DRInfo.doctorName
//          DRInfo.doctorSpeciality = final[sender.tag].doctor[0].doctor_speciality[0].speciality_name
//          DRInfo.doctorFee = final[sender.tag].doctor[0].consultation_fee
//          DRInfo.doctorExp = final[sender.tag].doctor[0].practice_started_year
//
//
//        DRInfo.getClinicData = final[sender.tag].clinic_slot
//        self.navigationController?.pushViewController(navTo, animated: true)
//       // present(navTo, animated: true, completion: nil)
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        if tableView == searchDoctorTV{
            let navToDoctorDetailsVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.doctorDetailedVC) as! DoctorDetailedVC
            
        // let url = URL(string: final[indexPath.row].doctor[0].doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
            
            DRInfo.doctorName = final[indexPath.row].doctor[0].doctor_name ?? ""
            DRInfo.doctorSpeciality = final[indexPath.row].doctor[0].doctor_speciality[0].speciality_name ?? ""
         //   DRInfo.doctorFee = final[indexPath.row].doctor[0].consultation_fee ?? ""
            DRInfo.doctorExp = final[indexPath.row].doctor[0].practice_started_year ?? 0
            DRInfo.doctorDiscription = final[indexPath.row].doctor[0].doctor_desc ?? ""
            DRInfo.id                = final[indexPath.row].doctor[0].id ?? 0
            DRInfo.getClinicData = final[indexPath.row].clinic_slot
            DRInfo.doctorProfileImage = final[indexPath.row].doctor[0].doctorProfileImage
            DRInfo.doctorEducation = final[indexPath.row].doctor[0].doctor_education
         
         self.navigationController?.pushViewController(navToDoctorDetailsVC, animated: true)
        }
    }


}

extension SearchDoctorVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchStatus = false
        self.final.removeAll()
        getData()
        DispatchQueue.main.async {
           self.searchDoctorTV.reloadData()

        }
    }
    
}
extension String {
    func trimWhiteSpace() -> String {
        let string = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return string
    }
}
extension SearchDoctorVC:tableviewCell{
    func onClickCell(index: Int) {
        print("\(index)")
    }
    
    
}



struct DoctorClinicResult {
    var clinic_slot:[clinicDetails] = []
    var doctor:[DoctorDeatils] = []
    var id:Int?
    var slug:String?
    init(){}
}

struct clinicDetails {
    var id:Int?
    var slotID: String?
    var clinicData:[clinic] = []
    var slot:[SlotDeatils] = []
    var slung:String?
    
}
struct ClinicAddress{
    var  id: Int?
    var  address_line1: String?
    var address_line2: String?
    var address_line3: String?
    var pincode: Int?
    var district: String?
    var state: String?
    var country: String?
    var long: String?
    var lat:String?
    var address_type: Int?
    
}
struct DoctorDeatils {
    var id: Int?
    var doctorID: String?
    var doctor_name: String?
    var doctor_education: String?
    var practice_started_year: Int?
    var gender:String?
    var doctor_license: String?
    var doctor_desc: String?
    var doctor_speciality:[DoctorSpecialities] = []
    var membership_details: String?
    var registration_no: String?
    var registration_council: String?
    var languages_known: String?
    var consultation_fee: String?
    var payment_type: String?
    var user: Int?
    var address:[DoctorAddress] = []
    var doctorProfileImage : String?
    
    
}
struct DoctorAddress {
    var id: Int?
    var address_line1: String?
    var address_line2: String?
    var address_line3: String?
    var pincode: Int?
    var district: String?
    var state: String?
    var country: String?
    var coordinates: String?
    var long: String?
    var lat: String?
    var address_type: Int?
}
struct DoctorSpecialities {
    var id: Int?
    var speciality_name: String?
    var desc: String?
}
struct SlotDeatils{
    var id: Int?
    var day: String?
    var schedule: String?
    var start_time: String?
    var end_time: String?
    
}

struct clinicSlot{
    var id:Int?
    var slotID : String?
    var slug:String?
    var clinicName:String?

}
struct clinic{
    
    var id:Int?
    var clinicID:String?
    var clinic_name:String?
    var clinic_contact_no:String?
    var clinic_address:[ClinicAddress] = []
}
struct DRInfo {
    static var id:Int?
    static var doctorName:String?
    static var doctorSpeciality:String?
    static var doctorFee:String?
    static var doctorDiscription:String?
    static var doctorExp:Int?
    static var doctorProfileImage:String? 
    static var doctorEducation:String?
    static var getClinicData:[clinicDetails] = []

}

struct PharmacyDataModel {
    var pharmacyName : String?
    var pharmacyId : String?
    var id : Int?
    
    var address:[address]=[]
}

struct address {
    var address_line1 : String?
    var address_line2 : String?
    var address_line3 : String?
    var pincode:Int?
    var district:Int?
    var state:Int?
    var country:Int?
    var address_type:Int?
    var long:String?
    var lat:String?
    

}

//    func searchPharmacy(){
//
//        let url = "https://testapi.yoopha.com/api/v1/y02_pharmacy/search_pharmacy/?limit=10&offset=0&search="
//        WebApiCallBack.shared.GETService123(extraParam: url) { (response) -> (Void) in
//            print(response)
//
//            if response != nil{
//                if let response = JSON(response).dictionary{
//                   if let results = response["results"]?.array{
//
//                    var pharmacyData = PharmacyDataModel()
//
//                    for result in results{
//
//                        pharmacyData.pharmacyName = result["pharmacyName"].string
//
//                        print(pharmacyData.pharmacyName)
//
//
//                    }
//
//                    }
//                }
//            }
//        }
//
//    }
