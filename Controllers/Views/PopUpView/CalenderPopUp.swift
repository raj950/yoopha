//
//  CalenderPopUp.swift
//  Yoopha
//
//  Created by Yoopha on 10/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Foundation
import DropDown

class CalenderPopUp: UIView,UITextFieldDelegate{
    @IBOutlet weak var eveningBtn: CustomButton!
    
    @IBOutlet weak var noonBtn: CustomButton!
    @IBOutlet weak var morningBtn: CustomButton!
    @IBOutlet weak var dateTF: DropDownTextField!
    @IBOutlet weak var confirmBtn: CustomButton!
    static let calenderInstance = CalenderPopUp()
    var patientsNames:[patientsList] = []
    let picker = UIPickerView()
    let datepicker=UIDatePicker()
    let toolBar = UIToolbar()
    let currentDate = Date()
    
    
    
    var clinicslots:[String] = []
    var clinicStart:String?
    var clinicEnd:String?
    var clinicDay:String?
    var clinicOpens:String?
    var schedule:String?
    var clinicSlotIdM:Int?
    var currentYear:Int = 0
    var month:Int = 0
    var slotInfo:[SlotDeatils]=[]
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet var parentView: UIView!
    
    @IBOutlet weak var popupViwe: UIView!
    
    
    @IBOutlet weak var clinicTF: DropDownTextField!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("CalenderPopUp", owner: self, options: nil)
        commonInit()
        getClinic()
        dateTF.delegate = self
        clinicTF.textColor = UIColor.white
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func commonInit() {
        
        
        popupViwe.layer.cornerRadius = 10
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    enum AlertType {
        case success
        case failure
    }
    func getClinic(){
        for i in 0..<DRInfo.getClinicData.count{
            
            if clinicslots.contains(where: {$0 == DRInfo.getClinicData[i].clinicData[0].clinic_name!}) {
                
                
            }
            else{
                clinicslots.append(DRInfo.getClinicData[i].clinicData[0].clinic_name!)
                
            }
            
            
        }
        print(clinicslots)
        
        clinicTF.datasource = clinicslots
    }
    func showAlert(title: String, message: String, alertType: AlertType,clinicName:String,slotData:[SlotDeatils]) {
       
        print(slotData)
        
        switch alertType {
        case .success:
          
           CalenderPopUp.calenderInstance.clinicTF.text = clinicName
           slotInfo = slotData
          
        case .failure:
            print("sekhar")
        
        }
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    
    
    
    @IBAction func tappedOnConfirmBtn(_ sender: Any) {
        
        if CalenderPopUp.calenderInstance.clinicTF.text?.isEmpty == true ||  CalenderPopUp.calenderInstance.dateTF.text?.isEmpty == true{
            simpleAlert(message: "please check your details")
        }else{
               parentView.removeFromSuperview()
           
        }
     
        
    }
    
    @IBAction func tappedOnCloseBtn(_ sender: Any) {
         parentView.removeFromSuperview()
    }
    
    
    func createPicker(){
        
        datepicker.datePickerMode = .date
        datepicker.setDate(Date(), animated: true)
      //  timePicker.setDate(Date(), animated: true)
     //   timePicker.minuteInterval = 15
        
        let pickerView = picker
        pickerView.backgroundColor = .white
        pickerView.showsSelectionIndicator = true
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action:#selector(donePicker))
        doneButton.tintColor = UIColor.white
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action:#selector(canclePicker))
        cancelButton.tintColor = UIColor.white
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.barTintColor = UIColor.appGreenColor
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian);
        let components = calendar?.components([NSCalendar.Unit.year,NSCalendar.Unit.month,NSCalendar.Unit.day,NSCalendar.Unit.hour,NSCalendar.Unit.minute], from: currentDate )
        
        let maximumYear = (components?.year)! - 150
        let maximumMonth = (components?.month)! - 1
        let maximumDay = (components?.day)! - 1
        
        let comps = NSDateComponents();
        comps.year = +maximumYear
        comps.month = +maximumMonth
        comps.day = +maximumDay
        let maxDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        datepicker.maximumDate = maxDate
        datepicker.minimumDate = datepicker.date
        self.dateTF.inputView = datepicker
        self.dateTF.inputAccessoryView = toolBar
        
    }
    
    @objc func donePicker() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFormatter2 = DateFormatter()
        
        dateFormatter2.dateFormat = "EEEE"
        
        let dayOfTheWeekString = dateFormatter2.string(from: datepicker.date)
        print(dayOfTheWeekString.uppercased().prefix(3))
        let day = dayOfTheWeekString.uppercased()
           let dayName = day.prefix(3)
        print(dayName)

        
      //  for i in 0..<DRInfo.getClinicData.count{
//            if DRInfo.getClinicData[i].clinicData.contains (where: { $0.clinic_name == clinicTF.text }){
        
                print(dayName)
        print(slotInfo[0].day)
                if slotInfo[0].day == "\(dayName)"{
                    
                    self.clinicStart = slotInfo[0].start_time
                    
                    self.clinicEnd = slotInfo[0].end_time
                    let end = self.clinicEnd?.timeConversion12(time24: self.clinicEnd!)
                    var clinicEndTime:String?
                    
                    let start = self.clinicStart?.timeConversion12(time24: self.clinicStart!)
                    var clinicStartTime:String?
                    
                    
                    clinicStartTime = start?.stringTrims(trim: start!)
                    clinicEndTime = end?.stringTrims(trim: end!)
                    
                    self.clinicOpens = clinicStartTime! + "-" + clinicEndTime!
                    
                    if slotInfo[0].schedule == "MORN"{
                        clinicSlotIdM = slotInfo[0].id
                       self.morningBtn.setTitle(self.clinicOpens, for: .normal)
                        
                    } else if slotInfo[0].schedule == "NOON"{
                        clinicSlotIdM = slotInfo[0].id
                       self.noonBtn.setTitle(self.clinicOpens, for: .normal)
                    }else if slotInfo[0].schedule == "EVEN"{
                       // clinicSlotIdM = slotInfo[0].id
                        clinicSlotIdM = slotInfo[0].id
                      self.eveningBtn.setTitle(self.clinicOpens, for: .normal)
                    }
//                    }else {
//                        self.makeToast("Slot Not Available",duration:3.0,position:.center)
//                    }
                }
                
          //  }
            
                        else{
            
                            self.noonBtn.setTitle("-", for: .normal)
                            self.morningBtn.setTitle("-", for: .normal)
                            self.eveningBtn.setTitle("-", for: .normal)
                            //simpleAlert(message: "slot not avaliable")
                            self.popupViwe.makeToast("Slot Not Available",duration:3.0,position:.center)
        
            
            
                        }
            
        //}
     //   print(self.clinicDay)
     //   print(self.schedule)
        
        
        
        
        
      //  let dayName = day.prefix(3)
        self.dateTF.text = dateFormatter.string(from: datepicker.date)
        self.dateTF.textColor = UIColor.white
        self.dateTF.resignFirstResponder()
        
    }
    
    @objc func canclePicker() {
        
        self.dateTF.resignFirstResponder()
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateTF{
              createPicker()
        }
     
    }
}


