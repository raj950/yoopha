//
//  ScorlledPopUpView.swift
//  Yoopha
//
//  Created by Yoopha on 15/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit

class ScrolledPopUpView: UIView{
    
    static let popUpInstance = ScrolledPopUpView()
    
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var parentView: UIView!
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       Bundle.main.loadNibNamed("ScrolledPopUpView", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    enum PopUpType {
        case alert
    }
    
    func showAlert (alertType: PopUpType) {
      
        
        switch alertType {
    
        case .alert:
           print("")
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    }
    private func commonInit() {
     
        
        alertView.layer.cornerRadius = 10
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func onClickCloseBtn(_ sender: Any) {
        
        parentView.removeFromSuperview()
    }
}

