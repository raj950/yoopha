//
//  SOS_PopUp.swift
//  Yoopha
//
//  Created by Yoopha on 18/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation

class SOS_PopUp: UIView {
    
    static let instance = SOS_PopUp()
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var continueBtn: CustomButton!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("SOS_PopUp", owner: self, options: nil)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func commonInit() {
//        img.layer.cornerRadius = 30
//        img.layer.borderColor = UIColor.white.cgColor
//        img.layer.borderWidth = 2
        
        alertView.layer.cornerRadius = 10
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    
    enum AlertType {
        case success
        case failure
    }
    
    func showAlert(alertType: AlertType) {
       
        
        switch alertType {
        case .success:
            print("")
//            img.image = UIImage(named: "success")
//            doneBtn.backgroundColor = #colorLiteral(red: 0.07450980392, green: 0.6666666667, blue: 0.6392156863, alpha: 1)
        case .failure:
            
             print("")
//            img.image = UIImage(named: "failure")
//            doneBtn.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        }
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    
   
    @IBAction func tappedOnContinueBtn(_ sender: Any) {
         parentView.removeFromSuperview()
    }
}
