//
//  PopUp_Scrolled.swift
//  Yoopha
//
//  Created by Yoopha on 15/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit

class PopUp_Scrolled : UIView{
    
    @IBOutlet weak var popUpScrollView: UIScrollView!
    static let popUpInstance = PopUp_Scrolled()
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var alertView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("PopUp_Scrolled", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    enum AlertType {
        case success
        
    }
    
    func showAlert(alertType: AlertType) {
     
    
        switch alertType {
        case .success:
            print("raj")
        }
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    private func commonInit() {
       
        
        alertView.layer.cornerRadius = 10
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    @IBAction func tappedOnCloseBtn(_ sender: Any) {
         parentView.removeFromSuperview()
    }
    
}

extension PopUp_Scrolled:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = popUpScrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = UIColor.green
    }
}
