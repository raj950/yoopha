//
//  YourAppointmentVC.swift
//  Yoopha
//
//  Created by Yoopha on 27/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown
import SVProgressHUD
import SideMenuSwift
import Kingfisher

class YourAppointmentVC: UIViewController {
    
    @IBOutlet weak var appointmentsLbl: UILabel!
    @IBOutlet weak var appointmentsTV: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    
    let params = NSMutableDictionary()
    var getPatientDeatils = patientsList()
   // var patientsNames:[patientsList] = []
  //  var patients = patientsList()
    var count = String()
    var nextt = String()
    var previous = String()
    var appointmentDetails:[patientsList]=[]
    var doctorInfo:[DoctorDeatils]=[]
    var backButtonStatus = false
    var status:String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if backButtonStatus == false{
            backBtn.isHidden = true
        }
        
        DispatchQueue.main.async {
            self.appointmentsTV.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
          self.getDoctorAppointmentList()
            registerCell()
        
    }

    @IBAction func tappedOnNewAppointment(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.consumerSearchVC) as! ConsumerSearchVC
        self.navigationController?.pushViewController(navTo, animated: true)
        
    }
    
    
    
    @IBAction func navBack(_ sender: Any) {
   
            navigationController?.popViewController(animated: true)
    }
 
    
    //MARK: Doctor Appointments List
    func getDoctorAppointmentList(){
      
        
        let url1 = "\(Constants.API.base_URL)\(Constants.API.doctor_appointment_view_by_user)" + Constants.ConstantStrings.slash
        let finalUrl = url1 + "\(loginModel.userid)" + Constants.ConstantStrings.slash
        print(finalUrl)
        print(loginModel.userid)
        WebApiCallBack.shared.GETServiceHeader(url: finalUrl, token: loginModel.token) { (response) -> (Void) in
            print(response)
            if let myResponse = JSON(response).dictionary{
                if let result = myResponse["results"]?.array{
                
                    var getPatientDeatils = patientsList()
                    var doctorSpeciality = DoctorSpecialities()
                    var doctorDetails = DoctorDeatils()
                    
                    for results in result{
                        let symptoms = results["symptoms"].string
                        let appointmentStatus = results["appointment_status"].dictionary
                        self.status = appointmentStatus!["status"]?.string
                        let bookingDate = results["booking_date"].string
                        let appointmentId = results["appointmentID"].string
                        let slug = results["slug"].string
                        PatientAppointmentsModel.patientSlugId = slug

                        let appointmentToken = results["token_no"].int
                        let doctorData = results["doctor"].dictionary
                        doctorDetails.doctor_name = doctorData!["doctor_name"]?.string
                        doctorDetails.doctor_education = doctorData!["doctor_education"]?.string
                        let clinicAddress = doctorData!["address"]?.dictionary
                        let addLine = clinicAddress!["address_line3"]?.string
                        doctorDetails.id = doctorData!["id"]?.int
                        let doctorProfileImg = doctorData!["doctor_profile_image"]?.array
                        for getImage in doctorProfileImg!{
                            doctorDetails.doctorProfileImage = getImage["upload"].string ?? "user_profile"
                        }
                        
                        ConsumerModel.doctorId = doctorDetails.id!
//                        let clinicData = results["clinic_slot"].dictionary
//                        let clinicInfo = clinicData!["clinic"]?.dictionary
//                        let clinicName = clinicInfo!["clinic_name"]?.string
//                        let slot = clinicData!["slot"]?.dictionary
//                        let startTime = slot!["start_time"]?.string
//                        let endTime = slot!["end_time"]?.string
                        
                        
//                        var clinicAdd = clinic()
//                        clinicAdd.clinic_name = clinicName
                       
                 
                        
                     
                        let doctorSpecialityData = doctorData?["doctor_speciality"]?.dictionary
                        doctorSpeciality.speciality_name = doctorSpecialityData!["speciality_name"]?.string ?? ""
                        
                        getPatientDeatils.patientToken = appointmentToken
                        getPatientDeatils.doctorName = doctorDetails.doctor_name
                        getPatientDeatils.doctorEducation = doctorDetails.doctor_education
                        getPatientDeatils.doctorSpeciality = doctorSpeciality.speciality_name
                        getPatientDeatils.doctroProfile = doctorDetails.doctorProfileImage
                        getPatientDeatils.bookingDate = bookingDate
                        getPatientDeatils.appointmentId = appointmentId
                      

                        
//                        getPatientDeatils.startTime = startTime
//                        getPatientDeatils.endTime = endTime
//                        getPatientDeatils.clinicName = clinicAdd.clinic_name
                        getPatientDeatils.patientSlug = slug
                        getPatientDeatils.symptoms = symptoms
                        getPatientDeatils.aptStatus = self.status
                        getPatientDeatils.clinicAddress =  addLine
                        self.appointmentDetails.append(getPatientDeatils)
                        self.doctorInfo.append(doctorDetails)
                        print(getPatientDeatils.clinicAddress)
                        
                    }
                    DispatchQueue.main.async {
                        self.appointmentsTV.reloadData()
                    }
                    
                    
                }
            }
        }
    }
    
    func registerCell(){
        let appointmentCell = UINib(nibName: Constants.Cell.userAppointmentsCell, bundle: nil)
        appointmentsTV.register(appointmentCell, forCellReuseIdentifier: Constants.Cell.userAppointmentsCell)
    }
}

extension YourAppointmentVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
       
        return appointmentDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell
            .userAppointmentsCell) as! UserAppointmentsCell
            cell.selectionStyle = .none
        
           // let start = appointmentDetails[indexPath.row].startTime?.timeConversion12(time24:(appointmentDetails[indexPath.row].startTime)!)
          //  let end  = appointmentDetails[indexPath.row].endTime?.timeConversion12(time24:(appointmentDetails[indexPath.row].endTime)!)
            let date = appointmentDetails[indexPath.row].bookingDate
            
            
            let startIndex = date!.index(date!.startIndex, offsetBy: 5)
            let endIndex = date!.index(date!.startIndex, offsetBy: 6)
            let dateStart =  date!.index(date!.startIndex, offsetBy: 8)
            let dateEnd =  date!.index(date!.startIndex, offsetBy: 9)
            let yearStart = date!.index(date!.startIndex, offsetBy: 2)
            let yearEnd = date!.index(date!.startIndex, offsetBy: 3)
            let year = String(date![yearStart...yearEnd])
            let dateTrim = String(date![dateStart...dateEnd])
            let monthtrim = String(date![startIndex...endIndex])
            let monthNameGet = DateFormatter().shortMonthSymbols[Int(monthtrim)! - 1]
            
            let bookingDate = dateTrim + " " + monthNameGet + " " + year
            print(bookingDate)
            let token = "\(appointmentDetails[indexPath.row].patientToken!)"
          //  let trim1 = end?.stringTrims(trim: end!)
           // let clinicTimes = trim! + " - " + trim1!
        
            let url = URL(string:doctorInfo[indexPath.row].doctorProfileImage! )
            cell.doctorProfile.kf.setImage(with: url)
            cell.drNameLbl.text = appointmentDetails[indexPath.row].doctorName
            cell.specialityLbl.text = appointmentDetails[indexPath.row].doctorSpeciality
            cell.drEducationLbl.text = appointmentDetails[indexPath.row].doctorEducation
          //  cell.clinicNameLbl.text = appointmentDetails[indexPath.row].clinicName! + "," + appointmentDetails[indexPath.row].clinicAddress!
            //   cell.startTimeLbl.text = clinicTimes
            cell.symptomLbl.text = appointmentDetails[indexPath.row].symptoms
            cell.userAppoinmentBackgroundView.backgroundColor = .aptDefaultBackgroundColor
            cell.tokenLbl.text = token
            cell.appointmentStatusLbl.textColor = .appGreenColor
            cell.tokenLbl.textColor = .appGreenColor
            cell.dateLbl.text = bookingDate
            cell.appointmentStatusLbl.text = appointmentDetails[indexPath.row].aptStatus
        
            if appointmentDetails[indexPath.row].aptStatus == "CANCELLED"{
                cell.tokenLbl.textColor = .aptCancelStatusColor
                cell.tokenLbl.attributedText = token.strikeThrough()
                cell.userAppoinmentBackgroundView.backgroundColor = .aptCancelStatusBackgroundColor
                cell.dateLbl.textColor = .gray
                cell.startTimeLbl.textColor = .darkGray
                cell.dateLbl.attributedText = bookingDate.strikeThrough()
                cell.appointmentStatusLbl.text = appointmentDetails[indexPath.row].aptStatus
                cell.appointmentStatusLbl.textColor = .aptCancelStatusColor
            //    cell.startTimeLbl.attributedText = clinicTimes.strikeThrough()
           
            }
            else{
                
            }
            return cell
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.userAppointmentsCell) as! UserAppointmentsCell
        
            if appointmentDetails[indexPath.row].aptStatus == "CLOSED"{
                let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.closedAppointmentDetailsVC) as! ClosedAppointmentDetailsVC
                navTo.savedetails = appointmentDetails
                navTo.selectedPatientDetails = indexPath.row
                self.navigationController?.pushViewController(navTo, animated: true)
            }
            if appointmentDetails[indexPath.row].aptStatus == "CANCELLED"{
                print("self")
            }
            if appointmentDetails[indexPath.row].aptStatus == "BOOKED"{
                let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.appointmentDetailsVC) as! AppointmentDetailsVC
                navTo.savedetails = appointmentDetails
                navTo.selectedPatientDetails = indexPath.row
                self.navigationController?.pushViewController(navTo, animated: true)
            }
            
        
    }
    
}


struct PatientAppointmentsModel{
    
    static var patientSlugId:String?
}


//func getPatientsList(){
//
//    let url1 = "\(Constants.API.base_URL)\(Constants.API.patientList)" + String(loginModel.userid) + Constants.ConstantStrings.slash
//    WebApiCallBack.shared.GETServiceHeader(url: url1,token: loginModel.token) { (response) -> (Void) in
//
//        if response != nil{
//            if let myResponse = JSON(response).dictionary{
//
//
//                if let result = myResponse["results"]?.array{
//                    var getPatientDeatils = patientsList()
//
//                    for results in result{
//                        getPatientDeatils.name.append(results["name"].string!)
//                        getPatientDeatils.id.append(results["id"].int!)
//                        getPatientDeatils.userId.append(results["user"].int!)
//                        self.patientsNames.append(getPatientDeatils)
//                    }
//                }
//                // print(self.patients.name)
//            }
//
//        }
//
//    }
//
//
//}
