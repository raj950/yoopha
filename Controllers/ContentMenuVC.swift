//
//  ContentMenuVC.swift
//  Yoopha
//
//  Created by Yoopha on 16/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class ContentMenuVC: UIViewController {

    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userProfileImage: CustomImageView!
    @IBOutlet weak var menuTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mobileLbl.text = "+91 " + loginModel.mobileNumber!
        nameLbl.text = "Mr. " + ConsumerModel.consumerName
   
        
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func tappedOnLogout(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
       present(navTo, animated: false, completion: nil)
    }
}
extension ContentMenuVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.contentMenuTVC) as! ContentMenuTVC
        cell.sideMenuContentLbl.text = Constants.ConstantStrings.menuContent[indexPath.row]
        
        if indexPath.row >= 5 {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.edit_ProfileVC) as! Edit_ProfileVC
            present(navTo, animated: false, completion: nil)
            // self.navigationController?.pushViewController(navTo, animated: true)
        }
        if indexPath.row == 1{
            let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.settingsVC) as! SettingsVC
            present(navTo, animated: false, completion: nil)
           // self.navigationController?.pushViewController(navTo, animated: true)
        }
        
    }
    
}
