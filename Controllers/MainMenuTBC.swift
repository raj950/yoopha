//
//  MainMenuTBC.swift
//  Yoopha
//
//  Created by Yoopha on 21/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class MainMenuTBC: UITabBarController,UITabBarControllerDelegate,UINavigationControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
       self.delegate = self
       
       
    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        getConsumer()
        get_SOS_By_UserId()
    }

    func getConsumer(){
        let url2 = "\(Constants.API.base_URL)\(Constants.API.consumer_consumer)"
        let finalUrl = url2 + "\(loginModel.userid)/"
        print(finalUrl)
        
        WebApiCallBack.shared.GETServiceHeader(url: finalUrl,token: loginModel.token) { (response) -> (Void) in
            print(response)
            
            if response != nil{
                if let response = JSON(response).dictionary{
                    if  let results = response["results"]?.array{
                        for result in results{
                            var consumer = ConsumerModel()
                            consumer.id = result["id"].int
                            consumer.consumerID = result["consumerID"].string
                            ConsumerModel.consumerId = consumer.id!
                            print(ConsumerModel.consumerId)
                            
                            print(consumer.id as Any)
                            
                            //  var consumerAddressData = ConsumerAddressModel()
                            let consumerAddress = result["address"].dictionary
                            let consumerID = consumerAddress!["id"]?.int
                            
                            ConsumerAddressModel.id = consumerID!
                            print(ConsumerAddressModel.id as Any)
                        }
                        
                        
                        
                        
                    }
                }
                
            }
        }
        // self.doctorTags()
    }
    
    
}

func get_SOS_By_UserId(){
    
    let url1 = Constants.API.base_URL + "y16_sos/"
    let url2 = loginModel.userid
    let url3 = "/sos_details/"
    let finalUrl = url1 + "\(url2)" + url3
    print(finalUrl)
    let url = URL(string: finalUrl)
    if let usableUrl = url {
        let request = URLRequest(url: usableUrl)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let data = data {
                if let response = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
                    print(response)
                    if let response = JSON(response).dictionary{
                        print(response)
                        if let results = response["results"]?.array{
                            
                            
                            var sosModel = SOSModel()
                            for result in results{
                                sosModel.id = result["id"].int
                                SOSModel.user_SOS_Id = sosModel.id
                                print(SOSModel.user_SOS_Id)
                            }
                        }
                    }
                }
                
            }
        })
        task.resume()
    }
}
