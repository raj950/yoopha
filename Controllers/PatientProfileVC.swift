//
//  PatientProfileVC.swift
//  Yoopha
//
//  Created by Yoopha on 05/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON


protocol AccessoryToolbarDelegate: class {
    func doneClicked(for textField: UITextField)
    func cancelClicked(for textField: UITextField)
}


class AccessoryToolbar: UIToolbar {
    fileprivate let textField: UITextField
    weak var accessoryDelegate: AccessoryToolbarDelegate?
    
    init(for textField: UITextField) {
        self.textField = textField
        super.init(frame: CGRect.zero)
        
        self.barStyle = .default
        self.barTintColor = UIColor.appGreenColor
        self.isTranslucent = true
        self.tintColor = UIColor.white
        self.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClicked))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClicked))
        self.setItems([cancelButton, spaceButton, doneButton], animated: false)
        self.isUserInteractionEnabled = true
        textField.inputAccessoryView = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc fileprivate func doneClicked() {
        accessoryDelegate?.doneClicked(for: self.textField)
    }
    
    @objc fileprivate func cancelClicked() {
        accessoryDelegate?.cancelClicked(for: self.textField)
    }
}


class PatientProfileVC: UIViewController {

    
    weak var accessoryDelegate: AccessoryToolbarDelegate?
    var datePicker : UIDatePicker!
    var dateFinalValue : String = ""
    
    @IBOutlet weak var mobileTF: CustomTF!
    @IBOutlet weak var nameTF: CustomTF!
    @IBOutlet weak var ageTF: DropDownTextField!
    var TFImagesArray = [#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon")]
    var TFNames:[DropDownTextField] = []
    
   static var dateTextField = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastTreatedTF.delegate = self
        sinceTF.delegate = self
        ageTF.delegate = self
        genderTF.datasource = ["Male","Female","Sister","Brother"]
        bloodGroupTF.datasource = ["B +ve","B -ve","O -ve"]
        relationTF.datasource =  ["Father","Mother","Brother","Sister"]
        relationTF2.datasource =  ["Father","Mother","Brother","Sister"]
        illnessTF.datasource = ["Diabetes","Diabetes","Diabetes","Diabetes"]
        illnessTF2.datasource = ["Diabetes","Diabetes","Diabetes","Diabetes"]
       // lastTreatedTF.datasource = ["1","2","3","4","5","B +ve","B -ve","O -ve","1","2","3","4","5","B +ve","B -ve","O -ve"]
        medicationTF1.datasource = ["1","2","3","4","5"]
        sinceTF.datasource = ["11111","2233","1223"]
        medicationTF2.datasource = ["1","2","3","4","5","B +ve","B -ve","O -ve","1","2","3","4","5","B +ve","B -ve","O -ve"]
      
        
        TFNames = [genderTF,bloodGroupTF,relationTF,illnessTF,lastTreatedTF,medicationTF1,illnessTF2,relationTF2,sinceTF,medicationTF2] as! [DropDownTextField]
       assignImagesToTF()
    }
    
    @IBOutlet weak var genderTF: DropDownTextField!{
        didSet{ genderTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(genderDropDown)))
        }
    }
    
    @IBOutlet weak var bloodGroupTF: DropDownTextField!{
        didSet{ bloodGroupTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bloodGroupDropDown)))
        }
    }
    @IBOutlet weak var relationTF: DropDownTextField!{
        didSet{
            relationTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(relationDropDown)))
        }
    }
    @IBOutlet weak var illnessTF: DropDownTextField!{
        didSet{
            illnessTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(illnessTFDropDown)))
        }
    }
    @IBOutlet weak var lastTreatedTF: UITextField!
//        {
//        didSet{
//            lastTreatedTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(lastTreatedTFDropDown)))
//        }
//    }
    @IBOutlet weak var medicationTF1: DropDownTextField!{
        didSet{
            medicationTF1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(medicationTF1DropDown)))
        }
    }
    @IBOutlet weak var illnessTF2: DropDownTextField!{
        didSet{
            illnessTF2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(illnessTF2DropDown)))
        }
    }
    @IBOutlet weak var relationTF2: DropDownTextField!{
        didSet{
            relationTF2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(relationTF2DropDown)))
        }
    }
    @IBOutlet weak var sinceTF: DropDownTextField!{
        didSet{
            sinceTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sinceTFDropDown)))
        }
    }
    @IBOutlet weak var medicationTF2: DropDownTextField!{
        didSet{
            medicationTF2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(medicationTF2DropDown)))
        }
    }
    
  
    @objc func genderDropDown(){
        genderTF.select(genderTF)
        genderTF.dropDown.selectionAction = { (index,item) in
            self.genderTF.text = item
            self.genderTF.resignFirstResponder()
        }
    }
    @objc func bloodGroupDropDown(){
        bloodGroupTF.select(bloodGroupTF)
        bloodGroupTF.dropDown.selectionAction = { (index,item) in
            self.bloodGroupTF.text = item
            self.bloodGroupTF.resignFirstResponder()
        }
    }
    @objc func relationDropDown(){
        relationTF.select(relationTF)
        relationTF.dropDown.selectionAction = {(index,item) in
            self.relationTF.text = item
            self.relationTF.resignFirstResponder()
        }
    }
    @objc func relationTF2DropDown(){
        relationTF2.select(relationTF2)
        relationTF2.dropDown.selectionAction = {(index,item) in
            self.relationTF2.text = item
            self.relationTF2.resignFirstResponder()
        }
    }
    @objc func illnessTFDropDown(){
        illnessTF.select(illnessTF)
        illnessTF.dropDown.selectionAction = {(index,item) in
            self.illnessTF.text = item
            self.illnessTF.resignFirstResponder()
        }
    }
    @objc func illnessTF2DropDown(){
        illnessTF2.select(illnessTF)
        illnessTF2.dropDown.selectionAction = {(index,item) in
            self.illnessTF2.text = item
            self.illnessTF2.resignFirstResponder()
        }
    }
//    @objc func lastTreatedTFDropDown(){
//        lastTreatedTF.select(lastTreatedTF)
//        lastTreatedTF.dropDown.selectionAction = {(index,item) in
//            self.lastTreatedTF.text = item
//            self.lastTreatedTF.resignFirstResponder()
//        }
//
//    }
    @objc func medicationTF1DropDown(){
        medicationTF1.select(medicationTF1)
        medicationTF1.dropDown.selectionAction = {(index,item) in
            self.medicationTF1.text = item
            self.medicationTF1.resignFirstResponder()
        }
    }
    @objc func sinceTFDropDown(){
        sinceTF.select(sinceTF)
        sinceTF.dropDown.selectionAction = {(index,item) in
            self.sinceTF.text = item
            self.sinceTF.resignFirstResponder()
        }
    }
    @objc func medicationTF2DropDown(){
        medicationTF2.select(medicationTF2)
        medicationTF2.dropDown.selectionAction = {(index,item) in
            self.medicationTF2.text = item
            self.medicationTF2.resignFirstResponder()
        }
    }
    func assignImagesToTF(){
        for j in 0..<TFNames.count{
            print(TFNames[j])
            let imageView = UIImageView(frame: CGRect(x: -5, y: 0, width: 20, height: 20))
            imageView.image = TFImagesArray[j]
            
            //imageView.tintColor = tintColor
            let view = UIView(frame : CGRect(x: -5, y: 5, width: 20, height: 20))
            view.addSubview(imageView)
            TFNames[j].rightViewMode = .always
            
            TFNames[j].rightView = view
            
        }
        
    }
    
    @objc func setEmptyTF(){
        
        let navTo = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.mainMenuTBC) as! MainMenuTBC
        present(navTo, animated: true, completion: nil)
    }
 
    @IBAction func tappedOnSubmit(_ sender: Any) {
        if nameTF.text?.isEmpty == true || mobileTF.text?.isEmpty == true || genderTF.text?.isEmpty == true || ageTF.text?.isEmpty == true || bloodGroupTF.text?.isEmpty == true || relationTF.text?.isEmpty == true || illnessTF.text?.isEmpty == true || lastTreatedTF.text?.isEmpty == true || medicationTF1.text?.isEmpty == true || illnessTF2.text?.isEmpty == true || relationTF2.text?.isEmpty == true || sinceTF.text?.isEmpty == true || medicationTF2.text?.isEmpty == true {
            
            simpleAlert(message: "Please Check Your Details")
        }else{
            if genderTF.text == "Male"{
                genderTF.text = "M"
            }else if genderTF.text == "Female"{
                genderTF.text = "F"
            }
            switch relationTF.text {
            case "Father":
                relationTF.text = "1"
            case "Mother":
                relationTF.text = "2"
            case "Brother":
                relationTF.text = "3"
            case "Sister":
                relationTF.text = "4"
            default:
                print("Choosen Option in not valid")
            }
            switch relationTF2.text {
            case "Father":
                relationTF2.text = "1"
            case "Mother":
                relationTF2.text = "2"
            case "Brother":
                relationTF2.text = "3"
            case "Sister":
                relationTF2.text = "4"
            default:
                print("Choosen Option is not valid")
            }
            createPatientProfile()
        }
       
    }
  
    func createPatientProfile(){
        
    //    let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y05_consumer/create_patientdetails/"
        
        
     let params:[String:Any] = [
        "name": nameTF.text as Any,
        "mobile_no":mobileTF.text as Any,
        "gender": genderTF.text as Any,
        "dob": ageTF.text as Any,
        "blood_group": bloodGroupTF.text as Any,
        "relationship": relationTF.text as Any,
        "medical_history": [
//            [
//            "illness_type": illnessTF.text as Any,
//            "last_treated": lastTreatedTF.text as Any,
//            "medication": false
//            ]
        ],
        "family_history": [
          
//            [ "illness_type": illnessTF.text as Any,
//                "relationship": relationTF2.text as Any,
//            "since": sinceTF.text as Any,
//            "medication": false
//            ]
        ],
        "user": loginModel.userid as Any
        ]
        
        print(params)
    
        
        WebApiCallBack.shared.POSTServiceHeader(serviceType: "\(Constants.API.base_URL)\(Constants.API.createPatientDetails)", parameters:  params , token:loginModel.token) { (response) -> (Void) in
            
            //print(response)
            
            let response = JSON(response).dictionary
            if  let error = response!["non_field_errors"]?.array{
                let errorvalue = error[0].string
                if errorvalue == "Unable to log in with provided credentials."{
                    AlertView.instance.showAlert(title: "YOOPHA", message: "Please Check Your details", alertType: .failure)
                    AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                }
            }else{
                guard let SUCCESS = response else {return}
                 let patient = SUCCESS["patientID"]?.string
            //    print(patient)
              //    loginModel.patientId = patient!
              //  print(loginModel.patientId)

                AlertView.instance.showAlert(title: "YOOPHA", message: "Patient Created Successfully", alertType: .success)
                AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                AlertView.instance.tokenLbl.isHidden = true
              
                AlertView.instance.doneBtn.addTarget(self, action: #selector(self.setEmptyTF), for: .touchUpInside)
            }
        }
        
        
        
    }

}


struct patientDetailsModel {
    let name:String
    let gender:String
    let age:Int
    let blood_group:String
    let relationship:Int
    let medical_history:[MedicalHistory] = []
    let family_history:[FamilyHistory] = []
    let user:Int
}

struct MedicalHistory {
    var illness_type:String
    var last_treated:Int
    var medication:Bool
}
struct FamilyHistory {
    var illness_type:String
    var relationship:Int
    var since:Int
    var medication:Bool
    
}


extension PatientProfileVC:UITextFieldDelegate,AccessoryToolbarDelegate{
    func doneClicked(for textField: UITextField) {
        let dateFormatter = DateFormatter()
//        dateFormatter.dateStyle = .short
//        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        textField.text = dateFormatter.string(from: datePicker.date)
        textField.resignFirstResponder()
    }
    
    func cancelClicked(for textField: UITextField) {
        textField.resignFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.lastTreatedTF
        {
            setUpTextFieldPicker(textField: self.lastTreatedTF)
        }
        if textField == self.sinceTF
        {
            setUpTextFieldPicker(textField: self.sinceTF)
        }
        if textField == self.ageTF
        {
            setUpTextFieldPicker(textField: self.ageTF)
        }
       
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func setUpTextFieldPicker(textField: UITextField) {
        
        // DatePicker
       
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        
    
        textField.inputView = datePicker
        
        // ToolBar
        let toolbar = AccessoryToolbar(for: textField)
        
        // let's not forget to set the delegate
        toolbar.accessoryDelegate = self
    }
}

