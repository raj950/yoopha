//
//  TB.swift
//  Yoopha
//
//  Created by Yoopha on 18/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class MainViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

         self.delegate = self
    }
    

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if viewController is MenuViewController {
            print("First tab")
            
            sideMenuController?.revealMenu()
        }
    }

}
