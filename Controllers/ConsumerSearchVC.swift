//
//  ConsumerSearchVC.swift
//  Yoopha
//
//  Created by Yoopha on 19/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON
import Kingfisher

class ConsumerSearchVC: UIViewController {
    @IBOutlet weak var navBackButton: UIButton!
    @IBOutlet weak var consumerSearchCV: UICollectionView!
    var count = String()
    var nextt = String()
    var previous = String()
   // var resultArr = []()
    var searchResultArr = [DoctorClinicResult]()
    var searchStatus = true
    var doctor_speciality = NSMutableDictionary()
    var address = NSMutableDictionary()
    var backButtonStatus = true
    
    var clinicAddressarr:[ClinicAddress] = []
    var doctorSpecialities:[DoctorSpecialities] = []
    var doctorAddreee:[DoctorAddress] = []
    var doctorDetails:[DoctorDeatils] = []
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        print(backButtonStatus)
        if backButtonStatus == false{
            
            navBackButton.isHidden = false
            
            //getData()
        }
        self.consumerSearchCV.delegate = self
        self.consumerSearchCV.dataSource = self
      
       // self.searchBar.setCustomBackgroundColor (color: UIColor.white)
        self.searchBar.layer.cornerRadius = self.searchBar.bounds.height/2.0
        self.searchBar.clipsToBounds = true
    
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
     
    }
    
    
    
//    func getData(){
//
//        let myParam = NSMutableDictionary()
//        print(myParam)
//
//        let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y03_doctor/search_doctor/"
//        WebApiCallBack.requestApi(webUrl:url, paramData: myParam as NSObject,methiod: "Get", completionHandler: { (responseObject, error) -> () in
//            print("responseObject = \(responseObject); error = \(error)")
//            if responseObject != nil {
//                let mresponse = JSON(responseObject!)
//                if let serverMessage = mresponse.dictionary{
//                    self.count = serverMessage["count"]!.stringValue
//                    self.nextt = serverMessage["next"]!.stringValue
//                    self.previous = serverMessage["previous"]!.stringValue
//                    if let results = serverMessage["results"]?.array{
//                        print(results)
//                        for result in results{
//                            let object = resultsModel()
//
//                            object.id = result["id"].stringValue
//                            object.doctorID = result["doctorID"].stringValue
//                            object.doctor_name = result["doctor_name"].stringValue
//                            object.doctor_education = result["doctor_education"].stringValue
//                            object.practice_started_year = result["practice_started_year"].stringValue
//                            object.gender = result["gender"].stringValue
//                            object.doctor_license = result["doctor_license"].stringValue
//                            object.doctor_desc = result["doctor_desc"].stringValue
//                            object.membership_details = result["membership_details"].stringValue
//                            object.registration_no = result["registration_no"].stringValue
//                            object.registration_council = result["id"].stringValue
//                            object.languages_known = result["registration_council"].stringValue
//                            object.consultation_fee = result["consultation_fee"].stringValue
//                            object.payment_type = result["payment_type"].stringValue
//                            object.user = result["id"].stringValue
//                            if let doctor_profile_image = result["doctor_profile_image"].array{
//                                for docPro in doctor_profile_image{
//                                   // let obj = doctorsProfileImageModel()
//                                  //  obj.id = docPro["id"].stringValue
//                                  //  obj.image_name = docPro["image_name"].stringValue
//                                    object.drImage = docPro["upload"].string!
//                                   // object.doctor_profile_imageArr.append(obj)
//                                }
//                                print(doctor_profile_image)
//                            }
//                            if let doctor_speciality = result["doctor_speciality"].dictionary{
//
//                                let speciality = doctor_speciality["speciality_name"]?.string
//                                print(speciality)
//                                object.drName = speciality!
//
//                                //print(object.doctorID)
//
//                            }
//                            if let address = result["address"].dictionary{
//                                for addKey in address.keys{
//                                    self.address.setObject(address[addKey]?.stringValue as Any , forKey: addKey.lowercased() as NSCopying)
//                                 //   object.address1 = self.address
//
//                                }
//                            }
//                            self.resultArr.append(object)
//                        }
//                    }
//                    self.consumerSearchCV.reloadData()
//                }
//            }
//        })
//
//
//    }
  

    @IBAction func tappedOnBackButton(_ sender: Any) {
      //  let navToMainMenu = storyboard?.instantiateViewController(withIdentifier: "MainMenuTBC") as! MainMenuTBC
        self.navigationController?.popViewController(animated: true)
        //self.present(navToMainMenu, animated: true, completion: nil)
    }
   
    
    private func setupCollectionView() {
        consumerSearchCV.delegate = self
        consumerSearchCV.dataSource = self
        let nib = UINib(nibName: "DoctorsCVC", bundle: nil)
        consumerSearchCV.register(nib, forCellWithReuseIdentifier: "DoctorsCVC")
    }

}

extension ConsumerSearchVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
      //  return final.count
//        if searchStatus{
//
//            print(self.final.count)
//            return self.final.count
//
//        }else{
//
//            return final.count
//        }
        return 9
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorsCVC", for: indexPath) as! DoctorsCVC
     //   cell.doctorsImageView.image = UIImage(named: "tags")
        
        if searchStatus == true{
       // let data = self.resultArr[indexPath.row]
       // cell.doctorsLabel?.text = final[indexPath.row].doctor[0].doctor_name
        
          // let url = URL(string: final[indexPath.row].doctor[0].doctor_profile_image ?? "user")
    
        //cell.myLabel.text = self.items[indexPath.item]
//            cell.doctorsLabel?.text = arrElements[indexPath.row]
//            cell.backgroundColor = .cyan
            
        
        }else{
            
           // cell.doctorsLabel?.text = arrElements[indexPath.row]
          //  let data = self.searchResultArr[indexPath.row]
           //// cell.doctorsLabel?.text = final[indexPath.row].doctor[0].doctor_name
            
           // let url = URL(string: data.drImage)
            
           // cell.doctorsImageView.kf.setImage(with: url)
            //cell.myLabel.text = self.items[indexPath.item]
          //  cell.backgroundColor = UIColor.cyan
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        print("You selected cell #\(indexPath.item)!")
        let navToSearchDoctorVC = storyboard?.instantiateViewController(withIdentifier: "SearchDoctorVC") as! SearchDoctorVC
        
       // print(final[indexPath.row].doctor[0].doctor_name)
        navigationController?.pushViewController(navToSearchDoctorVC, animated: true)
       // present(navToSearchDoctorVC, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
      
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        //  return CGSize(width: (self.imagesCollectionView.bounds.size.width/4), height: (self.imagesCollectionView.frame.size.height-10))
//        return CGSize(width: (self.consumerSearchCV.frame.size.width/2)-10, height: 340)
//    }
    
    
}





extension ConsumerSearchVC: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

     print(doctorSpecialities)
        if(doctorSpecialities.count == 0){
            searchStatus = true;
        } else {
            searchStatus = false;
        }
      
        self.consumerSearchCV.reloadData()
    }
        
}

