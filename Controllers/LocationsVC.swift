//
//  LocationsVC.swift
//  Yoopha
//
//  Created by Yoopha on 10/09/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class LocationsVC: UIViewController {

    @IBOutlet weak var locationsTF: CustomTF!
    @IBOutlet weak var locationsTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    @IBAction func tappedOnSetLocationBtn(_ sender: Any) {
        
        
    }
    
    @IBAction func tappedOnCloseBtn(_ sender: Any) {
        
        
      //  let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.appointmentsVC) as! AppointmentsVC
          //  navTo.locationsBtn.setTitle(, for: <#T##UIControl.State#>)
       // present(navTo, animated: false, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
}

extension LocationsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.ConstantStrings.popularLocations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:Constants.Cell.locationsTVC) as! LocationsTVC
        cell.locationsLbl.text = Constants.ConstantStrings.popularLocations[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        locationsTF.text! = Constants.ConstantStrings.popularLocations[indexPath.row]
        
        print(locationsTF.text!)
        
    //    AppointmentsVC.appointmentVC.locationsBtn.setTitle(locationsTF.text!, for: .normal)
       
        
        navigationController?.popViewController(animated: true)
    }
    
}
