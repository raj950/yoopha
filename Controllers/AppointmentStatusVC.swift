//
//  AppointmentStatusVC.swift
//  Yoopha
//
//  Created by Yoopha on 12/09/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class AppointmentStatusVC: UIViewController {
    var appointmentStatus = true
    @IBOutlet weak var tokenLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusImage: CustomImageView!
    @IBOutlet weak var appointmentTokenLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        if appointmentStatus == true{
            statusImage.image = UIImage(named: "success_Image")
        }else{
            statusImage.image = UIImage(named: "success-red")
            statusLbl.text = "Your booking has been cancelled!"
            tokenLbl.isHidden = true
            appointmentTokenLbl.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tappedOnCloseBtn(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.sideMenuController) as! SideMenuController
        self.navigationController?.pushViewController(navTo, animated: true)
    }
    
    @IBAction func tappedOnContinueBtn(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.sideMenuController) as! SideMenuController
        self.navigationController?.pushViewController(navTo, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
