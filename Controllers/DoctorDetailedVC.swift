//
//  DoctorDetailedVC.swift
//  Yoopha
//
//  Created by Yoopha on 20/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Kingfisher

protocol passTagIndex{
    func passIndexValue(name:String?,img:UIImage?)
}

class DoctorDetailedVC: UIViewController {
    @IBOutlet weak var viewTitleLbl: UILabel!
    
    @IBOutlet weak var tagImg: UIButton!
    @IBOutlet weak var doctorProfileImage: CustomImageView!
    @IBOutlet weak var drExpLbl: UILabel!
    @IBOutlet weak var drDescLbl: UILabel!
    @IBOutlet weak var drFeeLbl: UILabel!
    @IBOutlet weak var drEducationsLbl: UILabel!
    @IBOutlet weak var drSpecialityLbl: UILabel!
    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var doctorsListTV: UITableView!
    @IBOutlet weak var doctorsClinicsTV: UITableView!
    
    let date = Date()
    var doctorID:String?
    var doctorSlug:String?
    var doctorName:String?
    var doctorSpeciality:String?
    var doctorExp :String?
    var doctorFee:String?
    var doctorDiscription :String?
    var goPath:passTagIndex?
    var tagIndex:Int?
    var drName:String?

  
    override func viewDidLoad() {
        super.viewDidLoad()
        

      //  print(tagIndex)
        if AppointmentStatusModel.tagStatus == false{
            tagImg.setImage(UIImage(named: "tag-on"), for: .normal)
            //tagImg.buttonTag(btnImg: tagImg, consumerId: nil, DRId: nil)
        }else{
            tagImg.setImage(UIImage(named: "tag-off"), for: .normal)
            //tagImg.buttonTag(btnImg: tagImg, consumerId: nil, DRId: nil)
            
        }
    //   print(DRInfo.doctorProfileImage)
       
        doctorProfileImage.kf.setImage(with: URL(string: DRInfo.doctorProfileImage ?? Constants.ConstantStrings.defaultImage))
        viewTitleLbl.text = "Dr. " + DRInfo.doctorName!
        drNameLbl.text = DRInfo.doctorName
        drSpecialityLbl.text = DRInfo.doctorSpeciality! + " | " + DRInfo.doctorEducation!
     //   drFeeLbl.text = Constants.ConstantStrings.rupee + " " + DRInfo.doctorFee!
        drDescLbl.text = DRInfo.doctorDiscription
       // drEducationsLbl.text =
        

        cellRegistration()
        
        let doctorExperience = self.date.GetYear() - DRInfo.doctorExp!
        drExpLbl.text = "\(doctorExperience)" + " " + "+ years experience"
    
    }

    
    func cellRegistration(){

        let clinicsCell = UINib(nibName: Constants.Cell.clinicTVC, bundle: nil)
        self.doctorsClinicsTV.register(clinicsCell, forCellReuseIdentifier: Constants.Cell.clinicTVC)
        
       
     

    }

    @IBAction func tagAction(_ sender: Any) {
        if tagImg.currentImage == UIImage(named: "tag-on"){
            AppointmentStatusModel.tagStatus = false
            
            tagImg.setImage(UIImage(named: "tag-off"), for: .normal)
            
        }else{
            AppointmentStatusModel.tagStatus = true
            
            tagImg.setImage(UIImage(named: "tag-on"), for: .normal)
            
        }
    }
    @IBAction func tappedOnBookAppointmentBtn(_ sender: Any) {
        let navToBook = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.bookAppointmentVC) as! BookAppointmentVC
      //  navToBook.doctorName = DRInfo.doctorName
        //navToBook.clinicSlot = DRInfo.getClinicData[0].id
        self.navigationController?.pushViewController(navToBook, animated: true)
    }
    
    @IBAction func navBack(_ sender: Any) {
        let navBack = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.searchDoctorVC) as! SearchDoctorVC
       print(navBack.final.count)
        self.goPath?.passIndexValue(name: drNameLbl.text, img: tagImg.currentImage)
        self.navigationController?.popViewController(animated: true)
    }
}

extension DoctorDetailedVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DRInfo.getClinicData.count
       // return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let start = DRInfo.getClinicData[indexPath.row].slot[0].start_time!.timeConversion12(time24:DRInfo.getClinicData[indexPath.row].slot[0].start_time!)
            
            let end = DRInfo.getClinicData[indexPath.row].slot[0].end_time!.timeConversion12(time24: DRInfo.getClinicData[indexPath.row].slot[0].end_time!)
            let startTrimClinic = start.stringTrims(trim: start)
            let endTrimClinic = end.stringTrims(trim: end)
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.clinicTVC) as! ClinicTVC
            
            let clinicLocation = "\(DRInfo.getClinicData[indexPath.row].clinicData[0].clinic_address[0].address_line2!)" + "\(Constants.ConstantStrings.comma)" + "\(DRInfo.getClinicData[indexPath.row].clinicData[0].clinic_address[0].state!)"
            let clinicDays = "\(DRInfo.getClinicData[indexPath.row].slot[0].day!)" + "-" + "\(DRInfo.getClinicData[indexPath.row].slot[0].schedule!)"
            let clinicTimes =    " " + startTrimClinic+" to "+endTrimClinic
            cell.locationLbl.text = clinicLocation
            cell.clinicDayLbl.text = clinicDays
            cell.timingLbl.text = clinicTimes
            cell.clinicName.text = DRInfo.getClinicData[indexPath.row].clinicData[0].clinic_name! + " " + DRInfo.getClinicData[indexPath.row].clinicData[0].clinic_address[0].address_line3!
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
         
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 233
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navToBook = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.bookAppointmentVC) as! BookAppointmentVC
        navToBook.doctorName = DRInfo.doctorName
        navToBook.clinicSlot = DRInfo.getClinicData[0].id
       self.navigationController?.pushViewController(navToBook, animated: true)
    }
//    @objc func navToAppoint(_ sender:UIButton){
//        print(sender.tag)
//        let navToAppoint = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.bookAppointmentVC) as! BookAppointmentVC
//         navToAppoint.doctorName = DRInfo.doctorName
//         navToAppoint.doctorSpeciality = DRInfo.doctorSpeciality
//         navToAppoint.doctorFee = DRInfo.doctorFee
//
//       self.navigationController?.pushViewController(navToAppoint, animated: true)
//    }
}

extension DoctorDetailedVC:tableviewCell{
    func onClickCell(index: Int) {
        let navToDoctorDetailsVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.doctorDetailedVC) as! DoctorDetailedVC
        self.navigationController?.pushViewController(navToDoctorDetailsVC, animated: true)
    }
    
    
}
