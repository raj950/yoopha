//
//  SignupVC.swift
//  Yoopha
//
//  Created by Yoopha on 13/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift

class SignupVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var confirmPasswordTF: CustomTF!
    @IBOutlet weak var passwordTF: CustomTF!
    @IBOutlet weak var mailTF: CustomTF!
    @IBOutlet weak var mobileNumberTF: CustomTF!
    

   
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        return true
    }
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
        
    }

    @IBAction func tappedOnLoginButton(_ sender: Any) {
        let navToLogin = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
      //  self.navigationController?.popViewController(animated: true)
        self.present(navToLogin, animated: false, completion: nil)
    }
    
    

    
    func getDate(){
        if WebApiCallBack.isConnectedToNetwork() != true {
            simpleAlert(message:  "Please verify internet connectivity.")
            return
        }else{
            createConsumer()
        }
    }
    
    
//    func verifyEmail(){
//        let myParam = NSMutableDictionary()
//        let url = "\(Constants.API.base_URL)\(Constants.API.verifyEmail)" + Constants.ConstantStrings.slash
//        let finalUrl = url + mailTF.text!
//
//        WebApiCallBack.makeGetCall(webUrl: finalUrl, paramData: myParam) { (response, error) in
//            print(response as Any)
//             print(finalUrl)
//            if let myResponse = JSON(response).dictionary{
//                let message = myResponse["message"]?.string
//                let isValid = myResponse["is_email_valid"]?.string
//                if isValid == "Y"{
//
//                    }
////                    let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.verifyOTP) as! Verify_OTPVC
////                    self.present(navTo, animated: true, completion: nil)
//
//                }else if isValid == "N"{
//                    simpleAlert(message: message!)
//                }
//
//
//            }
//
//        }
    
    func verifyMobileNumber(){
        
          let myParam = NSMutableDictionary()
        let url = "\(Constants.API.base_URL)\(Constants.API.verifyMobileNumber)"
        let finalUrl = url + mobileNumberTF.text!
        
        WebApiCallBack.makeGetCall(webUrl: finalUrl, paramData: myParam) { (response, error) in
            print(response as Any)
            print(finalUrl)
            
            DispatchQueue.main.async {
                if let myResponse = JSON(response).dictionary{
                    let message = myResponse["message"]?.string
                    let isValid = myResponse["is_mobile_valid"]?.string
                    if isValid == "Y"{
                        
                        let myParam = NSMutableDictionary()
                        myParam.setObject(self.mobileNumberTF.text!, forKey: "username" as NSCopying)
                        myParam.setObject(self.mailTF.text!, forKey: "email" as NSCopying)
                        myParam.setObject(self.passwordTF.text!, forKey: "password" as NSCopying)
                        myParam.setObject(self.confirmPasswordTF.text!, forKey: "confirm_password" as NSCopying)
                        myParam.setObject(self.mobileNumberTF.text!, forKey: "mobile_no" as NSCopying)
                            // myParam.setObject(mobileNumberTF.text!, forKey: "mobile_no" as NSCopying)
                            //myParam.setObject("True", forKey: "is_verified" as NSCopying)
                        
                        WebApiCallBack.requestApi(webUrl:"\(Constants.API.base_URL)\(Constants.API.consumerRegister)", paramData: myParam as NSObject, token: "",methiod: "post", completionHandler: { (response, error) -> () in
                            // print("responseObject = \(response); error = \(error)")
                        print(response as Any)
                            if  let myResponse = JSON(response as Any).dictionary{
                                var signUpData = SignUpModel()
                                print(myResponse)
                                    
                                signUpData.email = myResponse["email"]?.string
                                signUpData.mobile = myResponse["mobile_no"]?.string
                                
                                SignUpModel.mobileNumber = myResponse["mobile_no"]?.string
                                    
                                    print(SignUpModel.mobileNumber)
                                let isVerified = myResponse["is_verified"]?.bool
                                    
                                if isVerified == false{
                                        
                                    OTPResultModel.mobile_no = myResponse["mobile_no"]?.string
                                        
                                    let slash = "/"
                                        // let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y01_common/generate_otp/"
                                    let finalUrl = "\(Constants.API.base_URL)\(Constants.API.generateOTP)" +  OTPResultModel.mobile_no! + slash
                                    print(finalUrl)
                                        
                                    WebApiCallBack.makeGetCall(webUrl: finalUrl, paramData: myParam) { (response, error) in
                                        print(response as Any)
                                            
                                        DispatchQueue.main.async {
                                        let navToVerifyOTPVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.verifyOTP) as! Verify_OTPVC
                                                
                                       // self.navigationController?.pushViewController(navToVerifyOTPVC, animated: true)

                                                self.present(navToVerifyOTPVC, animated: false, completion: nil)
                                            }
                                            
                                        }
                                        
                                    }
                                    //            if let email = myResponse["email"]?.array{
                                    //                let emailValue = email[0].string
                                    //                if emailValue == "yoopha_user with this email already exists."{
                                    //                    AlertView.instance.showAlert(title: "E-Mail", message: "User Already Existed With This E-Mail Id", alertType: .failure)
                                    //                    AlertView.instance.tokenLbl.isHidden = true
                                    //                    AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                                    //                }
                                    //                else if let mobile = myResponse["username"]?.array{
                                    //                    let mobileValue = mobile[0].string
                                    //                    if mobileValue == "yoopha_user with this mobile no already exists."{
                                    //                        AlertView.instance.showAlert(title: "Mobile Number", message: "User Already Existed With This Mobile Number", alertType: .failure)
                                    //                        AlertView.instance.tokenLbl.isHidden = true
                                    //                        AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                                    //                    }
                                    //                }
                                    //            }
                                    
                                    
                                    //               var signUpData = SignUpModel()
                                    //                   let mobile = myResponse["mobile_no"]?.string
                                    //                    let email = myResponse["email"]?.string
                                    //            signUpData.mobile = mobile
                                    //             signUpData.email = email
                                    //
                                    //            if self.mobileNumberTF.text != signUpData.mobile{
                                    //                let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.verifyOTP) as! Verify_OTPVC
                                    //                self.present(navTo, animated: true, completion: nil)
                                    //
                                    //            }
                                    //            if self.mailTF.text != signUpData.email{
                                    //                let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.verifyOTP) as! Verify_OTPVC
                                    //                self.present(navTo, animated: true, completion: nil)
                                    //            }
                                    
                                }
                                
                                
                                
                                
                                
                            })
                            //                    let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.verifyOTP) as! Verify_OTPVC
                            //                    self.present(navTo, animated: true, completion: nil)
                        
                            }
                        else{
                        simpleAlert(message: message!)
                    }
                    
                    
                }
            }
            
            
        }
    }
    
    func createConsumer(){
        
        verifyMobileNumber()
        //verifyEmail()
   
       
    }
    @IBAction func tappedOnSignupButton(_ sender: Any) {
        
        if mobileNumberTF.text!.isEmpty ||  mailTF.text!.isEmpty ||  passwordTF.text!.isEmpty ||  confirmPasswordTF.text!.isEmpty == true{
            simpleAlert(message: "Please Check Your Details")
       
        }else if passwordTF.text != confirmPasswordTF.text{
            simpleAlert(message: "Password and confirm password should be same")
        }else if mobileNumberTF.text?.count != 10{
            simpleAlert(message: "Please Enter Valid Mobile Number")
        }
        else{
              getDate()
        }
        
    }

}

struct SignUpModel{
    
    var mobile:String?
    var email:String?
    
    static var mobileNumber:String?
   // init(){}
    
}
