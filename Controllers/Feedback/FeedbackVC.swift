//
//  ViewController.swift
//  TableViewDropDown
//
//  Created by BriefOS on 5/3/17.
//  Copyright © 2017 BriefOS. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ExpandableHeaderViewDelegate {
   
    
var selectedElement = [Int : Int]()
     static let shared = FeedbackVC()
    @IBOutlet weak var tableView: UITableView!
    private var selectedOption:Int?
    
    var sections = [
        Section(quastion: "Quastion1",
                options: ["Options1", "Options2"],
                expanded: false),
        Section(quastion: "Quastion2",
                options: ["Options1", "Options2 Options Options Options Options Options Options Options ","Options3","Options2 Options Options Options Options" ],
                expanded: false),
        Section(quastion: "Quastion3",
                options: ["Options1", "Options2","Options3", "Options4"],
                expanded: false)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].options.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (sections[indexPath.section].expanded) {
            return 80
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpandableHeaderView()
        header.customInit(title: sections[section].quastion, section: section, delegate: self)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTVC") as! FeedbackTVC
        cell.selectionStyle = .none
        let answer = sections[indexPath.section].options[indexPath.row]
        cell.optionsLbl.text = answer
        cell.index = indexPath
        
        if indexPath.row == self.selectedElement[indexPath.section]{
            cell.optionsImage.image = UIImage(named: "selected-button")
        }
        else {
           cell.optionsImage.image = UIImage(named: "unselected-button")
        }
        
        cell.layoutSubviews()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        print(indexPath)
        
        let keys = selectedElement.keys
        
        if keys.contains(indexPath.section) {
            if selectedElement[indexPath.section] == indexPath.row {
            }else {
                selectedElement.removeValue(forKey: indexPath.section)
                selectedElement.updateValue(indexPath.row, forKey: indexPath.section)
            }
        }else {
            selectedElement.updateValue(indexPath.row, forKey: indexPath.section)
        }
        
        self.tableView.reloadData()

    }
    
    func toggleSection(header: ExpandableHeaderView, section: Int, button:UIButton, view:UIView, image:UIImageView) {
        sections[section].expanded = !sections[section].expanded
        
        
        tableView.beginUpdates()
        for i in 0 ..< sections[section].options.count {
            tableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        tableView.endUpdates()
    }
    
}
