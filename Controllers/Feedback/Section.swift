//
//  Section.swift
//  TableViewDropDown
//
//  Created by BriefOS on 5/3/17.
//  Copyright © 2017 BriefOS. All rights reserved.
//

import Foundation

struct Section {
    var quastion: String!
    var options: [String]!
    var expanded: Bool!
    
    init(quastion: String, options: [String], expanded: Bool) {
        self.quastion = quastion
        self.options = options
        self.expanded = expanded
    }
}
