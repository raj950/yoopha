//
//  OTPVC.swift
//  Yoopha
//
//  Created by Yoopha on 03/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import SwiftyJSON



class OTPVC: UIViewController {

   
    @IBOutlet weak var mobileNumberTF: CustomTF!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }

    @IBAction func getOTP(_ sender: Any) {

        if mobileNumberTF.text?.isEmpty == true{
            simpleAlert(message: "Please Enter Your Mobile Number")
        }else if mobileNumberTF.text?.count != 10 {
             simpleAlert(message: "Please Enter Valid Mobile Number")
        }else{
            OTPResultModel.shared.getOTP(MobileString: mobileNumberTF.text!)
            let navToVerifyOTPVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.verifyOTP) as! Verify_OTPVC
            self.present(navToVerifyOTPVC, animated: false, completion: nil)
        }
    }
    @IBAction func tappedOnLoginBtn(_ sender: Any) {
        let navToLogin = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
        present(navToLogin, animated: false, completion: nil)
     //  self.navigationController?.popViewController(animated: true)
    }
    
   
}


class OTPResultModel {
    
     static let shared = OTPResultModel()
    
    static var mobile_no:String?
    
    static let message = String()
    static var otp : String?
    
    static let otpVC = OTPVC()
    
    func getOTP(MobileString:String){
        let myParam = NSMutableDictionary()
       // mobileNumberTF.text = mobileNumberTF.text
        let slash = "/"
       // let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y01_common/generate_otp/"
        let finalUrl = "\(Constants.API.base_URL)\(Constants.API.generateOTP)" + "" + MobileString + "" + slash
        print(finalUrl)
        
        WebApiCallBack.makeGetCall(webUrl: finalUrl, paramData: myParam) { (response, error) in
            print(response as Any)
            
            
            OTPResultModel.mobile_no = response!["mobile_no"] as! String
            OTPResultModel.otp = (response!["message"] as! String)
            
            print(OTPResultModel.otp)
            print(OTPResultModel.mobile_no)
        }
    }
    
}
//OTP sent successfully
