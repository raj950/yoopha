//
//  CreateEmegencySOS_VC.swift
//  Yoopha
//
//  Created by Yoopha on 18/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
class CreateEmegencySOS_VC: UIViewController {

    let location = CLLocationManager()
    var latitude = ""
    var longitude = ""
    var btnActon:Bool = true
    var userID:Int?
    var userSOSDetails:[SOSModel] = []
    @IBOutlet weak var professionBtn: CustomButton!
    @IBOutlet weak var willingBtn: CustomButton!
    @IBOutlet weak var professionalInputStackView: UIStackView!
    @IBOutlet weak var professionalStackView: UIStackView!
    @IBOutlet weak var termsAndConditionsBtn: CustomButton!
    @IBOutlet weak var primaryName: CustomTF!
    @IBOutlet weak var primaryMobile: CustomTF!
    @IBOutlet weak var secondaryName: CustomTF!
    @IBOutlet weak var secondaryMobile: CustomTF!
    @IBOutlet weak var professionTF: CustomTF!
    var sosResultsData:[SOSModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsAndConditionsBtn.underline()
        professionalStackView.isHidden = true
        professionalInputStackView.isHidden = true
        
          getUserLocation()
        //  get_SOS_By_UserId()
        
        
    }
    
    
    func createSOS(){
      
        
        print(longitude)
        print(latitude)
        let params = [
            "user": loginModel.userid,
            "is_enabled": true,
            "can_support": false,
            "healthcare_professional": false,
            "profession": professionTF.text as Any,
            "longitude": longitude as Any,
            "latitude": latitude as Any,
            "primary_contact_name": primaryName.text as Any,
            "primary_contact_mobile":primaryMobile.text as Any,
            "secondary_contact_name":secondaryName.text as Any,
            "secondary_contact_mobile": secondaryMobile.text as Any
            ] as [String : Any]
        
        let url1 = "\(Constants.API.base_URL)\(Constants.API.create_sos)"
        print(url1)
        WebApiCallBack.shared.POSTServiceHeader(serviceType: url1, parameters: params, token: loginModel.token) { (response) -> (Void) in
            print(response)
            if response != nil{
                let response = JSON(response).dictionary
                let user_SOSID = response!["is_enabled"]?.bool
                    if  user_SOSID == true{
                        simpleAlert(message: "Your SOS is in activation")
                    }else{
                        let userData = response!["user"]?.dictionary
                            let userSOSData = SOSModel()
                            self.userSOSDetails.append(userSOSData)
                        let primary_contact_name = response!["primary_contact_name"]?.string
                                    print(primary_contact_name as Any)
                        let navTo = self.storyboard?.instantiateViewController(withIdentifier:"SOS_StatusVC") as! SOS_StatusVC
                        
                        self.navigationController?.pushViewController(navTo, animated: true)
                    }
            }
                }
        }
 
    @IBAction func navBack(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.appointmentsVC) as! AppointmentsVC
        
        self.navigationController?.pushViewController(navTo, animated: true)
    }
    
    @IBAction func tappedOnAgreeAndSubmit(_ sender: Any) {
        
        if primaryName.text?.isEmpty == true || primaryMobile.text?.isEmpty == true || secondaryMobile.text?.isEmpty == true || secondaryName.text?.isEmpty == true{
            
            simpleAlert(message: "Please Check Your Details")
        }else{
             createSOS()
        
            
        }
    }
    @IBAction func tappedOnWillingBtn(_ sender: Any) {
        
        btnActon = !btnActon
        
        if btnActon {
            willingBtn.setImage(UIImage(named: "Rectangle"), for: .normal)
            professionalStackView.isHidden = true
        } else {
            willingBtn.setImage(UIImage(named: "success_Image"), for: .normal)
            
            professionalStackView.isHidden = false
        }

    }
    @IBAction func tappedOnProfessionBtn(_ sender: Any) {
        btnActon = !btnActon
        
        if btnActon {
            professionBtn.setImage(UIImage(named: "Rectangle"), for: .normal)
            professionalInputStackView.isHidden = true
        } else {
            professionBtn.setImage(UIImage(named: "success_Image"), for: .normal)
            
            professionalInputStackView.isHidden = false
        }
       
    }
    
    @IBAction func teppenOnTermsAndConditions(_ sender: Any) {
    }
}


extension CreateEmegencySOS_VC:CLLocationManagerDelegate{
    
    func getUserLocation(){
        self.location.requestAlwaysAuthorization()
        self.location.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            location.delegate = self
            location.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            location.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let userLocations: CLLocationCoordinate2D = manager.location?.coordinate else {
            return
        }
        
        
        latitude = String(userLocations.latitude)
        longitude = String(userLocations.longitude)
    }
}

struct SOSModel{
     var id:Int?
    
     static var user_SOS_Id:Int?
     static var userLatitude:Float?
     static var userLongitude:Float?
}


