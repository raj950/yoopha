//
//  SOS_StatusVC.swift
//  Yoopha
//
//  Created by Yoopha on 18/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON

class SOS_StatusVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        turnOnSOS()
        
        
    }
    
    func turnOnSOS(){
        
        let parameters = [
            "user_sos": SOSModel.user_SOS_Id as Any,
            "is_emergency": true,
            "is_solved": false,
            "latitude":SOSModel.userLatitude as Any,
            "longitude":SOSModel.userLongitude as Any
            ] as [String : Any]
        
        let url1 = "\(Constants.API.base_URL)\(Constants.API.create_sos_on)"
     
        let url = URL(string: url1)
        if let usableUrl = url {
            var request = URLRequest(url: usableUrl)
            request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      
            request.httpMethod = "POST"
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {return}
           
            request.httpBody = httpBody
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data {
                    if let resposeDictionary = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
                        
                        print(resposeDictionary)
                        
                        if let response = JSON(resposeDictionary).dictionary{
                            print(response)
                            
                            let id = response["sos"]?.int
                            print(id)
                            
                        }
                    }
                    
                }
            })
            task.resume()
        }
        
    }
    
    
    @IBAction func tappedOnContinueBtn(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.appointmentsVC) as! AppointmentsVC
        
        navTo.sosButtonStatus = true
        
        UserDefaults.standard.set(true, forKey: "status")

        
        self.navigationController?.pushViewController(navTo, animated: true)
  
    }
    
    @IBAction func navBack(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.appointmentsVC) as! AppointmentsVC
        navTo.sosButtonStatus = false
        self.navigationController?.pushViewController(navTo, animated: true)
    }
}


