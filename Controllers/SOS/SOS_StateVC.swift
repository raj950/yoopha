//
//  SOS_StateVC.swift
//  Yoopha
//
//  Created by Yoopha on 18/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON

class SOS_StateVC: UIViewController {

    @IBOutlet weak var sosView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.tabBarController?.tabBar.isHidden = true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
   
    override func viewWillAppear(_ animated: Bool) {
        if UIViewController.self == SOS_StateVC.self{
       
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.isHidden = true
        }else{
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar.isHidden = false
        }
    }
    @IBAction func navBack(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.appointmentsVC) as! AppointmentsVC
        navTo.sosButtonStatus = true
        self.navigationController?.pushViewController(navTo, animated: true)
    }
    
    @IBAction func tappedOnDeactivateBtn(_ sender: Any) {
        
        alertWithMessage(title: "Deactivate SOS", message: "Are you sure? Do you want to deactivate this distress call", cancelButtonTitle: "CANCEL", buttonTitle: "DEACTIVATE", handler: { (action) in
            
            self.deActivateSOS()
            
        }) { (action) in
            self.resignFirstResponder()
        }
    }
    
    
    func deActivateSOS(){
        
       turnOff_SOS()
        UserDefaults.standard.set(false, forKey: "status")
        let navTo = self.storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.mainMenuTBC) as! MainMenuTBC
      //  navTo.sosButtonStatus = false
        
         present(navTo, animated: false, completion: nil)
    }
    
    func turnOff_SOS(){
        print(SOSModel.user_SOS_Id!)
        let url1 = "\(Constants.API.base_URL)\(Constants.API.deactivate_by_sos_id)"
        let finalUrl = url1 + "\(SOSModel.user_SOS_Id!)"
        print(finalUrl)
        WebApiCallBack.shared.GETServiceHeader(url: finalUrl, token: loginModel.token) { (response) -> (Void) in
            print(response)
            
            if response != nil{
                var SOS_DeactiveData = SOS_DeactiveModel()
               if let response = JSON(response).dictionary{
                    let SOS_DeactiveId = response["sosID"]?.string
                    SOS_DeactiveData.sosID = SOS_DeactiveId
                    print(SOS_DeactiveData.sosID)
                }
               
            }
        }
    }
}

struct SOS_DeactiveModel {
    var sosID:String?
    var is_solved:String?
    var message:String?
}


