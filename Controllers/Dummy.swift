//
//  Dummy.swift
//  Yoopha
//
//  Created by Yoopha on 22/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class Dummy: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
