//
//  PrescriptionDetailView.swift
//  Yoopha
//
//  Created by Yoopha on 13/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PrescriptionDetailVC: UIViewController {

    @IBOutlet weak var prescriptionTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        raj()
        registerCell()
    }

    func raj(){
        let params = NSMutableDictionary()
        let URL = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y08_eprescription/prescription_view_by_appointment_id/223/"
        
        
        let url = "y08_eprescription/prescription_view_by_appointment_id/"
        let url2 = 223
        let finalUrl = Constants.API.base_URL + url + "\(url2)/"
        print(finalUrl)
        WebApiCallBack.shared.GETServiceHeader(serviceType: URL, parameters: params as! [String : Any], token: loginModel.token) { (response) -> (Void) in
            print(response)
        }
    }
    
    func registerCell(){
        let prescriptionCell = UINib(nibName: "PrescriptionTVC", bundle: nil)
        prescriptionTV.register(prescriptionCell, forCellReuseIdentifier: "PrescriptionTVC")
        
    }

    @IBAction func navBack(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.E_PrescriptionVC) as! E_PrescriptionVC
        present(navTo, animated: false, completion: nil)
        
    }
}

extension PrescriptionDetailVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrescriptionTVC") as! PrescriptionTVC
        
        return cell
    }
}
