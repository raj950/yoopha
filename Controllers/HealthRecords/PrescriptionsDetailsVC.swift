//
//  PrescriptionsDetailsVC.swift
//  Yoopha
//
//  Created by Yoopha on 14/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class PrescriptionsDetailsVC: UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        presDetails()
       
    }
    

    func presDetails(){
        let params = NSMutableDictionary()
        let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y08_eprescription/prescription_view_by_appointment_id/223/"
        WebApiCallBack.shared.GETServiceHeader(serviceType: url, parameters: params as! [String : Any], token: loginModel.token) { (response) -> (Void) in
            print(response)
        }
    }

}
