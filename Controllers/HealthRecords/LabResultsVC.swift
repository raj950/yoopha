//
//  LabResultsVC.swift
//  Yoopha
//
//  Created by Yoopha on 18/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class LabResultsVC: UIViewController {

    var backButtonStatus = false
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var labResultsTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        
        
        if backButtonStatus == false{
            backBtn.isHidden = true
        }
       registerCell()
    }
    
    func registerCell(){
        let labCell = UINib(nibName: "LabTVC", bundle: nil)
        labResultsTV.register(labCell, forCellReuseIdentifier: "LabTVC")
        labResultsTV.separatorStyle = .none
        labResultsTV.showsVerticalScrollIndicator = false
    }

    
    @IBAction func navBack(_ sender: Any) {
//        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.mainMenuTBC) as! MainMenuTBC
//        present(navTo, animated: false, completion: nil)
        navigationController?.popViewController(animated: true)
    }
}

extension LabResultsVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabTVC") as! LabTVC
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.labResultsDetailsVC) as! LabResultsDetailsVC
        navigationController?.pushViewController(navTo, animated: true)
        //present(navTo, animated: false, completion: nil)
    }
    
}
