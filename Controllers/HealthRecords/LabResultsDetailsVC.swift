//
//  LabResultsDetailsVC.swift
//  Yoopha
//
//  Created by Yoopha on 25/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class LabResultsDetailsVC: UIViewController {

    @IBOutlet weak var clinicTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cellRegister()
    }
    
    func cellRegister(){
        let clinicCell = UINib(nibName: "ClinicTVC", bundle: nil)
        clinicTV.register(clinicCell, forCellReuseIdentifier: "ClinicTVC")
        clinicTV.separatorStyle = .none
    }
    
    @IBAction func navBack(_ sender: Any) {
        
//        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.sideMenuController) as! SideMenuController
//       navigationController?.pushViewController(navTo, animated: true)
        navigationController?.popViewController(animated: true)
    }
}

extension LabResultsDetailsVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClinicTVC") as! ClinicTVC
      cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: "LabAppointmentsDetailsVC") as! LabAppointmentsDetailsVC
        present(navTo, animated: false, completion: nil)
    }
    
}
