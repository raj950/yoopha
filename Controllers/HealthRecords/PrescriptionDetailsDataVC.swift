//
//  PrescriptionDetailsDataVC.swift
//  Yoopha
//
//  Created by Yoopha on 14/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SideMenuSwift

class PrescriptionDetailsDataVC: UIViewController {

    @IBOutlet weak var billAmountLbl: UILabel!
    @IBOutlet weak var doctorIdLbl: UILabel!
    @IBOutlet weak var doctorNameLbl: UILabel!
    @IBOutlet weak var patientIdLbl: UILabel!
    @IBOutlet weak var patientNameLbl: UILabel!
    @IBOutlet weak var prescriptionId: UILabel!
    @IBOutlet weak var detailedPrescriptionTV: UITableView!
    
    var productsInfo:[ProductDetailsModel]=[]
    override func viewDidLoad() {
        super.viewDidLoad()

        print(ConsumerAddressModel.id as Any)

        clinicDetails()
        registerCell()
    }

    override func viewWillAppear(_ animated: Bool){
        presDetails()
    }
    func presDetails(){
      
        let finalUrl = "\(Constants.API.base_URL)\(Constants.API.observation_view_by_appointment)"
    
        let finalUrl1 = finalUrl + "\(PrescriptionResultModel.appointmentID!)"
        print(PrescriptionResultModel.appointmentID!)
        print(finalUrl1)
        let url = URL(string: finalUrl1)
        if let usableUrl = url {
            var request = URLRequest(url: usableUrl)
            request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data {
                    if let response = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
                        
                        print(response)
                        
                        if let response = JSON(response).dictionary{
                            if let results = response["results"]?.array{
                                for result in results{
                                    let doctorAppointments = result["appointment"].dictionary
                                    let doctorDetails = doctorAppointments!["doctor"]?.dictionary
                                    var doctorData = DoctorDeatils()
                                    doctorData.doctor_name = doctorDetails!["doctor_name"]?.string
                                    doctorData.doctorID = doctorDetails!["doctorID"]?.string
                                    let patientInfo = doctorAppointments!["patient"]?.dictionary
                                    var patientData = PatientDetailsModel()
                                    patientData.name = patientInfo!["name"]?.string
                                     patientData.patientID = patientInfo!["patientID"]?.string
                                    DispatchQueue.main.async {
                                        self.doctorNameLbl.text = doctorData.doctor_name
                                        self.doctorIdLbl.text = doctorData.doctorID
                                        self.patientNameLbl.text = patientData.name
                                        self.patientIdLbl.text = patientData.patientID
                                    }
                                }
                           
                            }
                            
                        }
                        
                    }
                    
                }
                
            })
            task.resume()
        }
    }

    func clinicDetails(){
        let finalUrl = "\(Constants.API.base_URL)\(Constants.API.prescription_view_by_appointment_id)"
        
        let finalUrl1 = finalUrl + "\(PrescriptionResultModel.appointmentID!)/"
        print(finalUrl1)
        let url = URL(string: finalUrl1)
        if let usableUrl = url {
            var request = URLRequest(url: usableUrl)
            request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data {
                    if let response = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
                        
                        print(response)
                        
                        if let response = JSON(response).dictionary{
                            if let results = response["results"]?.array{
                                
                                for result in results{
                                   
                                    let prescription = result["prescription"].dictionary
                                    let prescriptionId = prescription!["prescriptionID"]?.string
                                    let billAmount = prescription!["bill_amount"]?.string
                                    
                                    PrescriptionResultModel.totalAmount = billAmount!
                                    print(PrescriptionResultModel.totalAmount)
                                    
                                    DispatchQueue.main.async {
                                        self.prescriptionId.text = prescriptionId
                                        self.billAmountLbl.text = billAmount
                                    }
                                    
                                    let productsData = prescription!["product_detail"]?.array
                                    var productData1 = ProductDetailsModel()
                                    for products in productsData!{
                                     
                                        productData1.product_name = products["product_name"].string
                                        productData1.cost_per_unit = products["cost_per_unit"].string
                                        productData1.count = products["count"].int
                                        productData1.total_price = products["total_price"].string
                                        productData1.a_food = products["a_food"].bool
                                        productData1.b_food = products["b_food"].bool
                                        productData1.morning = products["morning"].bool
                                        productData1.noon = products["noon"].bool
                                        productData1.evening = products["evening"].bool
                                        self.productsInfo.append(productData1)
                                        }
                                 
                                    
                                    print("productName:\(productData1.product_name)")
                                  
                                    }
                           
                            }
                            
                        }
                        DispatchQueue.main.async {
                            self.detailedPrescriptionTV.reloadData()
                        }
                    }
                    
                }
                
            })
            task.resume()
        }
    }
    func registerCell(){

        let prescriptionCell = UINib(nibName: "PrescriptionTVC", bundle: nil)
        detailedPrescriptionTV.register(prescriptionCell, forCellReuseIdentifier: "PrescriptionTVC")
    }
    @IBAction func tappedOnPlaceOrder(_ sender: Any) {
        placeOrder()
    }
    
    
    func placeOrder(){
        print(ConsumerAddressModel.id)
        let params = ["pharmacy": 1,
                      "prescription": 1,
                      "delivery_type": 1,
                      "status": 1,
                      "payment_mode": 2,
                      "is_active": true,
                      "shipping_address": ConsumerAddressModel.id,
                      "billing_address": ConsumerAddressModel.id,
                      "order_amount": PrescriptionResultModel.totalAmount,
                      "time_to_deliver": "2019-08-30T12:00:00+05:30"] as [String : Any]
        WebApiCallBack.shared.POSTServiceHeader(serviceType: "\(Constants.API.base_URL)\(Constants.API.create_pharmacy_order)", parameters: params, token: loginModel.token) { (response) -> (Void) in
            print(response)
            
            if response != nil{
                if let orderResponse = JSON(response).dictionary{
                
                    let status = orderResponse["status"]?.int
                    print(status)
                    
                    if let orderStatus = status{
                        
                        AlertView.instance.tokenLbl.isHidden = true
                        AlertView.instance.showAlert(title: "Order Placed", message: "Your order is placed succefully", alertType: .success)
                        AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                        AlertView.instance.doneBtn.addTarget(self, action: #selector(self.navTo), for: .touchUpInside)
                    }
                    
                }
            }
        }
    }
    
    @objc func navTo(){
    
        let navToMM = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.sideMenuController) as! SideMenuController
        present(navToMM, animated: false, completion: nil)
    }
    @IBAction func navBack(_ sender: Any) {
        
       // let navTo = storyboard?.instantiateViewController(withIdentifier: "E_PrescriptionVC") as! E_PrescriptionVC
     //   present(navTo, animated: false, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
}

extension PrescriptionDetailsDataVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return productsInfo.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let count = "\(productsInfo[indexPath.row].count!)"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrescriptionTVC") as! PrescriptionTVC
        cell.drugNameLbl.text = productsInfo[indexPath.row].product_name
        cell.rateLbl.text = productsInfo[indexPath.row].cost_per_unit
        cell.quantityLbl.text = count
        cell.amountLbl.text = productsInfo[indexPath.row].total_price
        
        if productsInfo[0].a_food == true{
            cell.afterFoodBtn.setImage(UIImage(named: "unselected"), for: .normal)
        }
        if productsInfo[0].b_food == true{
            cell.beforeFoodBtn.setImage(UIImage(named: "unselected"), for: .normal)
        }
        if productsInfo[0].morning == true{
             cell.mornBtn.setImage(UIImage(named: "unselected"), for: .normal)
        }
        if productsInfo[0].noon == true{
            cell.noonBtn.setImage(UIImage(named: "unselected"), for: .normal)
        }
        if productsInfo[0].evening == true{
            cell.evenBtn.setImage(UIImage(named: "unselected"), for: .normal)
        }
        
        cell.selectionStyle = .none
        
        return cell
    }


}

struct DoctorAppointmentsModel{
    var doctorID:String?
    var doctor_name:String?
}

