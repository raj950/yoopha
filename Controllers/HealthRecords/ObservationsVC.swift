//
//  ObservationsVC.swift
//  Yoopha
//
//  Created by Yoopha on 07/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON

class ObservationsVC: UIViewController {

    @IBOutlet weak var symptomLbl: UILabel!
    @IBOutlet weak var allergyLbl: UILabel!
    @IBOutlet weak var drDescriptionLbl: UILabel!
    
    @IBOutlet weak var sugarLbl: UILabel!
    @IBOutlet weak var temperatureLbl: UILabel!
    @IBOutlet weak var BPLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailedObservations()
   
    }
    
    func detailedObservations(){
      
        let url =  "\(Constants.API.base_URL)\(Constants.API.observation_view_by_appointment)\(AppointmentStatusModel.patientAppointmentId!)"
        print(url)
        WebApiCallBack.shared.GETService123(extraParam: url) { (response) -> (Void) in
            print(response)
            
            if response != nil{
                if let response = JSON(response).dictionary{
                    
                    if let results = response["results"]?.array{
                        
                        for result in results{
                            let appointment = result["appointment"].dictionary
                            
                            let weight = result["weight"].string
                            let blood_pressure = result["blood_pressure"].string
                            let sugar = result["sugar"].string
                            let temperature = result["temperature"].string
                          //  let desc = result["desc"].string
                            
                            
                        let symptoms = result["symptoms"].array
                            
                            for ailment in symptoms!{
                                
                                let ailment = ailment["ailment"].int
                                
                                print(ailment)
                            
                            
                            let allergies = result["allergies"].array
                            
                            for allergy in allergies!{
                                
                                let allergy = allergy["allergy"].string
                                
                                print(allergy)
                            
                            
                            
                            let doctor = appointment!["doctor"]?.dictionary
                            
                            if let drSpeciality = doctor!["doctor_speciality"]?.dictionary{
                               let DR_Speciality = drSpeciality["desc"]?.string
                                
                                DispatchQueue.main.async {
                                    self.drDescriptionLbl.text = DR_Speciality
                                    self.weightLbl.text = weight
                                    self.BPLbl.text = blood_pressure
                                    self.sugarLbl.text = sugar
                                    self.temperatureLbl.text = temperature
                                    self.allergyLbl.text = allergy
                                    self.symptomLbl.text = "\(ailment)"
                                    
                                }
                            }
                        
                            }
                            }
                        }
                }
            }
            }
        }
    }


    @IBAction func navBank(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.observationsListVC) as! ObservationsListVC
        present(navTo, animated: false, completion: nil)
    }
}
