//
//  ObservationsListVC.swift
//  Yoopha
//
//  Created by Yoopha on 16/08/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ObservationsListVC: UIViewController {

    
    var docDetails:[DoctorDeatils] = []
    var patientDetails:[PatientDetailsModel] = []
    var appointmentsDetails:[AppointmentsModel] = []
    var month:String?
    
    @IBOutlet weak var observationsTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

       registerCell()
       obervationsList()
    }
    
    
    func registerCell(){
         let observationsCell = UINib(nibName: "ObservationTVC", bundle: nil)
        self.observationsTV.register(observationsCell, forCellReuseIdentifier: "ObservationTVC")
    }
   
    
    func obervationsList(){
        
        
        let params = NSMutableDictionary()
        let url = "\(Constants.API.base_URL)\(Constants.API.observation_view_by_appointment)" + "\(AppointmentStatusModel.patientAppointmentId!)"
        print(url)
        WebApiCallBack.makeGetCall(webUrl: url, paramData: params) { (response, error) in
            print(response)
            
            if response != nil{
                
                if let response = JSON(response).dictionary{
                     if let results = response["results"]?.array{
                        
                        for result in results{
                            
                            var drDetails = DoctorDeatils()
                         //   let id = result["id"].int
                            var appointmentData = AppointmentsModel()
                            let apt = result["appointment"].dictionary
                            let bookingDate = apt!["booking_date"]?.string
                            let aptId = apt!["id"]?.int
                            let doctor = apt!["doctor"]?.dictionary
                            let doctorName = doctor!["doctor_name"]?.string
                            let doctorEducation = doctor!["doctor_education"]?.string
                            let symptoms = apt!["symptoms"]?.string
                            appointmentData.symptoms = symptoms
                            appointmentData.booking_date = bookingDate
                            appointmentData.id = aptId
                            drDetails.doctor_name = doctorName
                            drDetails.doctor_education = doctorEducation
                            var patientData = PatientDetailsModel()
                            let patient = apt!["patient"]?.dictionary
                            let patientName = patient!["name"]?.string
                            let patientId = patient!["patientID"]?.string
                            patientData.name = patientName
                            patientData.patientID = patientId
                            self.docDetails.append(drDetails)
                            self.patientDetails.append(patientData)
                            self.appointmentsDetails.append(appointmentData)
                            
                            print(self.docDetails)
                            print(self.docDetails.count)
                            
                            
                            let first4 = appointmentData.booking_date!.prefix(4)
                            
                            let startIndex = appointmentData.booking_date!.index(appointmentData.booking_date!.startIndex, offsetBy: 5)
                            let endIndex = appointmentData.booking_date!.index(appointmentData.booking_date!.startIndex, offsetBy: 6)
                            let monthtrim = String(appointmentData.booking_date![startIndex...endIndex])
                            
                            
                            let monthNameGet = DateFormatter().monthSymbols[Int(monthtrim)! - 1]
                            
                            
                            let one = appointmentData.booking_date!.index(appointmentData.booking_date!.startIndex, offsetBy: 8)
                            let two = appointmentData.booking_date!.index(appointmentData.booking_date!.startIndex, offsetBy: 9)
                            let dates = String(appointmentData.booking_date![one...two])
                            
                            self.month = first4 + "-" + monthNameGet + "-"  + dates
                            
                        }
                    }
             
                }
                
                DispatchQueue.main.async {
                     self.observationsTV.reloadData()
                }
               
            }
            
            
        }
    }



    @IBAction func navBack(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.mainMenuTBC) as! MainMenuTBC
        present(navTo, animated: false, completion: nil)
    }
    

}

extension ObservationsListVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return docDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObservationTVC") as! ObservationTVC
        cell.selectionStyle = .none
        cell.viewBtn.tag = indexPath.row
        cell.viewBtn.addTarget(self, action: #selector(navTo), for: .touchUpInside)
        cell.drNameLbl.text = docDetails[indexPath.row].doctor_name
        cell.drEducationLbl.text = docDetails[indexPath.row].doctor_education
        cell.patientNameLbl.text = patientDetails[indexPath.row].name
        cell.patientIdLbl.text = patientDetails[indexPath.row].patientID
        cell.symptomsLbl.text = appointmentsDetails[indexPath.row].symptoms
        cell.bookingDateLbl.text = month
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.observationsVC) as! ObservationsVC
        present(navTo, animated: false, completion: nil)
        
    }
    
    @objc func navTo(){
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.observationsVC) as! ObservationsVC
        present(navTo, animated: false, completion: nil)
    }
}

struct PatientDetailsModel {
    
    var id:Int?
    var patientID:String?
    var name:String?
}

struct AppointmentsModel {
    var id:Int?
    var symptoms:String?
    var booking_date:String?
    
}
