//
//  E-PrescriptionVC.swift
//  Yoopha
//
//  Created by Yoopha on 18/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class E_PrescriptionVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    var backButtonStatus = false
    var prescriptionResultArr = [PrescriptionResultModel]()
    var productResultArr = [ProductDetailsModel]()
    var month:String?
    var date:NSDate?
    var drAppID:Int?
   // var finalName:String = ""
    var twoStrings : [String] = []
    
    @IBOutlet weak var prescriptionTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if backButtonStatus == false{
            backBtn.isHidden = true
        }
        prescriptionList()
       // presDetails()
        registerCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true

    }
    
   
    
//    func presDetails(){
//        let params = NSMutableDictionary()
//        let finalUrl = Constants.API.base_URL + Constants.API.prescription_view_by_appointment_id +  "\(223)/"
//        print(finalUrl)
//        WebApiCallBack.shared.GETServiceHeader(serviceType:finalUrl , parameters: params as! [String : Any], token: loginModel.token) { (response) -> (Void) in
//            print(response)
//
//            if response != nil{
//
//                if let response = JSON(response).dictionary{
//
//                    if let results = response["results"]?.array{
//
//                        for result in results{
//
//                            let id = result["id"].int
//
//                            print(id)
//                        }
//                    }
//                }
//            }
//        }
//    }
    func prescriptionList(){
        let params = NSMutableDictionary()
        let baseUrl = "y08_eprescription/prescription_view_by_user_id/"
        let finalUrl = Constants.API.base_URL + baseUrl + "\(loginModel.userid)/"
        print(finalUrl)
        
        
            let url = URL(string: finalUrl)
            if let usableUrl = url {
                var request = URLRequest(url: usableUrl)
                request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
                let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data {
                        if let resposeDictionary = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
                            
                            print(resposeDictionary)
                            
                            if let response = JSON(resposeDictionary).dictionary{
                                
                                if let results = response["results"]?.array{
                                    var prescriptionsData = PrescriptionResultModel()
                                    
                                    
                                    for result in results{
                                        prescriptionsData.date_issued = result["date_issued"].string
                                        let prescription = result["prescription"].dictionary
                                        prescriptionsData.totalAmount = prescription!["bill_amount"]?.string
                                        prescriptionsData.appointmentId = result["appointment"].int
                                        PrescriptionResultModel.appointmentID = prescriptionsData.appointmentId!
                                   //     print(PrescriptionResultModel.appointmentID)
                                        
                                        prescriptionsData.prescriptionID = prescription!["prescriptionID"]?.string
                                        
                                        var productDetailsData = ProductDetailsModel()
                                        let productDetails = prescription!["product_detail"]?.array
                                        
                                        for productData in productDetails!{
                                            
                                            productDetailsData.product_name = productData["product_name"].string
                                            
                                            print(productDetailsData.product_name)
                                            productDetailsData.total_price = productData["total_price"].string
                                            
                                            productDetailsData.cost_per_unit = productData["cost_per_unit"].string
                                            productDetailsData.count = productData["count"].int
                                               print(productDetailsData.count)
                                            let Name = productDetailsData.product_name!+"x" + "\(productDetailsData.count!) "  
                                         
                                           print(Name)
                                            
                                            prescriptionsData.productDetails.append(Name)
                                            print(prescriptionsData.productDetails)
                                           // self.productResultArr.append(productDetailsData)
                                            
                                        }
                                        var finalName:String = ""
                                        for  i in 0..<prescriptionsData.productDetails.count {
                                            
                                           
                                        
  //self.finalName = ""
                                            finalName += prescriptionsData.productDetails[i]
   
                                        }
                                        self.twoStrings.append(finalName)
                                        prescriptionsData.productDetails.removeAll()

                                        print(finalName)
                                        
                                        let first4 = prescriptionsData.date_issued!.prefix(4)
                                        
                                        let startIndex = prescriptionsData.date_issued!.index(prescriptionsData.date_issued!.startIndex, offsetBy: 5)
                                        let endIndex = prescriptionsData.date_issued!.index(prescriptionsData.date_issued!.startIndex, offsetBy: 6)
                                        let monthtrim = String(prescriptionsData.date_issued![startIndex...endIndex])
                                        let monthNameGet = DateFormatter().monthSymbols[Int(monthtrim)! - 1]
                                        
                                        
                                        let one = prescriptionsData.date_issued!.index(prescriptionsData.date_issued!.startIndex, offsetBy: 8)
                                        let two = prescriptionsData.date_issued!.index(prescriptionsData.date_issued!.startIndex, offsetBy: 9)
                                        let dates = String(prescriptionsData.date_issued![one...two])
                                        
                                        prescriptionsData.issuedDate = dates + " " +  monthNameGet + " " + first4
                                        
                                     //   print(productDetailsData.product_name)
                                        
                                        self.prescriptionResultArr.append(prescriptionsData)
                                       // self.productResultArr.append(productDetailsData)
                                  
                                        
                                    }
                                   // self.twoStrings.append(self.finalName)
                                        print(self.twoStrings)
                        
                                }
                                
                            }
                            DispatchQueue.main.async {
                                self.prescriptionTV.reloadData()
                            }
                           
                        }
                       
                    }
                    
                })
                task.resume()
               
            }
    }
  
    @IBAction func navBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)

    }
    func registerCell(){
        
        let E_PrescriptionCell = UINib(nibName: "E_PrescriptionTVC", bundle: nil)
        prescriptionTV.register(E_PrescriptionCell, forCellReuseIdentifier: "E_PrescriptionTVC")
        prescriptionTV.showsVerticalScrollIndicator = false
        prescriptionTV.separatorStyle = .none
        
    }
    

}

extension E_PrescriptionVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return prescriptionResultArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"E_PrescriptionTVC") as! E_PrescriptionTVC
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
      //  cell.viewBtn.addTarget(self, action: #selector(navTo), for: .touchUpInside)
        cell.prescriptionIdLbl.text = prescriptionResultArr[indexPath.row].prescriptionID
        
        //cell.productNameLbl.text = self.twoStrings[indexPath.row]
        cell.amountLbl.text = prescriptionResultArr[indexPath.row].totalAmount
        
      
        cell.dateLbl.text = prescriptionResultArr[indexPath.row].issuedDate
      //  cell.viewBtn.tag = indexPath.row
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let navTo = storyboard?.instantiateViewController(withIdentifier:"PrescriptionDetailsDataVC") as! PrescriptionDetailsDataVC
         PrescriptionResultModel.appointmentID = prescriptionResultArr[indexPath.row].appointmentId
        navigationController?.pushViewController(navTo, animated: true)
       // present(navTo, animated: false, completion: nil)
    }
    
    @objc func navTo(_ sender:UIButton){
        
        let navTo = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.prescriptionDetailsDataVC) as! PrescriptionDetailsDataVC
        
      PrescriptionResultModel.appointmentID = prescriptionResultArr[sender.tag].appointmentId
       present(navTo, animated: false, completion: nil)
        
    }
    
    
}


struct PrescriptionResultModel{
    
    var prescriptionID:String?
    var totalAmount:String?
    var date_issued:String?
    var appointmentId:Int?
    var productDetails:[String] = []
    var issuedDate:String?
    static var presId = String ()
    static var appointmentID:Int?
    static var totalAmount = String()
   
}

struct ProductDetailsModel{
   
    var count:Int?
    var product_name:String?

    var brand:String?
    var type:String?
    var cost_per_unit:String?
    var total_price:String?
   
    var morning:Bool?
    var noon:Bool?
    var evening:Bool?
    var b_food:Bool?
    var a_food:Bool?
    var is_delete:Bool?
}
