//
//  PersonalProfileVC.swift
//  Yoopha
//
//  Created by Yoopha on 04/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PersonalProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var profilePickCV: UICollectionView!
    @IBOutlet weak var nameTF: CustomTF!
    @IBOutlet weak var mobileNumberTF: CustomTF!
    var selectedImage:UIImage?
    var count = String()
    var nextt = String()
    var previous = String()
    var selectedCountryIndex : Int = 0
    var selectedStateIndex :Int = 0
    var selectedDistrictIndex : Int = 0
    var TFImagesArray = [#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon"),#imageLiteral(resourceName: "down-icon")]
    var TFNames:[DropDownTextField] = []
    @IBOutlet weak var profileImage: CustomImageView!
    @IBOutlet weak var cameraBtn: UIButton!
    let picker = UIImagePickerController()
    var profileImageData:String = ""

    @IBOutlet weak var locationTF: CustomTF!
    @IBOutlet weak var streetTF: CustomTF!
    @IBOutlet weak var doorNumberTF: CustomTF!
    @IBOutlet weak var pincodeTF: CustomTF!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        registerCell()
        mobileNumberTF.text = SignUpModel.mobileNumber
        
        genderTF.datasource = ["Male","Female"]
      //  countryTF.datasource = ["1","2","3","4","5","6"]
        TFNames = [genderTF,countryTF,stateTF,districtTF]
        assignImagesToTF()
        getCountry()
        getStates()
        getDistricts()
    }
    func registerCell(){
        
        let profileImage = UINib(nibName: "UploadImageCVC", bundle: nil)
        profilePickCV.register(profileImage, forCellWithReuseIdentifier: "UploadImageCVC")
    }
    @IBAction func tappedOnSubmit(_ sender: Any) {
        
        if nameTF.text!.isEmpty || mobileNumberTF.text!.isEmpty || genderTF.text!.isEmpty || doorNumberTF.text!.isEmpty || streetTF.text!.isEmpty || locationTF.text!.isEmpty || doorNumberTF.text!.isEmpty || countryTF.text!.isEmpty || stateTF.text!.isEmpty || districtTF.text!.isEmpty == true{
            
            if loginModel.userid != loginModel.userid{
                 simpleAlert(message: "Please Fill Your Details")
            }else{
                simpleAlert(message: "User Already Exist With YOOPHA ,fill your patient profile")
            }
            
        }
       
        if genderTF.text == "Male"{
            genderTF.text = "M"
        }else if genderTF.text == "Female"{
            genderTF.text = "F"
        }
        if countryTF.text == "India"{
            countryTF.text = "1"
            print(countryTF.text!)
        }
        if stateTF.text == stateTF.text{
            stateTF.text = String(selectedStateIndex)
            print(stateTF.text!)
        }
        if districtTF.text == districtTF.text{
            districtTF.text = String(selectedDistrictIndex)
            print(districtTF.text!)
        }
        
         createProfile()
        if loginModel.userid == loginModel.userid{
            
//            alertWithMessage(title: "User Already Existed With YOOPHA", message: "User Already Existed With YOOPHA ,fill your patient profile", cancelButtonTitle: "Cancel", buttonTitle: "Create", handler: { (action) in
//
//                let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.patientProfileVC) as! PatientProfileVC
//
//
//                self.navigationController?.pushViewController(navTo, animated: true)
//
//            }) { (action) in
//                self.resignFirstResponder()
//            }
            //simpleAlert(message: "User Already Exist With YOOPHA ,fill your patient profile")
        }
    }
    
    func createProfile(){
        
        let params:[String:Any] =
            ["name": nameTF.text as Any,
            "emergency_contact": mobileNumberTF.text as Any,
            "gender": genderTF.text as Any,
            "address": [
            "address_type":1,
            "address_line1": doorNumberTF.text as Any,
            "address_line2":streetTF.text as Any,
            "address_line3":locationTF.text as Any,
            "pincode":pincodeTF.text as Any,
            "district": districtTF.text as Any,
            "state": stateTF.text as Any,
            "country": 1 as Any,
            "long":"098.09876",
            "lat":"098.67890"
            ],
            "user": loginModel.userid  as Any,
           // "image":profileImageData
                "image":[]
        ]
       
       print(loginModel.userid)
        
            WebApiCallBack.shared.POSTServiceHeader(serviceType: "\(Constants.API.base_URL)\(Constants.API.createConsumer)", parameters:  params , token:loginModel.token) { (response) -> (Void) in
            
                print(loginModel.token)
                loginModel.createProfileStatus = true
                print(response)
                if response != nil{
                    
                 //   var userProfileData = ProfileModel()
                    
                  //  ProfileModel.name = response["name"].string!
                    //ProfileModel.mobile = response["emergency_contact"].string!
                }
        }
        
        

    }
    
    @IBOutlet weak var genderTF: DropDownTextField!{
        didSet{ genderTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(genderDropDown)))
        }
    }
    @objc func genderDropDown(){
        genderTF.select(genderTF)
        genderTF.dropDown.selectionAction = { (index,item) in
        self.genderTF.text = item
        self.genderTF.resignFirstResponder()
        }
    }
    @objc func countryTFDropDown(){
        countryTF.select(countryTF)
        countryTF.dropDown.selectionAction = { (index,item) in
            self.countryTF.text = item
            self.countryTF.resignFirstResponder()
            self.selectedCountryIndex = index + 1
            print(self.selectedCountryIndex as Any)
            self.getStates()
            print("Index" ,index)
        }
    }
    @objc func stateTFDropDown(){
        stateTF.select(stateTF)
        stateTF.dropDown.selectionAction = { (index,item) in
            self.stateTF.text = item
            self.stateTF.resignFirstResponder()
            self.selectedStateIndex = index + 1
            print(self.selectedStateIndex as Any)
            self.getDistricts()
             print("Index" ,index)
        }
    }
    @objc func districtTFDropDown(){
        districtTF.select(districtTF)
        districtTF.dropDown.selectionAction = { (index,item) in
            self.districtTF.text = item
            self.districtTF.resignFirstResponder()
            self.selectedDistrictIndex = index + 1
            print(self.selectedDistrictIndex as Any)
             print("Index" ,index)
            
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == stateTF{
            getStates()
        }
        else if textField == districtTF{
            getDistricts()
        }
    }
    @IBOutlet weak var countryTF: DropDownTextField!{
        didSet{ countryTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(countryTFDropDown)))
        }
    }
    @IBOutlet weak var stateTF: DropDownTextField!{
        didSet{ stateTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(stateTFDropDown)))
    }
    }
    @IBOutlet weak var districtTF: DropDownTextField!{
        didSet{ districtTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(districtTFDropDown)))
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == stateTF{
            getStates()
        }
        else if textField == districtTF{
            getDistricts()
        }
    }
    
    func getStates() {
      
        WebApiCallBack.shared.GETServiceHeader(url: "\(Constants.API.base_URL)\(Constants.API.statesList)",token: loginModel.token) { (response) -> (Void) in
          //  print(response)
            if response != nil {
                let mresponse = JSON(response)
                if let serverMessage = mresponse.dictionary{
                    self.count = serverMessage["count"]?.stringValue ?? ""
                    self.nextt = serverMessage["next"]?.stringValue ?? ""
                    self.previous = serverMessage["previous"]?.stringValue ?? ""
                    if let results = serverMessage["results"]?.array{
                       // print(results)
                        var statesList = statesListModel()
                        var countryDetails = CountryDetails()
                        for result in results{
                        
                            let nameState = result["name"].string
                            statesList.name.append(nameState!)
                            self.stateTF.datasource = statesList.name

                            print(statesList.name)
                
                        }
                    }
                }
        
            }
        }
        
    }
    
    func getDistricts(){
        
      //  let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y01_common/district_list/24/"
        WebApiCallBack.shared.GETServiceHeader(url: "\(Constants.API.base_URL)\(Constants.API.districtsList)", token: loginModel.token) { (response) -> (Void) in
           // print(response)
            if response != nil {
                let mresponse = JSON(response)
                if let serverMessage = mresponse.dictionary{
                    self.count = serverMessage["count"]?.stringValue ?? ""
                    self.nextt = serverMessage["next"]?.stringValue ?? ""
                    self.previous = serverMessage["previous"]?.stringValue ?? ""
                    if let results = serverMessage["results"]?.array{
                       // print(results)
                        var districtsList = districtListModel()
                      //  var countryDetails = CountryDetails()
                        for result in results{
                            
                            let nameDistrict = result["name"].string
                            districtsList.name.append(nameDistrict!)
                            self.districtTF.datasource = districtsList.name
                            
                            print(districtsList.name)
                            
                        }
                    }
                }
                
            }
        }
        
    }
    func getCountry(){
    
        WebApiCallBack.shared.GETServiceHeader(url: "\(Constants.API.base_URL)\(Constants.API.countryList)",token: loginModel.token) { (response) -> (Void) in
        
            if response != nil {
                let mresponse = JSON(response)
                if let serverMessage = mresponse.dictionary{
                    self.count = serverMessage["count"]?.stringValue ?? ""
                    self.nextt = serverMessage["next"]?.stringValue ?? ""
                    self.previous = serverMessage["previous"]?.stringValue ?? ""
                    if let results = serverMessage["results"]?.array{
                        print(results)
                        var countryDetails = CountryDetails()
                        for result in results{
                            
                            let nameCountry = result["name"].string
                            countryDetails.name.append(nameCountry!)
                            self.countryTF.datasource = countryDetails.name as! [String]
                            
                            print(countryDetails.name)
                            
                        }
                    }
                }
                
            }
        }
        
    }
    @IBAction func tappedOnCameraBtn(_ sender: Any) {
        let alertController = UIAlertController(title: "Profile Picture", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
            
            //dismiss(animated: true, completion: nil)
        })
        
        let  deleteButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            let picker = UIImagePickerController()
            picker.delegate = self
            
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
            //dismiss(animated: true, completion: nil)
        })
        let  removePhoto = UIAlertAction(title: "Remove Photo", style: .default, handler: { (action) -> Void in
        
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(removePhoto)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        print(image)
        
        selectedImage = image
        profilePickCV.reloadData()
       // profileImage.clipsToBounds = true
        
        let imageData = image.jpegData(compressionQuality: 0.8)
        self.profileImageData = imageData!.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    func assignImagesToTF(){
        for j in 0..<TFNames.count{
            print(TFNames[j])
            let imageView = UIImageView(frame: CGRect(x: -5, y: 0, width: 20, height: 20))
            imageView.image = TFImagesArray[j]
            
            //imageView.tintColor = tintColor
            let view = UIView(frame : CGRect(x: -5, y: 5, width: 20, height: 20))
            view.addSubview(imageView)
            TFNames[j].rightViewMode = .always
            
            TFNames[j].rightView = view
            
        }
        
    }
    

}

extension PersonalProfileVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCVC", for: indexPath) as! UploadImageCVC
        cell.uploadImage.image = selectedImage
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCVC", for: indexPath) as! UploadImageCVC
        let navTo = storyboard?.instantiateViewController(withIdentifier: "DetailedVC") as! DetailedVC
        
       // navTo.image = UIImage(named:[indexPath.row])
        
        self.navigationController?.pushViewController(navTo, animated: true)
        
    }
    
    
}



//
//        "results": [
//        {
//        "id": 1,
//        "country": {
//        "id": 1,
//        "name": "India",
//        "code": "IND"
//        },
//        "name": "ANDHRA PRADESH",
//        "code": "AP"
//        },
struct statesListModel{
    var id : Int?
    var country: [CountryDetails] = []
    var name : [String] = []
    var code : String?
    init(){}
}
struct CountryDetails {
    var id:Int?
    var name:[String] = []
    var code:String?
}
struct districtListModel{
    var id:Int?
    var name:[String] = []
    var code:String?
}

struct ProfileModel{
    static var name = String()
    static var mobile = String()
   // init(){}
    
}
