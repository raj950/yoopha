//
//  SettingsVC.swift
//  Yoopha
//
//  Created by Yoopha on 12/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SideMenuSwift

class SettingsVC: UIViewController {

    @IBOutlet weak var navBack: UIButton!
    let content = Constants.ConstantStrings.settingsContent
    @IBOutlet weak var settingsTV: UITableView!
    var bgImage: UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func navBack(_ sender: Any) {
        
        
        let navToSettings = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.sideMenuController) as! SideMenuController
        present(navToSettings, animated: false, completion: nil)
    }
    
}

extension SettingsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.settingsTVC) as! SettingsTVC
            cell.contentLbl.text = content[indexPath.row]
        if indexPath.row == 0 || indexPath.row == 1{
            let switchView = UISwitch(frame: .zero)
            switchView.setOn(false, animated: true)
            switchView.tag = indexPath.row
            switchView.tintColor = UIColor(red: 39.0/255, green: 170.0/255, blue: 165/255, alpha: 1.0)
            switchView.onTintColor = switchView.tintColor
            switchView.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
            cell.accessoryView = switchView
        }
        
        
        if indexPath.row == 5{
           let view = UIView()
            let image: UIImage = UIImage(named:"log-out")!
            bgImage = UIImageView(image: image)
            bgImage!.frame = CGRect(x: 22,y: 13,width: 20,height: 20)
            view.addSubview(bgImage!)
            cell.addSubview(view)
            
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4{
            let navToChangePasswordVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.forgotPasswordVC) as! ForgotPasswordVC
            present(navToChangePasswordVC, animated: false, completion: nil)
        }
        if indexPath.row == 5{
            
            logOut()
            let navToLoginVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
            present(navToLoginVC, animated: false, completion: nil)
        }
    }
    
    func logOut(){
        WebApiCallBack.shared.GETServiceHeader(url: "\(Constants.API.base_URL)\(Constants.API.logOut)", token: loginModel.token) { (response) -> (Void) in
            print(response)
        }
    }
    @objc func switchChanged(_ sender : UISwitch!){
        
        print("table row switch Changed \(sender.tag)")
        print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }
}
