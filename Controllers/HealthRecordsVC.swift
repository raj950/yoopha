//
//  PopupView2Controller.swift
//  SwiftPopupInTabbar
//
//  Created by 한상준 on 14/04/2019.
//  Copyright © 2019 한상준. All rights reserved.
//

import UIKit

class HealthRecordsVC: UIViewController {

    @IBOutlet var popUpView: UIView!
    @IBAction func prescriptionBtn(_ sender: Any) {
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 30
    
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    @IBAction func downArrow(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    //navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func tappedOnPrescriptionBtn(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.E_PrescriptionVC) as! E_PrescriptionVC
        present(navTo, animated: false, completion: nil)
    }
    @IBAction func tappedOnLabResultBtn(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.labResultsVC) as! LabResultsVC
        present(navTo, animated: false, completion: nil)
        
    }
    @IBAction func tappedOnObservationBtn(_ sender: Any) {
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.observationsListVC) as! ObservationsListVC
        present(navTo, animated: false, completion: nil)
    }
}
