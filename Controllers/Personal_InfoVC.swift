//
//  Personal_Info.swift
//  Yoopha
//
//  Created by Yoopha on 01/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class Personal_InfoVC: UIViewController {

    @IBOutlet weak var profilePicture: UIImageView!
    
    let titleArr = ["Name","Email","Mobile Number","Change Password"]
    let descriptionArr = ["John Doe","johndoe@gmail.com","+91 " + loginModel.mobileNumber!,nil]
    
    override func viewDidLoad() {
        super.viewDidLoad()

      profilePicture.layer.cornerRadius = 65
    }
    

}

extension Personal_InfoVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.personal_InfoTVC) as! Personal_InfoTVC
        cell.titleLbl.text = titleArr[indexPath.row]
        cell.descriptionLbl.text = descriptionArr[indexPath.row]
        return cell
    }
    
    
}
