//
//  OrderVC.swift
//  Yoopha
//
//  Created by Yoopha on 11/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class OrderVC: UIViewController {

    @IBOutlet weak var orderTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        registerCell()

    }
    
    
    func registerCell(){
        
        let orderTVC = UINib(nibName: Constants.Cell.orderCell, bundle: nil)
        orderTV.register(orderTVC, forCellReuseIdentifier: Constants.Cell.orderCell)
    }
}


extension OrderVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.orderCell) as! OrderCell
        return cell
    }
    
    
}
