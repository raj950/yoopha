//
//  BookAppointmentVC.swift
//  Yoopha
//
//  Created by Yoopha on 24/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON
import Toast_Swift
import SideMenuSwift



class BookAppointmentVC: UIViewController,UITextFieldDelegate {
  
   
    let picker = UIPickerView()
    let datepicker=UIDatePicker()
    let timePicker=UIDatePicker()
    let finalFormatter = DateFormatter()
    let currentDate = Date()
    let toolBar = UIToolbar()
    var selectedTF = UITextField()
    var selectedDay:String?
    
    @IBOutlet weak var confirmBookingBtn: UIButton!

    @IBOutlet weak var newPatientBtn: UIButton!
    @IBOutlet weak var drEducationLbl: UILabel!
  //  @IBOutlet weak var eveningBtn: UIButton!
  //  @IBOutlet weak var noonBtn: UIButton!
  //  @IBOutlet weak var morningBtn: UIButton!
    @IBOutlet weak var selectDateTF: DropDownTextField!
    @IBOutlet weak var doctorProfileImage: CustomImageView!
    weak var accessoryDelegate: AccessoryToolbarDelegate?
    var datePicker : UIDatePicker!
    var dateFinalValue : String = ""
    let params = NSMutableDictionary()
    
    @IBOutlet weak var clinicTF: DropDownTextField!{
        didSet{clinicTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clinicTFDropDown)))}
     }
    @IBOutlet weak var expLbl: UILabel!
  //  @IBOutlet weak var feeLbl: UILabel!
    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var inputTextView: UITextView!
  
    
    @IBOutlet weak var selectTimeSlotTF: DropDownTextField!{
         didSet{selectTimeSlotTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(timeSlotTFDropDown)))}
    }
    
    @IBOutlet weak var selectPatientTF: DropDownTextField! {
     didSet{selectPatientTF.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPatientDropDown)))}
     }
    
  
    var getPatientDeatils = patientsList()
    @IBOutlet weak var symptomsTF: UITextField!
   

    static let bookAppointmentVC = BookAppointmentVC()
 
    var symptomsArray = [String]()
    var filteredSympArray = [String]()
    var symptoms = [String]()
    var since = [String]()
    var selectedIndex = Int ()
    var TFNames:[DropDownTextField] = []
    var TFImagesArray = [#imageLiteral(resourceName: "sort-down"),#imageLiteral(resourceName: "sort-down"),#imageLiteral(resourceName: "sort-down"),#imageLiteral(resourceName: "sort-down")]
    var count = String()
    var nextt = String()
    var previous = String()
    var patientsNames:[patientsList] = []
    var patients = patientsList()
    var dayNameArry = [String]()
    var monthNameArry = [String]()
    var datesArry = [Int]()
    var monthArray = [Int]()
    var offsetCount = 0
    var aliment:[symptomsListModel] = []
    var ailmentList = symptomsListModel()
    let dateFormatter = DateFormatter()
    var doctorName:String?
    var doctorSpeciality:String?
    var doctorExp:String?
    var doctorFee:String?
    var doctorId:Int?
    var clinicSlot:Int?
    var clinicArray = [String]()
    let cal = Calendar.current
    let date = Date()
    var clinicslots:[String] = []
    var clinicTimes:[String] = []
    var clinicStart:String?
    var clinicEnd:String?
    var clinicDay:String?
    var clinicOpens:String?
    var schedule:String?
    var clinicSlotIdM:Int?
    var currentYear:Int = 0
    var month:Int = 0
    var doctorExperience:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        print(doctorName)
         print(doctorSpeciality)
        
        print(DRInfo.getClinicData)
        nameLbl.text =  DRInfo.doctorName
        
         self.doctorExperience = self.date.GetYear() - DRInfo.doctorExp!
        expLbl.text = "\(doctorExperience!)" + " " + "+ years experience"
       // feeLbl.text = Constants.ConstantStrings.rupee + " " + DRInfo.doctorFee!
        specialityLbl.text = DRInfo.doctorSpeciality! + " | " + DRInfo.doctorEducation!
        doctorId = DRInfo.id
        
        //selectTimeSlotTF.datasource = ["Morn-","Female","Sister","Brother"]
        createPicker()
        getTodayDate()

        newPatientBtn.underline()
        
        
        
   //     print(DRInfo.doctorProfileImage)
        let url = URL(string: DRInfo.doctorProfileImage ?? Constants.ConstantStrings.defaultImage)
        doctorProfileImage.kf.setImage(with: url)
      
        TFNames = [selectPatientTF,clinicTF,selectDateTF,selectTimeSlotTF]
        assignImagesToTF()

        
        
        selectDateTF.delegate = self
        
        self.inputTextView.delegate = self as UITextViewDelegate
        self.inputTextView.layer.borderWidth = 1.0
        
        getPatientsList()
        getClinic()
        getSlot()
    
       
    }
    
    func getClinic(){
        for i in 0..<DRInfo.getClinicData.count{
            
            if clinicslots.contains(where: {$0 == DRInfo.getClinicData[i].clinicData[0].clinic_name! + " " + DRInfo.getClinicData[i].clinicData[0].clinic_address[0].address_line3! }) {
                
                
            }
            else{
                clinicslots.append(DRInfo.getClinicData[i].clinicData[0].clinic_name! + " " + DRInfo.getClinicData[i].clinicData[0].clinic_address[0].address_line3!)
                
            }
            
            
        }
        print(clinicslots)
       
        clinicTF.datasource = clinicslots
        
    }
    func getSlot(){
        for i in 0..<DRInfo.getClinicData.count{
            
            if clinicslots.contains(where: {$0 == DRInfo.getClinicData[i].slot[0].start_time! + " " + DRInfo.getClinicData[i].slot[0].end_time!}) {
                
                
            }
            else{
                clinicTimes.append(DRInfo.getClinicData[i].slot[0].start_time! + " " + DRInfo.getClinicData[i].slot[0].end_time!)
                
            }
            
            
        }
        print(clinicTimes)
        
        selectTimeSlotTF.datasource = clinicTimes
        
    }
    //MAKR : Select Date From Date Picker
    func createPicker(){
   
        datepicker.datePickerMode = .date
        datepicker.setDate(Date(), animated: true)
        timePicker.setDate(Date(), animated: true)
        timePicker.minuteInterval = 15
        
        let pickerView = picker
        pickerView.backgroundColor = .white
        pickerView.showsSelectionIndicator = true
    
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action:#selector(donePicker))
        doneButton.tintColor = UIColor.white
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action:#selector(canclePicker))
        cancelButton.tintColor = UIColor.white
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.barTintColor = UIColor.appGreenColor
        
        let calendar = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian);
        let components = calendar?.components([NSCalendar.Unit.year,NSCalendar.Unit.month,NSCalendar.Unit.day,NSCalendar.Unit.hour,NSCalendar.Unit.minute], from: currentDate )
        
        let maximumYear = (components?.year)! - 150
        let maximumMonth = (components?.month)! - 1
        let maximumDay = (components?.day)! - 1
        
        let comps = NSDateComponents();
        comps.year = +maximumYear
        comps.month = +maximumMonth
        comps.day = +maximumDay
        let maxDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        datepicker.maximumDate = maxDate
        datepicker.minimumDate = datepicker.date
        self.selectDateTF.inputView = datepicker
        self.selectDateTF.inputAccessoryView = toolBar
 
    }
    
    
    @objc func donePicker() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFormatter2 = DateFormatter()

        dateFormatter2.dateFormat = "EEEE"

        let dayOfTheWeekString = dateFormatter2.string(from: datepicker.date)
        print(dayOfTheWeekString.uppercased().prefix(3))
        let day = dayOfTheWeekString.uppercased()
        
     let dayName = day.prefix(3)
        
     //   self.noonBtn.setTitle("-", for: .normal)
      //  self.morningBtn.setTitle("-", for: .normal)
       // self.eveningBtn.setTitle("-", for: .normal)
        
        for i in 0..<DRInfo.getClinicData.count{
            if DRInfo.getClinicData[i].clinicData.contains (where: { $0.clinic_name == clinicTF.text }){
              
                print(DRInfo.getClinicData[i])
                print(dayName)
                if DRInfo.getClinicData[i].slot[0].day == "\(dayName)"{
                    
                    self.clinicStart = DRInfo.getClinicData[i].slot[0].start_time
                    
                    self.clinicEnd = DRInfo.getClinicData[i].slot[0].end_time
                    let end = self.clinicEnd?.timeConversion12(time24: self.clinicEnd!)
                    var clinicEndTime:String?
                    
                    let start = self.clinicStart?.timeConversion12(time24: self.clinicStart!)
                    var clinicStartTime:String?
                    
                    
                    clinicStartTime = start?.stringTrims(trim: start!)
                    clinicEndTime = end?.stringTrims(trim: end!)
                    
                    self.clinicOpens = clinicStartTime! + "-" + clinicEndTime!
                    
                    print(clinicOpens)
                    
                 //   self.selectTimeSlotTF.datasource = [clinicOpens] as! [String]
//                    if DRInfo.getClinicData[i].slot[0].schedule == "MORN"{
//                        clinicSlotIdM = DRInfo.getClinicData[i].id
//                        self.morningBtn.setTitle(self.clinicOpens, for: .normal)
//
//                    } else if DRInfo.getClinicData[i].slot[0].schedule == "NOON"{
//                         clinicSlotIdM = DRInfo.getClinicData[i].id
//                      self.noonBtn.setTitle(self.clinicOpens, for: .normal)
//                    }else if DRInfo.getClinicData[i].slot[0].schedule == "EVEN"{
//                         clinicSlotIdM = DRInfo.getClinicData[i].id
//                        //clinicSlotIdM = DRInfo.getClinicData[i].slot[0].id
//                        self.eveningBtn.setTitle(self.clinicOpens, for: .normal)
//
//                    }else {
//                       // self.view.makeToast("Slot Not Available456",duration:3.0,position:.center)
//                    }
                }
                else{

//                    self.noonBtn.setTitle("-", for: .normal)
//                    self.morningBtn.setTitle("-", for: .normal)
//                    self.eveningBtn.setTitle("-", for: .normal)
//                    //simpleAlert(message: "slot not avaliable")
//                    self.view.makeToast("Slot Not Available",duration:3.0,position:.center)
                }
            }
            

            
        }
      //  print(self.clinicDay)
        //print(self.schedule)
  
        self.selectDateTF.text = dateFormatter.string(from: datepicker.date)
           self.selectDateTF.resignFirstResponder()
        
    }
    
    @IBAction func tappedOnAddPatientBtn(_ sender: Any) {
        
        let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.profileVC) as! ProfileVC
        self.navigationController?.pushViewController(navTo, animated: true)
        
        
    }
    @objc func canclePicker() {
        
        self.selectDateTF.resignFirstResponder()
        
    }
    
    
    @objc func selectPatientDropDown(){
        selectPatientTF.select(selectPatientTF)
                selectPatientTF.dropDown.selectionAction = { (index,item) in
                    self.selectPatientTF.text = item
                    
                    loginModel.id = self.patientsNames[index].id[index]
                
                    self.selectPatientTF.resignFirstResponder()
                    
                   
        }
    }
    
    @objc func timeSlotTFDropDown(){
        selectTimeSlotTF.select(selectTimeSlotTF)
        selectTimeSlotTF.dropDown.selectionAction = { (index,item) in
            self.selectTimeSlotTF.text = item
            
            self.selectTimeSlotTF.text = "\(DRInfo.getClinicData[index].slot[0].id!)"
           // self.selectTimeSlotTF.text = DRInfo.getClinicData[index].slot[0].schedule
        //loginModel.id = self.patientsNames[index].id[index]
            
            self.selectTimeSlotTF.resignFirstResponder()
            
            
        }
    }
   
//    @IBAction func tappedOnMorningBtn(_ sender: Any) {
//
//        print(clinicSlotIdM)
//
//        if morningBtn.backgroundColor == UIColor.white{
//            morningBtn.backgroundColor = UIColor.appGreenColor
//            morningBtn.setTitleColor(UIColor.white, for: .normal)
//
//
//        }else{
//            morningBtn.backgroundColor = UIColor.white
//            morningBtn.setTitleColor(UIColor.appGreenColor, for: .normal)
//        }
//    }
//    @IBAction func tappedOnNoonBtn(_ sender: Any) {
//         print(clinicSlotIdM)
//        if noonBtn.backgroundColor == UIColor.white{
//            noonBtn.backgroundColor = UIColor.appGreenColor
//            noonBtn.setTitleColor(UIColor.white, for: .normal)
//        }else{
//            noonBtn.backgroundColor = UIColor.white
//            noonBtn.setTitleColor(UIColor.appGreenColor, for: .normal)
//        }
//
//    }
//    @IBAction func tappedOnEveningBtn(_ sender: Any) {
//         print(clinicSlotIdM)
//        if eveningBtn.backgroundColor == UIColor.white{
//            eveningBtn.backgroundColor = UIColor.appGreenColor
//            eveningBtn.setTitleColor(UIColor.white, for: .normal)
//        }else{
//            eveningBtn.backgroundColor = UIColor.white
//            eveningBtn.setTitleColor(UIColor.appGreenColor, for: .normal)
//        }
//    }
//
//
    @objc func clinicTFDropDown(){
        clinicTF.select(clinicTF)
        clinicTF.dropDown.selectionAction = {(index,item) in
            
          //  print(DRInfo.getClinicData[index].id)
        //    print(DRInfo.getClinicData[index].slot)
            
            self.clinicDay = DRInfo.getClinicData[index].slot[0].day
            self.schedule = DRInfo.getClinicData[index].slot[0].schedule


//            self.eveningBtn.setTitle("-", for: .normal)
//            self.noonBtn.setTitle("-", for: .normal)
//            self.morningBtn.setTitle("-", for: .normal)
            
            self.clinicTF.text = item
            self.clinicTF.resignFirstResponder()
        }
    }
   
    
    func assignImagesToTF(){
        for j in 0..<TFNames.count{
            print(TFNames[j])
            let imageView = UIImageView(frame: CGRect(x: -15, y: 0, width: 20, height: 20))
            imageView.image = TFImagesArray[j]
            let view = UIView(frame : CGRect(x: -15, y: 5, width: 20, height: 20))
            view.addSubview(imageView)
            TFNames[j].rightViewMode = .always
            
            TFNames[j].rightView = view
            
        }
        
    }

    func getPatientsList(){
     
      //  let url1 = "\(Constants.API.base_URL)\(Constants.API.patientList)" + String(loginModel.userid) + Constants.ConstantStrings.slash
        let url1 = "\(Constants.API.base_URL)\(Constants.API.get_user_patientdetails)" + "\(loginModel.userid)"  + "/"
      //  let url11 = "https://testapi.yoopha.com/api/v1/y05_consumer/get_user_patientdetails/6/"
        print(url1)
        
            let url = URL(string: url1)
            if let usableUrl = url {
                var request = URLRequest(url: usableUrl)
                request.setValue("JWT " + loginModel.token,forHTTPHeaderField: "Authorization")
                let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data {
                        if let response = try! JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any]{
                            print(response)
                            if response != nil{
                                if let myResponse = JSON(response).dictionary{
                                    if let result = myResponse["results"]?.array{
                                        var getPatientDeatils = patientsList()
                                        for results in result{
                                            getPatientDeatils.name.append(results["name"].string!)
                                            getPatientDeatils.id.append(results["id"].int!)
                                            getPatientDeatils.userId.append(results["user"].int!)
                                            self.patientsNames.append(getPatientDeatils)
                                        }
                                        self.selectPatientTF.datasource = getPatientDeatils.name
                                    }
                                    print(self.patients.name)
                                }
                                
                            }
                           
                        }
                    }
                })
                task.resume()
            }
    }
    func createAppointment(){
        let params:[String:Any] = [
            "doctor": doctorId! as Any,
            "clinic_slot": selectTimeSlotTF as Any,
            "booking_date": selectDateTF!.text as Any,
            "patient": loginModel.id as Any,
            "symptoms": symptomsTF.text! as Any,
            "allergy": [],
            "description": inputTextView.text as Any,
            "appointment_status": 1
        ]
        
        
        print(doctorId!)
        print(loginModel.patientId)
        print(loginModel.appointmentDate as Any)
        
        print(params)
        WebApiCallBack.shared.POSTServiceHeader(serviceType: "\(Constants.API.base_URL)\(Constants.API.doctorAppointment)", parameters: params, token: loginModel.token) { (response) -> (Void) in
            
            print(response)
            
            if response != nil{
                if let myResponse = JSON(response).dictionary{
                    
                 
                     let resp = myResponse["appointment_status"]?.int
                 
                    
                    if resp == 1{
                        
                        if AlertView.instance.img.isHidden == true
                        {
                            AlertView.instance.alertTitleConstraint.constant = 50
                            AlertView.instance.alertBtnConstraint.constant = 250
                            AlertView.instance.tokenLblConstraint.constant = 100
                            
                        }
                        
                        self.getPatientDeatils.appointmentTokenNumber = myResponse["token_no"]?.int!
                        self.getPatientDeatils.patientAppointmentId = myResponse["patient"]?.int
                        
                        loginModel.appointmentPatientId = self.getPatientDeatils.patientAppointmentId
                        
                        AlertView.instance.showAlert(title: "SUCCESS", message: "Appointment Created Successfully", alertType: .success)
                        AlertView.instance.tokenLbl.text =  "Your Token Number : " + "\(self.getPatientDeatils.appointmentTokenNumber!)"
                        AlertView.instance.doneBtn.addTarget(self, action: #selector(self.navTo), for: .touchUpInside)
                        
                    }
                   
                }else{
                      AlertView.instance.showAlert(title: "FAILED", message: "Try Again", alertType: .failure)
                }
                
            }
            
        }
    }
    
    @objc func navTo(){
        
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.mainMenuTBC) as! MainMenuTBC
      
        self.navigationController?.present(navTo, animated:false, completion: nil)
       
    }

    @IBAction func navBack(_ sender: Any) {
        
        let navBack = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.doctorDetailedVC) as! DoctorDetailedVC
        self.navigationController?.popViewController(animated: true)
       
    }
    
    // MARK:Confirm Booking
    @IBAction func tappedOnBooking(_ sender: Any) {
      
        AlertView.instance.img.isHidden = true
        if selectPatientTF.text?.isEmpty == true || clinicTF.text?.isEmpty == true || symptomsTF.text?.isEmpty == true || inputTextView.text.isEmpty == true{
            simpleAlert(message: "Please check your details")
        }
        else if clinicSlotIdM == nil{
            alertWithMessage(title: "Confirm Booking", message: "Are you sure? Do you want to confirm this booking", cancelButtonTitle: "Cancel", buttonTitle: "Confirm", handler: { (action) in
                
                self.createAppointment()
                let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.appointmentStatusVC) as! AppointmentStatusVC
                
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(navTo, animated: true)
                
            }) { (action) in
                self.resignFirstResponder()
            }
       }
    }
        
    
    }
extension BookAppointmentVC:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        inputTextView.layer.borderColor = UIColor.gray.cgColor
        let textContent = "Type in your notes to doctor…"
       
        if textView.text == textContent {
            textView.text = ""
            textView.textColor = UIColor.black
            textView.font = UIFont(name: poppinsFont, size: 14.0)
            inputTextView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        inputTextView.layer.borderColor = UIColor.clear.cgColor
    }

   
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            textView.resignFirstResponder()
        }
        return true
    }
}



struct symptomsListModel{
    
    var id:Int?
    var ailment_name:[String] = []
    
    init() {}
    
}

struct patientsList {
    var name:[String] = []
    var id:[Int] = []
    var patientId:Int?
    var appointmentTokenNumber:Int?
    var patientAppointmentId:Int?
    var appointmentId:String?
    var userId:[Int] = []
    var usersArray:[String] = []
    
    var patientToken:Int?
    var patientSlug:String?
    var doctorName:String?
    var doctorSpeciality:String?
    var bookingDate:String?
    var startTime:String?
    var endTime:String?
    var doctorEducation:String?
    var doctroProfile:String?
    var symptoms:String?
    var drExp:Int?
    
    var clinicName:String?
    var aptStatus:String?
    var clinicAddress:String?
    var doctorID:Int?
    
}




//    func getSymptomList(offsetValue:Int){
//
//        let url = "http://ec2-13-234-161-173.ap-south-1.compute.amazonaws.com/api/v1/y01_common/search_ailments/?limit=100&offset=\(offsetValue)"
//        WebApiCallBack.shared.GETService(serviceType: url, parameters: params as! [String : String]) { (response) -> (Void) in
//
//            print(response)
//            if response != nil{
//                if let myResponse = JSON(response).dictionary{
//                    self.count = myResponse["count"]!.stringValue
//                    self.nextt = myResponse["next"]!.stringValue
//                    self.previous = myResponse["previous"]!.stringValue
//                    if let result = myResponse["results"]?.array{
//
//                        for results in result{
//
//                            let ailment = results["ailment_name"].string
//                            self.ailmentList.ailment_name.append(ailment!)
//
//                            self.aliment.append(self.ailmentList)
//                        }
//
//                        print(self.aliment.count)
//
//
//                    }
//                }
//            }
//
//
//        }
//    }
