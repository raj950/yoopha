//
//  SearchMedicineVC.swift
//  Yoopha
//
//  Created by Yoopha on 12/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit

class SearchMedicineVC: UIViewController {

    @IBOutlet weak var prescriptionImage: UIImageView!
    @IBOutlet weak var prescriptionBtn: UIButton!
    @IBOutlet weak var searchMedicineTV: UITableView!
    @IBOutlet weak var cronicTF: DropDownTextField!
    var profileImageData:String = ""

     var currentTitle:String?
     var cronic = [String]()
     let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
       
    }
    

    @IBAction func tappedOnAddCronic(_ sender: UIButton) {
        if cronicTF.text!.isEmpty == true{
            alertWithTitle(title: "Alert", message: "Please provide your Cronic", buttonTitle: "OK")

        }else{
            cronic.insert(cronicTF.text!, at: 0)
            searchMedicineTV.insertRows(at: [IndexPath(row: 0, section: 0)], with: .right )
        }
        
       
    }
    
    @IBAction func tappedOnDelete(_ sender: Any) {
    }
    @objc func deleteRow(_ sender:UIButton){
        
        let deleteCell = sender.convert(CGPoint.zero, to: searchMedicineTV)
        guard let indexPath = searchMedicineTV.indexPathForRow(at: deleteCell) else {
            return
        }
        cronic.remove(at: indexPath.row)
        searchMedicineTV.deleteRows(at: [indexPath], with:.left)
        
    }
   
    @IBAction func tappedOnUploadPrescriptionBtn(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Profile Picture", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
            
            //dismiss(animated: true, completion: nil)
        })
        
        let  deleteButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            let picker = UIImagePickerController()
            picker.delegate = self
            
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
            //dismiss(animated: true, completion: nil)
            
            
        })
        let  removePhoto = UIAlertAction(title: "Remove Photo", style: .default, handler: { (action) -> Void in
            
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(removePhoto)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        print(image)
        prescriptionImage.image = image
        prescriptionImage.clipsToBounds = true
        
        let imageData = image.jpegData(compressionQuality: 0.8)
        self.profileImageData = imageData!.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}

extension SearchMedicineVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cronic.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.searchMedicineTVC) as! SearchMedicineTVC
        self.currentTitle  = cronic[indexPath.row]
        print(self.currentTitle)
        
      //  cell.cronicBtn.titleLabel?.text = currentTitle
        cell.cronicBtn.setTitle(self.currentTitle!, for: .normal)
        cell.cronicBtn.underline()
        print(cell.cronicBtn.currentTitle)

        
        cell.deleteCronic.addTarget(self, action: #selector(deleteRow), for: .touchUpInside)
      //  cell.incrementBtn.addTarget(self, action: #selector(increment), for: .touchUpInside)
        return cell
    }
    
    
    
}

extension SearchMedicineVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{

}

extension SearchMedicineVC:OptionsCell{
    func onClickCell(index: Int) {
    
    }
    
    
}
