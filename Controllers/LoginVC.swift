//
//  ViewController.swift
//  Yoopha
//
//  Created by Yoopha on 12/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SideMenuSwift


class LoginVC: UIViewController {

    @IBOutlet weak var customView: CustomView!
    @IBOutlet weak var mobileLineView: UIView!
    var mobileArr = [String]()
    @IBOutlet weak var passwordTF: CustomTF!
    @IBOutlet weak var mobileNumberTF: CustomTF!
    @IBOutlet weak var privacyTextView: UITextView!
    
    var consumerId:Int?
    var tokenId:String?
    
    let resultArray = [loginModel]()
    var consumerArray = [ConsumerModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyBoardWhenTappedOnView()
        updateTextView()
        
        mobileNumberTF.delegate = self
        
        mobileNumberTF.text = "9985544837"
        passwordTF.text = "raja@123"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK: Forgot Password
    @IBAction func tappedOnForgotPasswordBtn(_ sender: Any) {
        
        let navToOTPVC = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.otpVC) as!  OTPVC
      
        present(navToOTPVC, animated: false, completion: nil)
    }
    
    @IBAction func tappedOnCreateAccountBtn(_ sender: Any) {
        
        let navToSignUpVC = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.signUpVC) as! SignupVC
        present(navToSignUpVC, animated: false, completion: nil)
    }
    
    @IBAction func tappedOnLoginBtn(_ sender: Any) {
        
        
         if mobileNumberTF.text!.isEmpty || passwordTF.text?.isEmpty == true{
            simpleAlert(message: "Please Enter Your Details")
        }
         else{
            login()
            
        }
       
    }
    
    func login(){
       
            let parameters = ["username":self.mobileNumberTF.text,"password":self.passwordTF.text]
            let url1 = "\(Constants.API.base_URL)\(Constants.API.login)"
        
            WebApiCallBack.shared.POSTService(serviceType: url1, parameters: parameters as! [String : String]) { (response) -> (Void) in
            print(response)
            if response != nil{
                let response = JSON(response).dictionary
                
                if  let error = response?["non_field_errors"]?.array{
                    let errorvalue = error[0].string
                    if errorvalue == "Unable to log in with provided credentials."{
                        AlertView.instance.tokenLbl.isHidden = true
                        AlertView.instance.showAlert(title: "YOOPHA", message: "Please Check Your details", alertType: .failure)
                        AlertView.instance.doneBtn.setTitle("OK", for: .normal)
                    }
                }
                    
                else{
                   
                    let SUCCESS = response
                    if SUCCESS != nil{
                        let userToken = SUCCESS!["token"]?.string
                        
                        let userId = SUCCESS!["userid"]?.int
                        
                        let profile = SUCCESS!["profile"]?.string
                        loginModel.mobileNumber = self.mobileNumberTF.text!
                        loginModel.userProfile = profile
                        loginModel.token = userToken!
                        loginModel.userid = userId!
                        self.consumerId = loginModel.userid
                        self.tokenId = loginModel.token
                        
                        print(loginModel.userProfile!)
                        print(loginModel.userid)
                        
                         self.getConsumer()
                        
                        let navToMM = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
                        self.present(navToMM, animated: false, completion: nil)
                    }else{
                        simpleAlert(message: "Some Thing Went Wrong, Please Try Agani")
                    }
                   
                }
                }

               
        }
       
    }

    func getConsumer(){
        let url2 = "\(Constants.API.base_URL)\(Constants.API.consumer_consumer)"
        let finalUrl = url2 + "\(loginModel.userid)/"
        print(finalUrl)

        WebApiCallBack.shared.GETServiceHeader(url: finalUrl,token: loginModel.token) { (response) -> (Void) in
            print(response)

            if response != nil{
                if let response = JSON(response).dictionary{
                    if  let results = response["results"]?.array{
                        for result in results{
                            var consumer = ConsumerModel()
                            consumer.id = result["id"].int
                            consumer.consumerID = result["consumerID"].string
                            consumer.name = result["name"].string
                           
                            ConsumerModel.consumerId = consumer.id!
                            ConsumerModel.consumerName = consumer.name!
                            print(ConsumerModel.consumerId)
                            print(ConsumerModel.consumerName)
                            print(consumer.id as Any)

                            self.consumerArray.append(consumer)
                            let consumerAddress = result["address"].dictionary
                            let consumerID = consumerAddress!["id"]?.int

                            ConsumerAddressModel.id = consumerID!
                            print(ConsumerAddressModel.id as Any)
                        }




                    }
                }

            }
        }

    }

    @IBAction func tappedOnSignupButton(_ sender: Any) {
        
        let navToSignup = storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
      self.present(navToSignup, animated: false, completion: nil)
        
        
    }
    func updateTextView() {
        let path = "https://www.yoopha.com"
        let text = privacyTextView.text ?? ""
        let attributedString = NSAttributedString.makeHyperlink(for: path, in: text, as: "Privacy Policy")
        let font = privacyTextView.font
        let textColor = privacyTextView.textColor
        privacyTextView.attributedText = attributedString
        privacyTextView.font = font
        privacyTextView.textColor = textColor
    }


}


extension LoginVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(mobileNumberTF.text?.count as Any)
        if mobileNumberTF.text!.count < 9 ||  mobileNumberTF.text!.count > 9{
            
            mobileLineView.backgroundColor = .red
              return true
        }
        else{
            mobileLineView.backgroundColor = .green
              return true
        }
      
    }
    
    
}




