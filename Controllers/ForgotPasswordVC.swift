//
//  ForgotPasswordVC.swift
//  Yoopha
//
//  Created by Yoopha on 03/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Toast_Swift
import SwiftyJSON

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var confirmPassword: CustomTF!
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var passwordTF: CustomTF!
    @IBOutlet weak var oldPasswordTF: CustomTF!
    var oldPasswordTFStatus = false
    var oldPasswordViewStatus = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if oldPasswordTFStatus == true{
            
            oldPasswordTF.isHidden = true
            oldPasswordView.isHidden = true
        
        }
        
    }
    

    @IBAction func tappedOnSubmit(_ sender: Any) {
    
        if oldPasswordTF.text?.isEmpty == true || passwordTF.text?.isEmpty == true || confirmPassword.text?.isEmpty == true{
            
            simpleAlert(message: "Please fill your details")
        }else{
            changePassword()
        }
       
    }
    
    func changePassword(){
        let params = ["old_password":oldPasswordTF.text,"new_password1":passwordTF.text,"new_password2":confirmPassword.text]
        
        WebApiCallBack.shared.POSTServiceHeader(serviceType: "\(Constants.API.base_URL)\(Constants.API.changePassword)", parameters:  params as! [String : String], token:loginModel.token) { (response) -> (Void) in
            
            print(response)
            
            if let status = response["success"].string{
                
                if status == status{
                    self.view!.makeToast(status,duration:3.0,position:.center)
                }
                
                let navTo = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
                self.present(navTo, animated: false, completion: nil)
            }

            
            if let failure = response["old_password"].array{
                
                let failureStatus = failure[0].string
                if failureStatus == failureStatus{
                    self.view!.makeToast(failureStatus,duration:3.0,position:.center)
                }
            }
           
        }
    }
    
    @IBAction func tappedOnBackNav(_ sender: Any) {
        let navTo = storyboard?.instantiateViewController(withIdentifier: Constants.ViewController.loginVC) as! LoginVC
        present(navTo , animated: false, completion: nil)
    }
}
