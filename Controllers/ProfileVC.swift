//
//  ProfileVC.swift
//  Yoopha
//
//  Created by Yoopha on 05/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import UIKit
import Parchment

class ProfileVC: UIViewController {

    var itemArray=["Personal","Patient"]
    var fixedpgController:FixedPagingViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageView()
      
    }
    
    func setPageView(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let personalProfile = storyboard.instantiateViewController(withIdentifier: Constants.ViewController.personalProfileVC)
        let patientProfile = storyboard.instantiateViewController(withIdentifier:Constants.ViewController.patientProfileVC)
       
        fixedpgController=FixedPagingViewController(viewControllers: [personalProfile,patientProfile])
        addChild(fixedpgController)
        view.addSubview(fixedpgController.view)
        view.constrainToEdges(fixedpgController.view)
        fixedpgController.didMove(toParent: self)
        fixedpgController.dataSource=self
    }

   

    @IBAction func navBack(_ sender: Any) {
       // let navTo = storyboard?.instantiateViewController(withIdentifier:Constants.ViewController.mainMenuTBC) as! MainMenuTBC
       // navigationController?.popToViewController(navTo, animated: true)
        navigationController?.popViewController(animated: true)
      //  navigationController.pop
       // self.navigationController?.popToViewController ((self.navigationController?.viewControllers[1]) as! Your_ViewController, animated: true)
       // present(navTo, animated: true, completion: nil)
        
    }
}

extension ProfileVC:PagingViewControllerDataSource{
    
    func numberOfViewControllers<T>(in pagingViewController: PagingViewController<T>) -> Int where T : PagingItem, T : Comparable, T : Hashable {
        return itemArray.count
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController where T : PagingItem, T : Comparable, T : Hashable {
        return fixedpgController.viewControllers[index]
        
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T where T : PagingItem, T : Comparable, T : Hashable {
        return PagingIndexItem(index: index, title: itemArray[index]) as! T
    }
    
    
}
