//
//  LoginInfo.swift
//  Yoopha
//
//  Created by Yoopha on 03/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation

struct loginModel{
   
    static var active = String()
    let groupID = Int()
    let groupName = String()
    static var profile = String()
    
    static var mobileNumber:String?
    static var userid = Int()
    static var token = String()
    static var patientId = String()
    static var id:Int?
    static var userName = String()
    static var appointmentDate:String?
    static var appointmentPatientId:Int?
    static var createProfileStatus:Bool?
    static var userProfile:String?
    
    
    
}
//"userid": 6,
//"active": true,
//"profile": "Y",
//"groupID": "1",
//"groupName": "CONSUMER"
