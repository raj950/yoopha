//
//  ConsumerRegistration.swift
//  Yoopha
//
//  Created by Yoopha on 23/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation

struct ConsumerRegistration: Codable {
    let email, mobileNo, username, password: String
    let confirmPassword: String
    let isVerified: Bool
    
    enum CodingKeys: String, CodingKey {
        case email
        case mobileNo = "mobile_no"
        case username, password
        case confirmPassword = "confirm_password"
        case isVerified = "is_verified"
    }
}
