//
//  SearchDoctor.swift
//  Yoopha
//
//  Created by Yoopha on 02/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//


import Foundation

// MARK: - SearchDoctorInfo
struct SearchDoctorInfo: Codable {
    let count: Int
    let next, previous: JSONNull?
    let results: [Result]
    static var doctors:SearchDoctorInfo?
}

// MARK: - Result
struct Result: Codable {
    let id: Int
    let doctorID, doctorName: String
    let doctorEducation: DoctorEducation
    let practiceStartedYear: Int
    let gender: Gender
    let doctorLicense: DoctorLicense
    let doctorDesc: DoctorDesc
    let doctorProfileImage: [DoctorProfileImage]
    let doctorSpeciality: DoctorSpeciality
    let membershipDetails: MembershipDetails
    let registrationNo: String
    let registrationCouncil: MembershipDetails
    let languagesKnown: LanguagesKnown
    let consultationFee, paymentType: String
    let user: Int
    let address: Address
    
    enum CodingKeys: String, CodingKey {
        case id, doctorID
        case doctorName = "doctor_name"
        case doctorEducation = "doctor_education"
        case practiceStartedYear = "practice_started_year"
        case gender
        case doctorLicense = "doctor_license"
        case doctorDesc = "doctor_desc"
        case doctorProfileImage = "doctor_profile_image"
        case doctorSpeciality = "doctor_speciality"
        case membershipDetails = "membership_details"
        case registrationNo = "registration_no"
        case registrationCouncil = "registration_council"
        case languagesKnown = "languages_known"
        case consultationFee = "consultation_fee"
        case paymentType = "payment_type"
        case user, address
    }
}

// MARK: - Address
struct Address: Codable {
    let id: Int
    let addressLine1: AddressLine1
    let addressLine2: AddressLine2
    let addressLine3: String
    let pincode: Int
    let district: District
    let state: State
    let country: Country
    let long, lat: String
    let addressType: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case addressLine1 = "address_line1"
        case addressLine2 = "address_line2"
        case addressLine3 = "address_line3"
        case pincode, district, state, country, long, lat
        case addressType = "address_type"
    }
}

enum AddressLine1: String, Codable {
    case doorNo10 = "door no 10"
}

enum AddressLine2: String, Codable {
    case banuNagarMainRoad = "banu nagar Main Road"
    case velacheryMainRoad = "Velachery Main Road"
}

enum Country: String, Codable {
    case india = "India"
}

enum District: String, Codable {
    case tamilNaduChennai = "TAMIL NADU-CHENNAI"
}

enum State: String, Codable {
    case indiaTAMILNADU = "India-TAMIL NADU"
}

enum DoctorDesc: String, Codable {
    case doctor = "DOCTOR"
}

enum DoctorEducation: String, Codable {
    case mbbs = "MBBS"
}

enum DoctorLicense: String, Codable {
    case yoopha1 = "YOOPHA1"
}

// MARK: - DoctorProfileImage
struct DoctorProfileImage: Codable {
    let id: Int
    let imageName: String
    let upload: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case imageName = "image_name"
        case upload
    }
}

// MARK: - DoctorSpeciality
struct DoctorSpeciality: Codable {
    let id: Int
    let specialityName, desc: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case specialityName = "speciality_name"
        case desc
    }
}

enum Gender: String, Codable {
    case f = "F"
    case m = "M"
}

enum LanguagesKnown: String, Codable {
    case englishTamil = "English, Tamil"
}

enum MembershipDetails: String, Codable {
    case ima = "IMA"
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
