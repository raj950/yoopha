//
//  stringExtention.swift
//  Yoopha
//
//  Created by matrixstream on 20/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation

extension String{
    
    func timeConversion12(time24: String) -> String {
        let dateAsString = time24
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        
        let date = df.date(from: dateAsString)
        df.dateFormat = "hh:mm:ssa"
        
        let time12 = df.string(from: date!)
        print(time12)
        return time12
    }
    
    func stringTrims(trim: String) -> String{
        
        var a:Character?
        var b:Character?
        var c:Character?
        var d:Character?
        for i in 0..<trim.count{
            
            if i == 0{
                let indexs = trim.index(trim.startIndex, offsetBy: i)
                a = trim[indexs]
                
                
            }else if i==1{
                let indexs = trim.index(trim.startIndex, offsetBy: i)
                b = trim[indexs]
                
            }else if i == 8{
                
                let indexs = trim.index(trim.startIndex, offsetBy: i)
                c = trim[indexs]
            }else if i==9{
                
                let indexs = trim.index(trim.startIndex, offsetBy: i)
                d = trim[indexs]
            }
           
        }
        
        return "\(a!)" + "\(b!)" + "\(c!)" + "\(d!)"
        
    }
}
