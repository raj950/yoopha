//
//  NSAttributedStringExtension.swift
//  Hyperlinks
//
//  Created by Kyle Lee on 4/27/19.
//  Copyright © 2019 Kilo Loco. All rights reserved.
//

import Foundation

extension NSAttributedString {
    
    static func makeHyperlink(for path: String, in string: String, as substring: String) -> NSAttributedString {
        let nsString = NSString(string: string)
        let substringRange = nsString.range(of: substring)
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(.link, value: path, range: substringRange)
        return attributedString
    }
    
    
    
}


