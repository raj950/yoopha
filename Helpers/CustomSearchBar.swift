//
//  CustomSearchBar.swift
//  Yoopha
//
//  Created by Yoopha on 19/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomSearchBar: UISearchBar{
    
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var shadowColor: UIColor = UIColor.black {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    
    
    

}
