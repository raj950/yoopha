//
//  DropDownTF.swift
//  Yoopha
//
//  Created by Yoopha on 05/07/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import DropDown

protocol DropDownTextFieldDelegate {
    func onSelected(index: Int, item: String)
}
class DropDownTextField:UITextField,UITextFieldDelegate {
    let dropdowndelegate:DropDownTextFieldDelegate? = nil
    let dropDown:DropDown = DropDown()
    var datasource = [String]() {
        didSet {
            dropDown.dataSource = datasource
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.dropDown.show()
        return true
    }
    override func awakeFromNib() {
        dropDown.anchorView = self
        self.inputView = UIView()
        self.setLeftPaddingPoints(10)
        
       
        // self.setRightPaddingPoints(5)
        self.delegate = self
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.resignFirstResponder()
            if let thedelegate = self.dropdowndelegate {
                thedelegate.onSelected(index: 0, item: item)
            }
        }
        dropDown.cancelAction = { () in
            self.resignFirstResponder()
        }
    }
}
