//
//  ViewControllers+Extensions.swift
//  Yoopha
//
//  Created by Yoopha on 13/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//


import Foundation
import UIKit

extension UIViewController
{
    func hideKeyBoardWhenTappedOnView()
    {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc  func dismissKeyBoard()
    {
        view.endEditing(true)
    }
}
extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
}
extension UIImage {
    static func imageFromLayer (layer: CALayer) -> UIImage? {
        UIGraphicsBeginImageContext(layer.frame.size)
        guard let currentContext = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in: currentContext)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage
    }
}
extension UISearchBar {
    func setCustomBackgroundColor (color: UIColor) {
        let backgroundLayer = CALayer()
        backgroundLayer.frame = frame
        backgroundLayer.backgroundColor = color.cgColor
        if let image = UIImage.imageFromLayer(layer: backgroundLayer) {
            self.setBackgroundImage(image, for: .any, barMetrics: .default)
        }
    }
}

extension UISearchBar{
    
    func searchBarFont(){
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name:poppinsFont, size: 12)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: poppinsFont, size: 12)
        
    }
}
extension UIViewController {
    
    func alertWithTitle(title: String, message: String, buttonTitle: String,handler: ((UIAlertAction) -> Void)? = nil,cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //  let cancel = UIAlertAction(title: cancelButtonTitle, style: .default, handler: cancelHandler)
        
        //   alertController.addAction(cancel)
        
        if buttonTitle != ""{
            
            let action = UIAlertAction(title: buttonTitle, style: .default, handler: handler)
            
            alertController.addAction(action)
            
        }
        self.present(alertController, animated: true, completion: nil)
        return alertController
        
    }
    
}
extension UIColor {
    static let appGreenColor = UIColor(red: 39.0/255, green: 170.0/255, blue: 165/255, alpha: 1.0)
    static let aptCancelStatusColor = UIColor(red: 230.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0)
    static let aptCancelStatusBackgroundColor = UIColor(red: 253.0/255, green: 229.0/255, blue: 229.0/255, alpha: 1.0)
    static let aptDefaultBackgroundColor = UIColor(red: 233.0/255, green: 246.0/255, blue: 246.0/255, alpha: 1.0)
}
extension UIButton{
    
    func underline() {
        guard let title = self.titleLabel else { return }
        guard let tittleText = title.text else { return }
        let attributedString = NSMutableAttributedString(string: (tittleText))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: (tittleText.count)))
        self.setAttributedTitle(attributedString, for: .normal)
        
        
    }
    
    
    func buttonTag(btnImg:UIButton,consumerId:Int?,DRId:Int?){
        //AppointmentStatusModel.tagStatus = false
        
        if AppointmentStatusModel.tagStatus == true{
            
            if btnImg.currentImage == UIImage(named: "Fovourite-final"){
                AppointmentStatusModel.tagStatus = false
                
                btnImg.setImage(UIImage(named: "fovourite-selected"), for: .normal)
                let myParams = ["consumer":consumerId,"doctor":DRId]
                print(myParams)
                let url = "\(Constants.API.base_URL)\(Constants.API.create_consumerDoctorTag)"
                WebApiCallBack.shared.POSTServiceHeader(serviceType: url, parameters: myParams as [String : Any], token:loginModel.token) { (response) -> (Void) in
                    print(response)
                }
                
            }else{
                btnImg.setImage(UIImage(named: "Fovourite-final"), for: .normal)
                let params = NSMutableDictionary()
                let url = Constants.API.base_URL + "y06_tags/"
                let url2 = consumerId
                let finalUrl = url + "\(url2)" + Constants.API.consumerDoctor_untag
                print(finalUrl)
                WebApiCallBack.makeDeleteCall(webUrl: finalUrl, paramData: params, token:loginModel.token) { (response, error) in
                    
                    print(response)
                    
                }
                
                
            }
        }
    }
}
extension Date {
    
    init(year: Int, month: Int, day: Int) {
        var dc = DateComponents()
        dc.year = year
        dc.month = month
        dc.day = day
        
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        if let date = calendar.date(from: dc) {
            self.init(timeInterval: 0, since: date)
        } else {
            fatalError("Date component values were invalid.")
        }
    }
    
    func GetYear() -> Int {
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let components = calendar.components([.day , .month , .year], from: self)
        return components.year!
    }
    
    func GetMonth() -> Int {
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let components = calendar.components([.day , .month , .year], from: self)
        return components.month!
    }
    
    func GetDay() -> Int {
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let components = calendar.components([.day , .month , .year], from: self)
        return components.day!
    }
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}
extension UIViewController {
    
    func alertWithMessage(title: String, message: String, cancelButtonTitle: String, buttonTitle: String,handler: ((UIAlertAction) -> Void)? = nil,cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: cancelHandler)
        
        alertController.addAction(cancel)
        
        if buttonTitle != ""{
            
            let action = UIAlertAction(title: buttonTitle, style: .destructive, handler: handler)
            
            alertController.addAction(action)
            
        }
        self.present(alertController, animated: true, completion: nil)
        return alertController
        
    }
    
}
extension String{
    func strikeThrough()->NSAttributedString{
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
}
