//
//  CustomLabel.swift
//  Yoopha
//
//  Created by Yoopha on 19/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class CustomLabel:UILabel{
    
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
}
