//
//  Constants.swift
//  Yoopha
//
//  Created by Yoopha on 17/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation



        public let data:Data              = Data()
        public let poppinsFont            = "Poppins"
        public let communityUrl           = "https://community.yoopha.com"
        public let blogsUrl               = "https://blogs.yoopha.com"
        public let Dr                     = "Dr."

class Constants: NSObject {
    static let shared = Constants()
    
class API:NSObject{
        
        static let searchDoctor1          = "https://testapi.yoopha.com/api/v1/y03_doctor/search_speciality_doctor_clinic/?search="

        static let base_URL               = "https://testapi.yoopha.com/api/v1/"
        static let login                  = "y00_auth/login/"
        static let consumerRegister       = "y00_auth/consumer_register/"
        static let searchDoctor           = "y00_auth/v1/y03_doctor/search_doctor/"
        static let changePassword         = "y00_auth/change_pass/"
        static let logOut                 = "y00_auth/logout/"
        static let generateOTP            = "y01_common/generate_otp/"
        static let verifyOTP              = "y01_common/verify_otp/"
        static let districtsList          = "y01_common/district_list/24/"
        static let countryList            = "y01_common/country/"
        static let statesList             = "y01_common/state_list/1/"
        static let doctorsGeoList         = "y03_doctor/doctor_geo_list/80.000/13.000/4/"
        static let patientList            = "y05_consumer/get_user_patientdetails/"
        static let createConsumer         = "y05_consumer/create/"
        static let createPatientDetails   = "y05_consumer/create_patientdetails/"
        static let doctorAppointment      = "y07_appointment/create_Doctor_appointment/"
        static let doctor_appointment_view_by_patient = "y07_appointment/Doctor_appointment_view_by_patient/"
        static let verifyMobileNumber     = "y00_auth/check_mobile_valid/"
        static let verifyEmail            = "y00_auth/check_email_valid"
        static let get_doctor_appointment_by_slug = "y07_appointment/get_doctor_appointment_by_slug/"
        static let doctor_appointment_view_by_user = "y07_appointment/Doctor_appointment_view_by_user"
        static let create_consumerDoctorTag = "y06_tags/create_consumerDoctorTag/"
        static let consumerDoctor_untag = "/ConsumerDoctor_untag/"
        static let getConsumer = "y05_consumer/consumer/"
        static let create_sos              = "y16_sos/create_sos/"
        static let create_sos_on          = "y16_sos/create_sos_on/"
        static let deactivate_by_sos_id   = "y16_sos/sos_deactivate_by_sos_id/"
        static let prescription_view_by_appointment_id = "y08_eprescription/prescription_view_by_appointment_id/"
        
        static let observation_view_by_appointment = "y07_appointment/observation_view_by_appointment/"
        static let consumerDoctorTag_view_by_consumer = "y06_tags/consumerDoctorTag_view_by_consumer/"
        static let consumer_consumer = "y05_consumer/consumer/"
        static let create_pharmacy_order = "y10_order_medicine/create_pharmacy_order/"
        static let get_user_patientdetails = "y05_consumer/get_user_patientdetails/"
        static let prescription_view_by_user_id = "y08_eprescription/prescription_view_by_user_id/"
        static let blogs = "https://blogs.yoopha.com/ghost/api/v2/content/posts/?key=ee1713b3f87a4d836972aa5b7f&limit=6"
      //  static let consumerDetails = "y05_consumer/consumer/"
    
      //  static let observation_view_by_appointment = "y07_appointment/observation_view_by_appointment/"
        
    }
class Cell:NSObject{
        
        static let searchDoctorsCell      = "SearchDoctorsCell"
        static let userAppointmentsCell   = "UserAppointmentsCell"
        static let tagsTVC                = "TagsTVC"
        static let symptomsTVC            = "SymptomsTVC"
        static let notificationsTVC       = "NotificationsTVC"
        static let personal_InfoTVC       = "Personal_InfoTVC"
        static let orderCell              = "OrderCell"
        static let orderQuantityTVC       = "OrderQuantityTVC"
        static let transactionHistoryTVC  = "TransactionHistoryTVC"
        static let searchMedicineTVC      = "SearchMedicineTVC"
        static let settingsTVC            = "SettingsTVC"
        static let calenderCVC            = "calenderCVC"
        static let clinicTVC              = "ClinicTVC"
        static let sortTVC                = "SortTVC"
        static let contentMenuTVC         = "ContentMenuTVC"
        static let trendingTopicsCVC      = "TrendingTopicsCVC"
        static let shareAndLearnTVC       = "ShareAndLearnTVC"
        static let menuMainTVC            = "MenuMainTVC"
        static let welcomeCVC             = "WelcomeCVC"
        static let locationsTVC           = "LocationsTVC"
    
       
    
    }
class ViewController:NSObject{
    
        static let sideMenuController     = "SideMenuController"
    
        static let consumerSearchVC       = "ConsumerSearchVC"
        static let medical_InfoVC         = "Medical_InfoVC"
        static let otpVC                  = "OTPVC"
        static let signUpVC               = "SignupVC"
        static let loginVC                = "LoginVC"
        static let verifyOTP              = "Verify_OTPVC"
        static let doctorDetailedVC       = "DoctorDetailedVC"
        static let menuVC                 = "MenuVC"
        static let mainMenuTBC            = "MainMenuTBC"
        static let orderVC                = "OrderVC"
        static let settingsVC             = "SettingsVC"
        static let searchDoctorVC         = "SearchDoctorVC"
        static let forgotPasswordVC       = "ForgotPasswordVC"
        static let bookAppointmentVC      = "BookAppointmentVC"
        static let profileVC              = "ProfileVC"
        static let personal_InfoVC        = "Personal_InfoVC"
        static let personalProfileVC      = "PersonalProfileVC"
        static let patientProfileVC       = "PatientProfileVC"
        static let healthRecordsVC        = "HealthRecordsVC"
        static let tagsVC                 = "TagsVC"
        static let edit_ProfileVC         = "Edit_ProfileVC"
        static let yourAppointmentVC      = "YourAppointmentVC"
        static let appointmentDetailsVC   = "AppointmentDetailsVC"
        static let E_PrescriptionVC       = "E_PrescriptionVC"
        static let labResultsVC           = "LabResultsVC"
        static let observationsVC         = "ObservationsVC"
        static let labResultsDetailsVC    = "LabResultsDetailsVC"
        static let prescriptionDetailsDataVC  = "PrescriptionDetailsDataVC"
        static let observationsListVC     = "ObservationsListVC"
        static let appointmentsVC         = "AppointmentsVC"
        static let notificationVC         = "NotificationVC"
        static let locationsVC            = "LocationsVC"
        static let appointmentStatusVC    = "AppointmentStatusVC"
        static let closedAppointmentDetailsVC = "ClosedAppointmentDetailsVC"
        
       
        
    }
class ConstantStrings:NSObject{
        
        static let comma                  = ","
        static let minusString            = "-"
        static let slash                  = "/"
        static let rupee                  = "₹"
        static let year                   = "Yrs"
        static let defaultImage           = "user_profile"
        static let mornArrayElements      = ["MORN","MORN","MORN","MORN","MORN","MORN","MORN"]
        static let noonArrayElements      = ["NOON","NOON","NOON","NOON","NOON","NOON","NOON"]
        static let evenArrayElements      = ["EVEN","EVEN","EVEN","EVEN","EVEN","EVEN","EVEN"]
        
        static let menuSections           = [
//                                                Section(quastion: "     Appointments",
//                                                    options: [],
//                                                    expanded: false),
                                                Section(quastion: "     Health Records",
                                                    options: [" Observations"," E-Prescriptions"," E-Lab"],
                                                    expanded: true),
                                                Section(quastion: "     Tags",
                                                        options: [],
                                                        expanded: false),
                                                Section(quastion: "     Blogs",
                                                        options: [],
                                                        expanded: false),
                                                Section(quastion: "     Share & Learn",
                                                        options: [],
                                                        expanded: false),
                                                Section(quastion: "     About",
                                                        options: [],
                                                        expanded: false),
                                                Section(quastion: "     Log Out",
                                                        options: [],
                                                        expanded: false),
                                                Section(quastion: "     Setting",
                                                        options: [],
                                                        expanded: false),]
        
        static let settingsContent       = ["Remainders","Push Notifications","Rate us on AppStore",
                                            "Support","Change Password","     Log Out"]
    
        static let menuContent = ["Profile","Settings","Order History","Transactions","Health Records","About"]
    
        static let popularLocations = ["Adyar","Guindy","Velachery","Anna Nagar","Tambaram","Saidapet","T.Nagar","Nungambakam","Vadapalani","Porur"]
    }
    
    
   
}


