
import UIKit
import SwiftyJSON
import SVProgressHUD

import Alamofire
import SystemConfiguration

let window = UIApplication.shared.keyWindow

let ReachabilityStatusChangedNotification = "ReachabilityStatusChangedNotification"

enum ReachabilityType: CustomStringConvertible {
    case WWAN
    case WiFi
    
    var description: String {
        switch self {
        case .WWAN: return "WWAN"
        case .WiFi: return "WiFi"
        }
    }
}

enum ReachabilityStatus: CustomStringConvertible  {
    case Offline
    case Online(ReachabilityType)
    case Unknown
    
    var description: String {
        switch self {
        case .Offline: return "Offline"
        case .Online(let type): return "Online (\(type))"
        case .Unknown: return "Unknown"
        }
    }
}

class WebApiCallBack:UIViewController{
    
    static let shared = WebApiCallBack()
    let count = 1
    class func connectionStatus() -> ReachabilityStatus {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                
                SCNetworkReachabilityCreateWithAddress(nil, $0)
                
            }
            
        }) else {
            return .Unknown
        }
        
        var flags : SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .Unknown
        }
        
        return ReachabilityStatus(reachabilityFlags: flags)
    }
    
    
    
    func POSTService(serviceType : String,parameters : [String:String], onTaskCompleted : @escaping (JSON)->(Void)) {
        
        if WebApiCallBack.isConnectedToNetwork() != true {
            simpleAlert(message: "Please verify internet connectivity.")
            return
        }else{
        print(serviceType)

        print(parameters)
        guard let url = URL(string: "\(serviceType)") else { return }
        print(parameters)
       
        SVProgressHUD.show(withStatus: "Loading")
            Alamofire.request(url, method: .post, parameters: parameters,encoding: JSONEncoding.default,headers: nil).responseJSON{ response in
            
            print(response)
            let jsonData = JSON(response.result.value as Any)
            onTaskCompleted(jsonData)
            SVProgressHUD.dismiss()
        }
        }
    }
    func GETService(serviceType : String,parameters : [String:String], onTaskCompleted : @escaping (JSON)->(Void)) {


        if WebApiCallBack.isConnectedToNetwork() != true {
            simpleAlert(message: "Please verify internet connectivity.")
            return
        }else{
        print(serviceType)
        guard let url = URL(string: "\(serviceType)") else { return }
        print(parameters)
//        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading")
            Alamofire.request(url, method: .get, parameters: parameters,headers: nil).responseJSON{ response in

                //  print(response)
                let jsonData = JSON(response.result.value as Any)
               // DispatchQueue.global().asyncAfter(deadline: .now() + 1 ){
                    onTaskCompleted(jsonData)
                    SVProgressHUD.dismiss()
              //  }

            }
        //}
        }

    }
    
    
    func GETService123(extraParam : String, onTaskCompleted : @escaping (JSON)->(Void) ) {
        
        print(extraParam)
        guard let url = URL(string: "\(extraParam)") else { return }
        Alamofire.request(url, method: .get,encoding:JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                print(response)
                let jsonData = JSON(response.result.value as Any)
                onTaskCompleted(jsonData)
        }
    }
    
    func POSTServiceHeader(serviceType : String,parameters : [String:Any],token:String, onTaskCompleted : @escaping (JSON)->(Void)) {
        if WebApiCallBack.isConnectedToNetwork() != true {
            simpleAlert(message: "Please verify internet connectivity.")
            return
        }else{
        print(serviceType)
        guard let url = URL(string: "\(serviceType)") else { return }
        print(parameters)
        
        let headersVal = [
            "Authorization": "JWT"+" "+(token),
            "Content-Type": "application/json",
            "accept": "application/json"
            
        ]
        
        print(headersVal)
        SVProgressHUD.show(withStatus: "Loading")
        Alamofire.request(url, method: .post, parameters: parameters,encoding: JSONEncoding.default,headers: headersVal).responseJSON{ response in
           
        //    print(response)
            let jsonData = JSON(response.result.value as Any)
           
            onTaskCompleted(jsonData)
              SVProgressHUD.dismiss()
        }
        }
    }
    
    func GETServiceHeader(url : String,token:String,onTaskCompleted : @escaping (JSON)->(Void) ) {
        if WebApiCallBack.isConnectedToNetwork() != true {
            simpleAlert(message: "Please verify internet connectivity.")
            return
        }else{
        print(url)
         guard let url = URL(string: "\(url)") else { return }
        
        let headersVal = [
            "Authorization": "JWT"+" "+(token),
            "Content-Type": "application/json",
            "accept": "application/json"
        ]
         SVProgressHUD.show(withStatus: "Loading")
        Alamofire.request(url, method: .get,encoding:JSONEncoding.default, headers: headersVal).responseJSON { response in
                
                print(response)
                let jsonData = JSON(response.result.value as Any)
                onTaskCompleted(jsonData)
         SVProgressHUD.dismiss()
        }
    }
    }
    
//    func GETServiceHeader(serviceType : String,token:String, onTaskCompleted : @escaping (JSON)->(Void)) {
//
//
//        print(serviceType)
//        guard let url = URL(string: "\(serviceType)") else { return }
//
//
//        let headersVal = [
//            "Authorization": "JWT"+" "+(token),
//            "Content-Type": "application/json",
//            "accept": "application/json"
//        ]
//
//        print(headersVal)
//        SVProgressHUD.show()
//        DispatchQueue.main.async {
//            Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers: headersVal).responseJSON{ response in
//
//                //  print(response)
//                let jsonData = JSON(response.result.value as Any)
//
//                onTaskCompleted(jsonData)
//                SVProgressHUD.dismiss()
//
//            }
//        }
//    }
    class func monitorReachabilityChanges() {
        let host = "google.com"
        var context = SCNetworkReachabilityContext(version: 0, info: nil, retain: nil, release: nil, copyDescription: nil)
        let reachability = SCNetworkReachabilityCreateWithName(nil, host)!
        
        SCNetworkReachabilitySetCallback(reachability, { (_, flags, _) in
            let status = ReachabilityStatus(reachabilityFlags: flags)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification),
                object: nil)
            
            }, &context)
        SCNetworkReachabilityScheduleWithRunLoop(reachability, CFRunLoopGetMain(),context as! CFString)
    }
    class func isConnectedToNetwork() -> Bool {
        
        let status = connectionStatus()
        switch status {
        case .Unknown, .Offline:
//            print("Not connected")
                return false
        case .Online(.WWAN):
//            print("Connected via WWAN")
            return true
        case .Online(.WiFi):
//            print("Connected via WiFi")
            return true
        }
        
    }

    class func requestApi(webUrl: String,paramData: NSObject, token:String,methiod: String, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
     
        if (!isConnectedToNetwork()){
             simpleAlert(message: "Please verify internet connectivity.")
            
        }else{
              if methiod == "post"{
                //let object = paramData as! NSMutableDictionary
               // object.setObject("Y", forKey: "NeedLangaugeLabel" as NSCopying)
               
                makePostCall(webUrl: webUrl, paramData: paramData,completionHandler: completionHandler)
               
            }else if methiod == "Get" {
                //let urlStr = webUrl + "NeedLangaugeLabel=Y"
                makeGetCall(webUrl: webUrl, paramData: paramData,completionHandler: completionHandler)
            }else if methiod == "Put"{
                //let urlStr = webUrl + "NeedLangaugeLabel=Y"
                makePutCall(webUrl: webUrl, paramData: paramData,completionHandler: completionHandler)
            }else if methiod == "Delete"{
                //let urlStr = webUrl + "NeedLangaugeLabel=Y"
                makeDeleteCall(webUrl: webUrl, paramData: paramData, token: token,completionHandler: completionHandler)
            }else if methiod == "Patch"{
                //let urlStr = webUrl + "NeedLangaugeLabel=Y"
                makePatchCall(webUrl: webUrl, paramData: paramData,token: token,completionHandler: completionHandler)
            }
           
        }
        
    }
    
    class func makePostCall(webUrl: String, paramData: NSObject, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        let tokenVal = "sai"
        let authVal = "sai"

        let headersVal = [
            "Authorization": "Bearer "+(tokenVal as String),
            "auth": ""+(authVal as String)
        ]
        
        
        Alamofire.request(webUrl, method: .post, parameters:  paramData as? [String : AnyObject], encoding: JSONEncoding.default, headers: headersVal)
            .responseJSON { response in
//                print("First json \(response)")
            // SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    completionHandler(value as? NSDictionary, nil)
                   
                case .failure(let error):
                    
//                 print("Failure with JSON: \(error)")
                    completionHandler(nil, error as NSError?)
                    
                    // SVProgressHUD.dismiss()
                }
                
        }
    }
    
    class func makeGetCall(webUrl: String, paramData: NSObject, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        let tokenVal = "sai"
        let authVal = "sai"
        
        let headersVal = [
            "Authorization": "Bearer"+(tokenVal as String),
            "auth": ""+(authVal as String)
        ]
        
        
        Alamofire.request(webUrl, method: .get, parameters:  nil, encoding: JSONEncoding.default, headers: headersVal)
            .responseJSON { response in
                //                print("First json \(response)")
                
               
                 DispatchQueue.global().asyncAfter(deadline: .now() - 10 ){
                switch response.result {
                case .success(let value):
                  
                    
                   
                         completionHandler(value as? NSDictionary, nil)
                        
                
                case .failure(let error):
                    
                    //                 print("Failure with JSON: \(error)")
                    completionHandler(nil, error as NSError?)
                    
                    
                }
                
                }
        }
    
    }
    
    class func makePutCall(webUrl: String, paramData: NSObject, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        let tokenVal = "sai"
        let authVal = "sai"
        
        let headersVal = [
            "Authorization": "Bearer "+(tokenVal as String),
            "auth": ""+(authVal as String)
        ]
        
        
        Alamofire.request(webUrl, method: .put, parameters:  paramData as? [String : AnyObject], encoding: JSONEncoding.default, headers: headersVal)
            .responseJSON { response in
                //                print("First json \(response)")
                
                switch response.result {
                case .success(let value):
                    completionHandler(value as? NSDictionary, nil)
                case .failure(let error):
                    
                    //                 print("Failure with JSON: \(error)")
                    completionHandler(nil, error as NSError?)
                }
        }
    }
    class func makePatchCall(webUrl: String, paramData: NSObject,token:String, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let headersVal = [
            "Authorization": "JWT"+" "+(token),
            "Content-Type": "application/json"
            
        ]
        
        Alamofire.request(webUrl, method: .patch, parameters:  paramData as? [String : AnyObject], encoding: JSONEncoding.default, headers: headersVal)
            .responseJSON { response in
                //                print("First json \(response)")
                
                switch response.result {
                case .success(let value):
                    completionHandler(value as? NSDictionary, nil)
                case .failure(let error):
                    
                    //                 print("Failure with JSON: \(error)")
                    completionHandler(nil, error as NSError?)
                }
        }
    }
    
    class func makeDeleteCall(webUrl: String, paramData: NSObject,token:String, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let headersVal = [
            "Authorization": "JWT"+" "+(token),
            "Content-Type": "application/json"
            
        ]
        
        Alamofire.request(webUrl, method: .delete, parameters:  paramData as? [String : AnyObject], encoding: JSONEncoding.default, headers: headersVal)
            .responseJSON { response in
                //                print("First json \(response)")
                
                switch response.result {
                case .success(let value):
                    completionHandler(value as? NSDictionary, nil)
                case .failure(let error):
                    
                    //                 print("Failure with JSON: \(error)")
                    completionHandler(nil, error as NSError?)
                }
        }
    }
    
}
extension ReachabilityStatus {
     init(reachabilityFlags flags: SCNetworkReachabilityFlags) {
        let connectionRequired = flags.contains(.connectionRequired)
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if !connectionRequired && isReachable {
            if isWWAN {
                self = .Online(.WWAN)
            } else {
                self = .Online(.WiFi)
            }
        } else {
            self =  .Offline
        }
    }
}
