//
//  NetworkingService.swift
//  Yoopha
//
//  Created by Yoopha on 22/06/19.
//  Copyright © 2019 yoopha. All rights reserved.
//

import Foundation
import Alamofire

class NetworkingService:NSObject{
    static let shared = NetworkingService()
    
    func restAPIPOST<T:Codable>(type:T.Type,url:String,parameters:[String:Any],error:Error,success successBlock:@escaping(T) -> Void)
    {
    
        let urlString = url
        let parameterValues = parameters
        print(parameters)
        Alamofire.request(urlString ,method: .post,parameters: parameterValues).responseJSON{ (response)  -> Void in
            do{
                print(response.result.value)
                guard let result = try? JSONDecoder().decode(T.self, from:response.result.value as! Data) else { throw NetworkingErrors.badNetworkingStuff }
                successBlock(result)
                
            }catch{
                print("Error:",error.localizedDescription)
            }
            
        }
    }
}
